public class Solution {
	public int nthUglyNumber(int n) {

		int[] res = new int[n];
		res[0] = 1;

		int mul2 = 0, mul3 = 0, mul5 = 0;

		for (int i = 1; i < n; i++) {
			res[i] = Math.min(res[mul2] * 2, Math.min(res[mul3] * 3, res[mul5] * 5));

			if (res[i] == res[mul2] * 2)
				mul2++;
			if (res[i] == res[mul3] * 3)
				mul3++;
			if (res[i] == res[mul5] * 5)
				mul5++;
		}
		return res[n - 1];
	}

	public static void main(String[] args) {
		System.out.println(new Solution().nthUglyNumber(10));
	}
}