import java.util.ArrayList;
import java.util.List;

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Solution {
	public List<List<String>> printTree(TreeNode root) {
		List<List<String>> res = new ArrayList<>();

		if (root == null)
			return res;

		int height = getHeight(root);
		int width = (1 << height) - 1;

		List<String> nullRow = new ArrayList<>();
		for (int colIdx = 0; colIdx < width; colIdx++)
			nullRow.add("");

		for (int rowIdx = 0; rowIdx < height; rowIdx++)
			res.add(new ArrayList<>(nullRow));

		fillMatrix(root, 0, 0, width - 1, res);

		return res;
	}

	private void fillMatrix(TreeNode root, int row, int left, int right, List<List<String>> res) {
		if (root == null)
			return;

		int middle = (left + right) / 2;

		res.get(row).set(middle, String.valueOf(root.val));
		fillMatrix(root.left, row + 1, left, middle - 1, res);
		fillMatrix(root.right, row + 1, middle + 1, right, res);
	}

	private int getHeight(TreeNode root) {
		if (root == null)
			return 0;
		return 1 + Math.max(getHeight(root.left), getHeight(root.right));
	}

	public static void main(String[] args) {
		TreeNode root1 = new TreeNode(1);
		root1.left = new TreeNode(2);
		List<List<String>> res1 = new Solution().printTree(root1);
		for (List<String> row : res1)
			System.out.println(row);

		TreeNode root2 = new TreeNode(1);
		root2.left = new TreeNode(2);
		root2.right = new TreeNode(3);
		root2.left.right = new TreeNode(4);
		List<List<String>> res2 = new Solution().printTree(root2);
		for (List<String> row : res2)
			System.out.println(row);

		TreeNode root3 = new TreeNode(1);
		root3.left = new TreeNode(2);
		root3.left.left = new TreeNode(3);
		root3.left.left.left = new TreeNode(4);
		root3.right = new TreeNode(5);

		List<List<String>> res3 = new Solution().printTree(root3);
		for (List<String> row : res3)
			System.out.println(row);
	}
}