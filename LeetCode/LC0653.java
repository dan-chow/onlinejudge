class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Solution {
	public boolean findTarget(TreeNode root, int k) {
		if (root == null || (root.left == null && root.right == null))
			return false;

		return findTarget(root, root, k);
	}

	private boolean findTarget(TreeNode curr, TreeNode root, int k) {
		if (curr == null)
			return false;

		TreeNode another = findNode(root, k - curr.val);
		if (another != null && another != curr)
			return true;

		if (findTarget(curr.left, root, k) || findTarget(curr.right, root, k))
			return true;

		return false;
	}

	private TreeNode findNode(TreeNode root, int k) {
		if (root == null)
			return null;

		if (root.val == k)
			return root;

		if (root.val > k)
			return findNode(root.left, k);

		return findNode(root.right, k);
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(5);
		root.left = new TreeNode(3);
		root.left.left = new TreeNode(2);
		root.left.right = new TreeNode(4);
		root.right = new TreeNode(6);
		root.right.right = new TreeNode(7);

		System.out.println(new Solution().findTarget(root, 7));
		System.out.println(new Solution().findTarget(root, 28));

	}
}