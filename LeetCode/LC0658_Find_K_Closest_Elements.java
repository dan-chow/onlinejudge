import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class Solution {
	public List<Integer> findClosestElements(List<Integer> arr, int k, int x) {
		List<Integer> greater = new ArrayList<>();
		List<Integer> smaller = new ArrayList<>();
		for (int i : arr) {
			if (i >= x)
				greater.add(i);
			else
				smaller.add(i);
		}

		Collections.reverse(smaller);
		int sIdx = 0, gIdx = 0;
		List<Integer> res = new ArrayList<>();
		for (int size = 0; size < k; size++) {
			if (sIdx < smaller.size() && gIdx < greater.size()) {
				if (Math.abs(smaller.get(sIdx) - x) <= Math.abs(greater.get(gIdx) - x))
					res.add(smaller.get(sIdx++));
				else
					res.add(greater.get(gIdx++));
			} else if (sIdx < smaller.size()) {
				res.add(smaller.get(sIdx++));
			} else {
				res.add(greater.get(gIdx++));
			}
		}

		Collections.sort(res);

		return res;
	}

	public static void main(String[] args) {
		List<Integer> arr = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
		System.out.println(new Solution().findClosestElements(arr, 4, 3));
		System.out.println(new Solution().findClosestElements(arr, 4, 1));
	}
}