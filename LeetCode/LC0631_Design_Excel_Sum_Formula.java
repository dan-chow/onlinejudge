import java.util.ArrayList;
import java.util.List;

class Element {
	int row;
	int col;

	public Element(String str) {
		this.col = str.charAt(0) - 'A';

		this.row = 0;
		for (int i = 1; i < str.length(); i++) {
			this.row = this.row * 10 + str.charAt(i) - '0';
		}
		this.row--;
	}
}

class SumOperation {
	int r;
	char c;
	String[] strs;

	public SumOperation(int r, char c, String[] strs) {
		this.r = r;
		this.c = c;
		this.strs = strs;
	}
}

public class Excel {
	private int[][] matrix;
	private List<SumOperation> ops;

	public Excel(int H, char W) {
		this.matrix = new int[H][W - 'A' + 1];
		this.ops = new ArrayList<>();
	}

	public void set(int r, char c, int v) {
		this.matrix[r - 1][c - 'A'] = v;

		List<SumOperation> toRemove = new ArrayList<>();
		for (SumOperation op : ops)
			if (op.r == r && op.c == c)
				toRemove.add(op);
		ops.removeAll(toRemove);

		boolean update = true;
		while (update) {
			update = false;
			for (int i = ops.size() - 1; i >= 0; i--)
				if (subsum(ops.get(i).r, ops.get(i).c, ops.get(i).strs))
					update = true;
		}

	}

	public int get(int r, char c) {
		return this.matrix[r - 1][c - 'A'];
	}

	public int sum(int r, char c, String[] strs) {
		List<SumOperation> toRemove = new ArrayList<>();
		for (SumOperation op : ops)
			if (op.r == r && op.c == c)
				toRemove.add(op);
		ops.removeAll(toRemove);
		this.ops.add(new SumOperation(r, c, strs));

		subsum(r, c, strs);
		return get(r, c);
	}

	public boolean subsum(int r, char c, String[] strs) {

		int result = 0;
		for (String str : strs) {
			if (str.contains(":")) {
				Element e1 = new Element(str.split(":")[0]);
				Element e2 = new Element(str.split(":")[1]);

				for (int i = e1.row; i <= e2.row; i++)
					for (int j = e1.col; j <= e2.col; j++)
						result += this.matrix[i][j];

			} else {
				Element e = new Element(str);
				result += this.matrix[e.row][e.col];
			}
		}

		if (this.matrix[r - 1][c - 'A'] == result)
			return false;
		else {
			this.matrix[r - 1][c - 'A'] = result;
			return true;
		}

	}

	public void print() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}

	public static void main(String[] args) {
		Excel obj = new Excel(3, 'C');
		obj.print();

		obj.set(1, 'A', 2);
		obj.print();

		String[] strs = { "A1", "A1:B2" };
		obj.sum(3, 'C', strs);
		obj.print();

		obj.set(2, 'B', 2);
		obj.print();
	}
}