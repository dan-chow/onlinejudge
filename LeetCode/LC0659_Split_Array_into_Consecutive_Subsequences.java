import java.util.HashMap;

class Solution {
	public boolean isPossible(int[] nums) {
		HashMap<Integer, Integer> freq = new HashMap<>();
		for (int n : nums)
			freq.put(n, freq.getOrDefault(n, 0) + 1);

		HashMap<Integer, Integer> except = new HashMap<>();
		for (int n : nums) {

			if (freq.getOrDefault(n, 0) == 0)
				continue;

			if (except.getOrDefault(n, 0) > 0) {
				freq.put(n, freq.get(n) - 1);
				except.put(n, except.get(n) - 1);
				except.put(n + 1, except.getOrDefault(n + 1, 0) + 1);
			} else if (freq.getOrDefault(n + 1, 0) > 0 && freq.getOrDefault(n + 2, 0) > 0) {
				freq.put(n, freq.get(n) - 1);
				freq.put(n + 1, freq.get(n + 1) - 1);
				freq.put(n + 2, freq.get(n + 2) - 1);
				except.put(n + 3, except.getOrDefault(n + 3, 0) + 1);
			} else {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		int[] nums1 = { 1, 2, 3, 3, 4, 5 };
		System.out.println(new Solution().isPossible(nums1));
		int[] nums2 = { 1, 2, 3, 3, 4, 4, 5, 5 };
		System.out.println(new Solution().isPossible(nums2));
		int[] nums3 = { 1, 2, 3, 4, 4, 5 };
		System.out.println(new Solution().isPossible(nums3));
	}
}