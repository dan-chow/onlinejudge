import java.util.Arrays;

public class Solution {
	public int lengthOfLongestSubstring(String s) {
		int[] table = new int[256];
		Arrays.fill(table, -1);

		int len = 0, left = 0, maxLen = 0;

		for (int i = 0; i < s.length(); i++) {
			if (table[s.charAt(i)] < left) {
				table[s.charAt(i)] = i;
				len++;
				maxLen = Math.max(maxLen, len);
			} else {
				left = table[s.charAt(i)];
				len = i - left;
				table[s.charAt(i)] = i;

			}
		}
		return maxLen;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().lengthOfLongestSubstring("abcabcbb"));
		System.out.println(new Solution().lengthOfLongestSubstring("bbbbb"));
		System.out.println(new Solution().lengthOfLongestSubstring("pwwkew"));
	}
}