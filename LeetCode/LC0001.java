import java.util.HashMap;

public class Solution {
	public int[] twoSum(int[] nums, int target) {
		HashMap<Integer, Integer> map = new HashMap<>();
		for (int i = 0; i < nums.length; i++) {
			if (!map.containsKey(target - nums[i])) {
				map.put(nums[i], i);
			} else {
				int[] res = { map.get(target - nums[i]), i };
				return res;
			}
		}
		return null;
	}

	public static void main(String[] args) {
		int[] nums = { 2, 7, 11, 15 };
		int[] res = new Solution().twoSum(nums, 9);
		for (int i : res)
			System.out.println(i);
	}
}