import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

class Element implements Comparable<Element> {
	int row;
	int col;
	int val;

	public Element(int row, int col, int val) {
		this.row = row;
		this.col = col;
		this.val = val;
	}

	@Override
	public int compareTo(Element other) {
		return this.val - other.val;
	}
}

public class Solution {

	public int[] smallestRange(List<List<Integer>> nums) {
		PriorityQueue<Element> pq = new PriorityQueue<>();
		int right = Integer.MIN_VALUE;
		for (int i = 0; i < nums.size(); i++) {
			pq.add(new Element(i, 0, nums.get(i).get(0)));
			right = Math.max(right, nums.get(i).get(0));
		}

		int minRange = Integer.MAX_VALUE;
		int start = -1;
		int end = -1;
		while (pq.size() == nums.size()) {
			Element e = pq.poll();
			if (right - e.val < minRange) {
				minRange = right - e.val;
				start = e.val;
				end = right;
			}
			if (e.col < nums.get(e.row).size() - 1) {
				e.col++;
				e.val = nums.get(e.row).get(e.col);
				pq.add(e);
				right = Math.max(right, e.val);
			}
		}
		return new int[] { start, end };
	}

	public static void main(String[] args) {
		List<List<Integer>> nums = new ArrayList<>();
		List<Integer> list1 = Arrays.asList(4, 10, 15, 24, 26);
		List<Integer> list2 = Arrays.asList(0, 9, 12, 20);
		List<Integer> list3 = Arrays.asList(5, 18, 22, 30);
		nums.add(list1);
		nums.add(list2);
		nums.add(list3);

		int[] res = new Solution().smallestRange(nums);
		for (int i : res)
			System.out.println(i);
	}
}