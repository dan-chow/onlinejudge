class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Solution {
	public TreeNode mergeTrees(TreeNode t1, TreeNode t2) {
		if (t1 == null)
			return t2;

		if (t2 == null)
			return t1;

		return subMerge(t1, t2);
	}

	private TreeNode subMerge(TreeNode t1, TreeNode t2) {
		if (t1 == null && t2 == null)
			return null;

		if (t1 == null) {
			t2.left = subMerge(null, t2.left);
			t2.right = subMerge(null, t2.right);
			return t2;
		}

		if (t2 == null) {
			t1.left = subMerge(null, t1.left);
			t1.right = subMerge(null, t1.right);
			return t1;
		}

		TreeNode newRoot = new TreeNode(t1.val + t2.val);
		newRoot.left = subMerge(t1.left, t2.left);
		newRoot.right = subMerge(t1.right, t2.right);
		return newRoot;

	}

	public static void main(String[] args) {
		TreeNode t1 = new TreeNode(1);
		t1.left = new TreeNode(3);
		t1.left.left = new TreeNode(5);
		t1.right = new TreeNode(2);

		TreeNode t2 = new TreeNode(2);
		t2.left = new TreeNode(1);
		t2.left.right = new TreeNode(4);
		t2.right = new TreeNode(3);
		t2.right.right = new TreeNode(7);

		TreeNode res = new Solution().mergeTrees(t1, t2);
		System.out.println(res);
	}
}