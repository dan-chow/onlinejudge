public class Solution {
	public int minPathSum(int[][] grid) {
		int M = grid.length;
		int N = grid[0].length;

		int[][] dp = new int[M][N];

		dp[M - 1][N - 1] = grid[M - 1][N - 1];
		for (int j = N - 2; j >= 0; j--)
			dp[M - 1][j] = grid[M - 1][j] + dp[M - 1][j + 1];
		for (int i = M - 2; i >= 0; i--)
			dp[i][N - 1] = grid[i][N - 1] + dp[i + 1][N - 1];

		for (int i = M - 2; i >= 0; i--)
			for (int j = N - 2; j >= 0; j--)
				dp[i][j] = grid[i][j] + Math.min(dp[i][j + 1], dp[i + 1][j]);

		return dp[0][0];
	}

	public static void main(String[] args) {
		int[][] grid = { { 1, 3, 2, 4 }, { 5, 1, 7, 9 }, { 3, 4, 1, 7 }, { 2, 2, 5, 3 } };
		System.out.println(new Solution().minPathSum(grid));
	}
}