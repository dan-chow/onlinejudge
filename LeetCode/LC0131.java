import java.util.ArrayList;
import java.util.List;

public class Solution {
	public List<List<String>> partition(String s) {
		List<List<String>> res = new ArrayList<>();
		if (s == null || s.isEmpty())
			return res;

		List<String> tmpList = new ArrayList<>();
		subPartition(s, 0, 0, tmpList, res);
		return res;
	}

	private void subPartition(String s, int startIdx, int tmpLength, List<String> tmpList, List<List<String>> res) {
		if (tmpLength == s.length()) {
			res.add(new ArrayList<>(tmpList));
			return;
		}

		for (int i = startIdx + 1; i <= s.length(); i++) {
			String tmp = s.substring(startIdx, i);
			if (isPalindrome(tmp)) {
				tmpList.add(tmp);
				tmpLength += tmp.length();
				subPartition(s, i, tmpLength, tmpList, res);
				tmpLength -= tmp.length();
				tmpList.remove(tmpList.size() - 1);
			}

		}
	}

	private boolean isPalindrome(String str) {
		for (int i = 0; i < str.length() / 2; i++) {
			if (str.charAt(i) != str.charAt(str.length() - 1 - i))
				return false;
		}
		return true;
	}

	public static void main(String[] args) {
		List<List<String>> res = new Solution().partition("aab");
		for (List<String> list : res)
			System.out.println(list);
	}
}