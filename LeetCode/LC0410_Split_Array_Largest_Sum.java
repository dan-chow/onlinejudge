public class Solution {
	public int splitArray(int[] nums, int m) {
		long lowerBound = Integer.MIN_VALUE;
		long upperBound = 0;
		for (int n : nums) {
			lowerBound = Math.max(lowerBound, n);
			upperBound += n;
		}

		while (lowerBound <= upperBound) {
			long maxSum = (lowerBound + upperBound) / 2;
			if (validate(nums, m, maxSum)) {
				upperBound = maxSum - 1;
			} else {
				lowerBound = maxSum + 1;
			}
		}
		return (int) lowerBound;
	}

	public boolean validate(int[] nums, int m, long sum) {
		long remain = 0;
		int cnt = 0;
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] > remain) {
				remain = sum - nums[i];
				cnt++;
				if (cnt > m)
					return false;
			} else {
				remain -= nums[i];
			}
		}
		return true;
	}

	public static void main(String[] args) {
		int[] nums = { 7, 2, 5, 10, 8 };
		System.out.println(new Solution().splitArray(nums, 2));
	}
}
