public class Solution {
	public int findUnsortedSubarray(int[] nums) {
		if (nums == null || nums.length == 0)
			return 0;

		int beg = -1;
		int min = nums[nums.length - 1];
		for (int i = nums.length - 2; i >= 0; i--) {
			min = Math.min(min, nums[i]);
			if (nums[i] > min)
				beg = i;
		}

		if (beg == -1)
			return 0;

		int end = -1;
		int max = nums[0];
		for (int i = 1; i < nums.length; i++) {
			max = Math.max(max, nums[i]);
			if (nums[i] < max)
				end = i;
		}

		return end - beg + 1;

	}

	public static void main(String[] args) {
		int[] nums = { 2, 6, 4, 8, 10, 9, 15 };
		System.out.println(new Solution().findUnsortedSubarray(nums));
	}
}