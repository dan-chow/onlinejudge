public class Solution {
	public int numSquares(int n) {
		int[] dp = new int[n + 1];

		dp[1] = 1;

		for (int i = 1; i * i <= n; i++)
			dp[i * i] = 1;

		for (int i = 2; i <= n; i++) {
			if (dp[i] == 1)
				continue;
			int min = Integer.MAX_VALUE;
			for (int j = 1; j <= i / 2; j++) {
				if (dp[j] + dp[i - j] < min)
					min = dp[j] + dp[i - j];
			}
			dp[i] = min;
		}

		return dp[n];
	}

	public static void main(String[] args) {
		System.out.println(new Solution().numSquares(5));
	}
}