public class Solution {
	public int maxProduct(String[] words) {
		int[] dict = new int[words.length];
		for (int i = 0; i < words.length; i++) {
			for (int j = 0; j < words[i].length(); j++) {
				dict[i] |= 1 << (words[i].charAt(j) - 'a' + 1);
			}
		}

		int max = 0;
		for (int i = 0; i < words.length - 1; i++) {
			for (int j = i + 1; j < words.length; j++) {
				if ((dict[i] & dict[j]) == 0) {
					int mul = words[i].length() * words[j].length();
					if (mul > max)
						max = mul;
				}
			}
		}
		return max;
	}

	public static void main(String[] args) {
		String[] words1 = { "abcw", "baz", "foo", "bar", "xtfn", "abcdef" };
		System.out.println(new Solution().maxProduct(words1));

		String[] words2 = { "a", "ab", "abc", "d", "cd", "bcd", "abcd" };
		System.out.println(new Solution().maxProduct(words2));

		String[] words3 = { "a", "aa", "aaa", "aaaa" };
		System.out.println(new Solution().maxProduct(words3));
	}
}