
class ListNode {
	int val;
	ListNode next;

	ListNode(int x) {
		val = x;
	}
}

public class Solution {
	public ListNode sortList(ListNode head) {
		if (head == null || head.next == null)
			return head;

		ListNode slow = head;
		ListNode fast = head.next;
		while (fast != null && fast.next != null) {
			slow = slow.next;
			fast = fast.next.next;
		}
		ListNode head2 = slow.next;
		slow.next = null;

		head = sortList(head);
		head2 = sortList(head2);
		return merge(head, head2);

	}

	private ListNode merge(ListNode l1, ListNode l2) {
		ListNode dummyHead = new ListNode(-1);

		ListNode p = dummyHead;
		while (l1 != null && l2 != null) {
			if (l1.val < l2.val) {
				p.next = l1;
				l1 = l1.next;
			} else {
				p.next = l2;
				l2 = l2.next;

			}
			p = p.next;
		}
		p.next = l1 != null ? l1 : l2;
		return dummyHead.next;
	}

	public static void main(String[] args) {
		ListNode head = new ListNode(2);
		head.next = new ListNode(8);
		head.next.next = new ListNode(5);
		head.next.next.next = new ListNode(1);
		head.next.next.next.next = new ListNode(9);
		head.next.next.next.next.next = new ListNode(7);

		head = new Solution().sortList(head);
		ListNode p = head;
		while (p != null) {
			System.out.println(p.val);
			p = p.next;
		}

	}
}