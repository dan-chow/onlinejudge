public class Solution {
	public int countArrangement(int N) {

		// save count in visited[0]
		int[] visited = new int[N + 1];
		countArrangement(1, N, visited);
		return visited[0];
	}

	private void countArrangement(int pos, int n, int[] visited) {
		if (pos == n + 1) {
			visited[0]++;
			return;
		}

		for (int i = 1; i <= n; i++) {
			if (visited[i] == 0 && (i % pos == 0 || pos % i == 0)) {
				visited[i] = 1;
				countArrangement(pos + 1, n, visited);
				visited[i] = 0;
			}
		}
	}

	public static void main(String[] args) {
		System.out.println(new Solution().countArrangement(2));
	}
}