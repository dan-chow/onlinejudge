public class Solution {
	public int singleNumber(int[] nums) {

		int ones = 0;
		int twos = 0;
		int threes = 0;
		for (int i : nums) {
			int newTwos = ones & i;
			twos |= newTwos;

			ones ^= i;

			threes = twos & ones;

			ones = ones & (~threes);
			twos = twos & (~threes);
		}
		return ones;
	}

	public static void main(String[] args) {
		int[] nums = { 1, 1, 1, 2, 2, 2, 3, 4, 4, 4 };
		System.out.println(new Solution().singleNumber(nums));

	}
}