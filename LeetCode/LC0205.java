import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

public class Solution {
	public boolean isIsomorphic(String s, String t) {
		if (s == null || t == null || s.length() != t.length())
			return false;

		LinkedHashMap<Character, List<Integer>> sMap = new LinkedHashMap<>();
		for (int i = 0; i < s.length(); i++) {
			if (!sMap.containsKey(s.charAt(i)))
				sMap.put(s.charAt(i), new ArrayList<Integer>());

			sMap.get(s.charAt(i)).add(i);
		}

		LinkedHashMap<Character, List<Integer>> tMap = new LinkedHashMap<>();
		for (int i = 0; i < t.length(); i++) {
			if (!tMap.containsKey(t.charAt(i)))
				tMap.put(t.charAt(i), new ArrayList<Integer>());

			tMap.get(t.charAt(i)).add(i);
		}

		Iterator<Character> itSKey = sMap.keySet().iterator();
		Iterator<Character> itTKey = tMap.keySet().iterator();

		while (itSKey.hasNext()) {
			if (!itTKey.hasNext())
				return false;

			if (!sMap.get(itSKey.next()).equals(tMap.get(itTKey.next())))
				return false;
		}

		if (itTKey.hasNext())
			return false;

		return true;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().isIsomorphic("egg", "add"));
		System.out.println(new Solution().isIsomorphic("foo", "bar"));
		System.out.println(new Solution().isIsomorphic("paper", "title"));
		System.out.println(new Solution().isIsomorphic("", ""));
	}
}