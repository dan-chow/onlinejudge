
class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Solution {
	public boolean isValidBST(TreeNode root) {
		return isValidBST(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}

	private boolean isValidBST(TreeNode root, int min, int max) {
		if (root == null)
			return true;

		if (root.val > max)
			return false;

		if (root.val < min)
			return false;

		if (root.val == max && root.right != null)
			return false;
		if (root.val == min && root.left != null)
			return false;

		return isValidBST(root.left, min, root.val - 1) && isValidBST(root.right, root.val + 1, max);
	}

	public static void main(String[] args) {

		TreeNode root1 = new TreeNode(2);
		root1.left = new TreeNode(1);
		root1.right = new TreeNode(3);
		System.out.println(new Solution().isValidBST(root1));

		TreeNode root2 = new TreeNode(1);
		root2.left = new TreeNode(2);
		root2.right = new TreeNode(3);

		System.out.println(new Solution().isValidBST(root2));
	}
}