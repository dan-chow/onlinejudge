public class Solution {

	public final static int[] table = new int[255];

	static {
		table['I'] = 1;
		table['V'] = 5;
		table['X'] = 10;
		table['L'] = 50;
		table['C'] = 100;
		table['D'] = 500;
		table['M'] = 1000;
	}

	public int romanToInt(String s) {
		int result = 0;
		for (int i = 0; i < s.length() - 1; i++) {
			if (table[s.charAt(i + 1)] > table[s.charAt(i)])
				result -= table[s.charAt(i)];
			else
				result += table[s.charAt(i)];
		}
		result += table[s.charAt(s.length() - 1)];
		return result;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().romanToInt("MMMCMXCIX"));
	}
}
