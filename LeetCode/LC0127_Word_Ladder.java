import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class Solution {

	public int ladderLength(String beginWord, String endWord, List<String> wordList) {
		HashSet<String> notReached = new HashSet<>(wordList);
		if (!notReached.contains(endWord))
			return 0;

		HashSet<String> reached = new HashSet<>();
		reached.add(beginWord);

		int dist = 1;

		while (notReached.contains(endWord)) {
			HashSet<String> newReached = new HashSet<>();
			for (String visited : reached) {
				final char[] charArr = visited.toCharArray();
				for (int i = 0; i < visited.length(); i++) {
					char[] tmpCharArr = Arrays.copyOf(charArr, charArr.length);
					for (char c = 'a'; c <= 'z'; c++) {
						tmpCharArr[i] = c;
						String tmpStr = new String(tmpCharArr);
						if (notReached.contains(tmpStr)) {
							newReached.add(tmpStr);
							notReached.remove(tmpStr);
						}

					}
				}

			}

			dist++;

			if (newReached.size() == 0)
				return 0;

			reached = newReached;
		}
		return dist;

	}

	public static void main(String[] args) {
		String beginWord = "hit";
		String endWord = "cog";
		List<String> wordList = new ArrayList<>();
		wordList.add("hot");
		wordList.add("dot");
		wordList.add("dog");
		wordList.add("lot");
		wordList.add("log");
		wordList.add("cog");

		System.out.println(new Solution().ladderLength(beginWord, endWord, wordList));
	}
}