public class Solution {
	public int search(int[] nums, int target) {
		if (nums == null || nums.length == 0)
			return -1;

		int begin = 0;
		int end = nums.length - 1;
		while (begin < end) {
			int mid = (begin + end) / 2;
			if (nums[mid] == target)
				return mid;

			if (nums[mid] > nums[end]) {
				if (target < nums[mid] && target > nums[end])
					end = mid - 1;
				else
					begin = mid + 1;
			} else if (nums[mid] < nums[begin]) {
				if (target > nums[mid] && target < nums[begin])
					begin = mid + 1;
				else
					end = mid - 1;
			} else {
				if (target > nums[mid])
					begin = mid + 1;
				else
					end = mid - 1;
			}
		}
		if (nums[begin] == target)
			return begin;
		else
			return -1;

	}

	public static void main(String[] args) {
		int[] nums = { 4, 5, 6, 7, 0, 1, 2 };
		System.out.println(new Solution().search(nums, 5));
		System.out.println(new Solution().search(nums, 11));
	}

}