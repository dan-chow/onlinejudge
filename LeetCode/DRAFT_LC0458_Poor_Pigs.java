public class Solution {
	public int poorPigs(int buckets, int minutesToDie, int minutesToTest) {
		if (buckets == 1)
			return 0;
		int num = 1;
		int testNum = minutesToTest / minutesToDie + 1;
		int max = testNum;
		while (max < buckets) {
			max *= testNum;
			num++;
		}
		return num;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().poorPigs(1000, 15, 60));
	}
}