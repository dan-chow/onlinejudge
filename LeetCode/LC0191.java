public class Solution {
	// you need to treat n as an unsigned value
	public int hammingWeight(int n) {
		int digit = 1;
		int cnt = 0;
		for (int i = 0; i < 32; i++) {
			if ((n & digit) != 0)
				cnt++;

			digit = digit << 1;
		}
		return cnt;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().hammingWeight(11));
	}
}