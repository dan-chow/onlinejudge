public class Solution {
	public boolean isPerfectSquare(int num) {
		if (num == 1 || num == 4)
			return true;

		long numLong = num;

		long beg = 0, end = (numLong + 1) / 2;
		while (beg <= end) {
			long mid = (beg + end) / 2, tmp = mid * mid;

			if (tmp == numLong)
				return true;

			if (mid * mid < numLong)
				beg = mid + 1;
			else
				end = mid - 1;
		}
		return false;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().isPerfectSquare(16));
		System.out.println(new Solution().isPerfectSquare(14));
	}
}