public class Solution {
	public int arrayNesting(int[] nums) {
		boolean visited[] = new boolean[nums.length];

		int maxLen = Integer.MIN_VALUE;

		for (int i = 0; i < nums.length; i++) {
			int len = 0;

			while (i < nums.length && visited[i])
				i++;

			if (i >= nums.length)
				break;

			int n = i;

			while (true) {
				visited[n] = true;
				if (len > nums.length)
					break;

				n = nums[n];
				len++;

				if (n == i) {
					if (len > maxLen)
						maxLen = len;

					break;
				}
			}
		}
		return maxLen;
	}

	public static void main(String[] args) {

		int[] nums = { 5, 4, 0, 3, 1, 6, 2 };

		System.out.println(new Solution().arrayNesting(nums));
	}
}