public class Solution {
	public int countPrimes(int n) {
		if (n <= 2)
			return 0;
		boolean[] isPrime = new boolean[n / 2];
		for (int i = 0; i < isPrime.length; i++)
			isPrime[i] = true;
		isPrime[0] = false;

		int firstPrime = 1;
		while (firstPrime < isPrime.length) {
			int step = 2 * firstPrime + 1;
			for (int j = firstPrime + step; j < isPrime.length; j += step)
				isPrime[j] = false;

			do {
				firstPrime++;
			} while (firstPrime < isPrime.length && !isPrime[firstPrime]);
		}

		int cnt = 1;
		for (int i = 0; i < isPrime.length; i++)
			if (isPrime[i])
				cnt++;

		return cnt;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().countPrimes(97));
	}
}