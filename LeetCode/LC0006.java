public class Solution {
	public String convert(String s, int numRows) {
		if (s == null || s.length() == 0 || numRows <= 0)
			return "";

		if (numRows == 1)
			return s;

		StringBuffer[] sbs = new StringBuffer[numRows];
		for (int i = 0; i < numRows; i++)
			sbs[i] = new StringBuffer();

		int strIdx = 0;
		int sbIdx = 0;
		boolean inc = true;
		while (strIdx < s.length()) {
			if (inc) {
				sbs[sbIdx++].append(s.charAt(strIdx++));
				if (sbIdx == numRows) {
					inc = !inc;
					sbIdx -= 2;
				}
			} else {
				sbs[sbIdx--].append(s.charAt(strIdx++));
				if (sbIdx == -1) {
					inc = !inc;
					sbIdx += 2;
				}
			}
		}

		StringBuffer res = new StringBuffer();
		for (StringBuffer sb : sbs) {
			res.append(sb);
		}
		return res.toString();
	}

	public static void main(String[] args) {
		System.out.println(new Solution().convert("PAYPALISHIRING", 3));
	}
}