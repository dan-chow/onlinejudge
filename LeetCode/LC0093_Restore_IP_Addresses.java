import java.util.ArrayList;
import java.util.List;

public class Solution {
	public List<String> restoreIpAddresses(String s) {
		List<String> res = new ArrayList<>();
		StringBuffer sb = new StringBuffer();
		restoreIpAddresses(s, 0, 4, sb, res);
		return res;
	}

	private void restoreIpAddresses(String s, int startIdx, int dotCnt, StringBuffer sb, List<String> res) {
		if (startIdx == s.length()) {
			if (dotCnt == 0)
				res.add(new String(sb).substring(0, sb.length() - 1));
			return;
		}

		if (s.charAt(startIdx) == '0') {
			if (s.length() - startIdx - 1 < dotCnt - 1 || s.length() - startIdx - 1 > 3 * (dotCnt - 1))
				return;

			sb.append('0').append('.');
			restoreIpAddresses(s, startIdx + 1, dotCnt - 1, sb, res);
			sb.setLength(sb.length() - 2);
			return;
		}

		int num = 0;
		for (int i = startIdx; i < Math.min(startIdx + 3, s.length()); i++) {
			num = num * 10 + s.charAt(i) - '0';

			if ((s.length() - i - 1) > 3 * (dotCnt - 1))
				continue;

			if (num > 255 || (s.length() - i - 1 < dotCnt - 1))
				break;

			sb.append(num).append('.');
			restoreIpAddresses(s, i + 1, dotCnt - 1, sb, res);
			sb.setLength(sb.length() - String.valueOf(num).length() - 1);
		}
	}

	public static void main(String[] args) {
		System.out.println(new Solution().restoreIpAddresses("25525511135"));
		System.out.println(new Solution().restoreIpAddresses("010010"));
		System.out.println(new Solution().restoreIpAddresses("0000"));
		System.out.println(new Solution().restoreIpAddresses(""));
	}
}