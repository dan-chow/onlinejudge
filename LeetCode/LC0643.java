public class Solution {
	public double findMaxAverage(int[] nums, int k) {
		if (k <= 0 || nums == null || nums.length < k)
			return 0;

		double sum = 0.0;
		for (int i = 0; i < k; i++)
			sum += nums[i];

		double max = sum / k;
		for (int i = k; i < nums.length; i++) {
			sum = sum - nums[i - k] + nums[i];
			if (sum / k > max)
				max = sum / k;
		}
		return max;
	}

	public static void main(String[] args) {
		int[] nums = { 1, 12, -5, -6, 50, 3 };
		System.out.println(new Solution().findMaxAverage(nums, 4));
	}
}