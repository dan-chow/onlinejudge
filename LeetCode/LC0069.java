public class Solution {
	public int mySqrt(int x) {
		long low = 0;
		long high = x;
		while (low < high) {
			long mid = (low + high) / 2;

			if (mid * mid == x)
				return (int) mid;
			else if (mid * mid > x)
				high = mid - 1;
			else
				low = low == mid ? mid + 1 : mid;
		}
		return (int) (low * low > x ? low - 1 : low);
	}

	public static void main(String[] args) {
		System.out.println(new Solution().mySqrt(4));
		System.out.println(new Solution().mySqrt(5));
		System.out.println(new Solution().mySqrt(2147395599));
	}
}