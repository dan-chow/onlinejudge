import java.util.PriorityQueue;

public class Solution {
	public int findKthLargest(int[] nums, int k) {
		PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
		for (int n : nums) {
			priorityQueue.add(n);
			if (priorityQueue.size() > k)
				priorityQueue.poll();
		}
		return priorityQueue.poll();
	}

	public static void main(String[] args) {
		int[] nums = { 3, 2, 1, 5, 6, 4 };
		System.out.println(new Solution().findKthLargest(nums, 2));
	}
}