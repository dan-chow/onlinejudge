public class Solution {

	public static class ListNode {
		int val;
		ListNode next;

		ListNode(int x) {
			val = x;
		}
	}

	public ListNode removeNthFromEnd(ListNode head, int n) {

		ListNode q = head;

		while (n > 0) {
			q = q.next;
			n--;
		}

		if (q == null) {
			head = head.next;
			return head;
		}

		ListNode p = head;
		while (q.next != null) {
			p = p.next;
			q = q.next;
		}

		p.next = p.next.next;

		return head;
	}

	public static void main(String[] args) {
		ListNode head = new ListNode(1);
		head.next = new ListNode(2);
		head.next.next = new ListNode(3);
		head.next.next.next = new ListNode(4);
		head.next.next.next.next = new ListNode(5);

		ListNode res = new Solution().removeNthFromEnd(head, 2);
		ListNode p = res;
		while (p != null) {
			System.out.println(p.val);
			p = p.next;
		}
	}
}