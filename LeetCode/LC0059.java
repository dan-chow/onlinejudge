public class Solution {
	public final static int L2R = 0;
	public final static int T2B = 1;
	public final static int R2L = 2;
	public final static int B2T = 3;

	public int[][] generateMatrix(int n) {
		int num = 1;
		int max = n * n;

		int top = -1, bottom = n, left = -1, right = n;

		int[][] matrix = new int[n][n];

		int i = 0, j = 0;
		int state = L2R;
		while (num <= max) {
			switch (state) {
			case L2R:
				matrix[i][j++] = num++;
				if (j == right) {
					top++;
					i++;
					j--;
					state = (state + 1) % 4;
				}
				break;
			case T2B:
				matrix[i++][j] = num++;
				if (i == bottom) {
					right--;
					j--;
					i--;
					state = (state + 1) % 4;
				}
				break;
			case R2L:
				matrix[i][j--] = num++;
				if (j == left) {
					bottom--;
					j++;
					i--;
					state = (state + 1) % 4;
				}
				break;
			case B2T:
				matrix[i--][j] = num++;
				if (i == top) {
					left++;
					i++;
					j++;
					state = (state + 1) % 4;
				}
				break;
			default:
				break;
			}
		}
		return matrix;
	}

	public static void main(String[] args) {
		int[][] res = new Solution().generateMatrix(3);
		for (int[] line : res) {
			for (int i : line) {
				System.out.print(i + " ");
			}
			System.out.println();
		}
	}
}