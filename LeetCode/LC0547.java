import java.util.LinkedList;
import java.util.Queue;

public class Solution {
	public int findCircleNum(int[][] M) {
		if (M == null || M.length == 0)
			return 0;

		int n = M.length;

		boolean[] visited = new boolean[n];

		Queue<Integer> queue = new LinkedList<>();
		int cnt = 0;
		for (int node = 0; node < M.length; node++) {
			if (!visited[node]) {
				cnt++;
				queue.add(node);

				while (queue.size() > 0) {
					int tmp = queue.poll();
					visited[tmp] = true;
					for (int to = 0; to < n; to++) {
						if (M[tmp][to] == 1 && !visited[to])
							queue.add(to);
					}
				}
			}
		}
		return cnt;
	}

	public static void main(String[] args) {
		int[][] M1 = { { 1, 1, 0 }, { 1, 1, 0 }, { 0, 0, 1 } };
		System.out.println(new Solution().findCircleNum(M1));

		int[][] M2 = { { 1, 1, 0 }, { 1, 1, 1 }, { 0, 1, 1 } };
		System.out.println(new Solution().findCircleNum(M2));
	}
}