public class Solution {
	// add 1 to n-1 elements is the same as subtract 1 from 1 element
	public int minMoves(int[] nums) {
		int min = Integer.MAX_VALUE;
		for (int n : nums)
			min = Math.min(min, n);

		int moves = 0;
		for (int n : nums)
			moves += (n - min);
		return moves;
	}

	public static void main(String[] args) {
		int[] nums = { 1, 2, 3 };
		System.out.println(new Solution().minMoves(nums));
	}
}