public class Solution {
	public boolean isUgly(int num) {
		if (num == 0)
			return false;

		while (num % 5 == 0)
			num /= 5;

		while (num % 3 == 0)
			num /= 3;

		while (num % 2 == 0)
			num /= 2;

		return num == 1;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().isUgly(6));
		System.out.println(new Solution().isUgly(8));
		System.out.println(new Solution().isUgly(14));
	}
}