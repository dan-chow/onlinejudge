public class Solution {
	public String multiply(String num1, String num2) {
		if (num1.length() < num2.length())
			return multiply(num2, num1);

		if ("0".equals(num2))
			return num2;

		String result = "0";

		int tailLen = 0;
		for (int i = num2.length() - 1; i >= 0; i--) {
			StringBuffer tmpSum = new StringBuffer();
			
			for (int t = 0; t < tailLen; t++)
				tmpSum.append(0);

			tailLen++;

			int m = num2.charAt(i) - '0';
			if (m == 0)
				continue;

			int carray = 0;
			for (int j = num1.length() - 1; j >= 0; j--) {
				int tmp = (num1.charAt(j) - '0') * m + carray;
				tmpSum.insert(0, (char) ((tmp % 10) + '0'));
				carray = tmp / 10;
			}

			if (carray > 0)
				tmpSum.insert(0, (char) (carray % 10 + '0'));

			result = sum(result, tmpSum.toString());
		}
		return result;
	}

	private String sum(String num1, String num2) {
		if (num1.length() > num2.length())
			return sum(num2, num1);

		StringBuffer newNum1 = new StringBuffer(num1);
		for (int i = 0; i < num2.length() - num1.length(); i++)
			newNum1.insert(0, 0);

		StringBuffer sum = new StringBuffer();
		int carray = 0;
		for (int i = newNum1.length() - 1; i >= 0; i--) {
			int tmp = (newNum1.charAt(i) - '0') + (num2.charAt(i) - '0') + carray;
			sum.insert(0, (char) (tmp % 10 + '0'));
			carray = tmp / 10;
		}

		if (carray > 0)
			sum.insert(0, carray);

		return sum.toString();
	}

	public static void main(String[] args) {
		System.out.println(new Solution().multiply("123", "456"));
	}

}