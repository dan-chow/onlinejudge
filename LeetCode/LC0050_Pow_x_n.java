public class Solution {
	public double myPow(double x, int n) {
		if (x == 1)
			return 1;

		if (n < 0)
			return 1 / (x * myPow(x, -(n + 1)));

		double result = 1;
		double tmp = x;
		while (n > 0) {
			if (n % 2 == 1) {
				result *= tmp;
			}
			tmp *= tmp;
			n /= 2;
		}
		return result;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().myPow(5, -1));
	}
}