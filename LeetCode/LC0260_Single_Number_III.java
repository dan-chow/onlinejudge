public class Solution {
	public int[] singleNumber(int[] nums) {
		int xor = 0;
		for (int i : nums)
			xor ^= i;

		int diff = 1;
		for (int i = 0; i < 32; i++) {
			if ((xor & (diff)) != 0)
				break;
			diff = diff << 1;
		}

		int[] res = new int[2];
		for (int i : nums) {
			if ((i & diff) == 0) {
				res[0] ^= i;
			} else {
				res[1] ^= i;
			}
		}
		return res;
	}

	public static void main(String[] args) {
		int[] nums = { 1, 2, 1, 3, 2, 5 };
		int[] res = new Solution().singleNumber(nums);
		for (int i : res)
			System.out.println(i);
	}
}