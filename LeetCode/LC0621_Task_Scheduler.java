import java.util.Arrays;

public class Solution {
	public int leastInterval(char[] tasks, int n) {
		n++;
		int[] arr = new int[26];
		for (char c : tasks)
			arr[c - 'A']++;

		int taskCnt = 0;
		for (int i = 0; i < 26; i++)
			taskCnt += (arr[i] != 0) ? 1 : 0;

		Arrays.sort(arr);

		int cnt = 0;
		while (taskCnt > n) {
			cnt += n;
			for (int i = 0; i < n; i++) {
				if (--arr[25 - i] == 0) {
					taskCnt--;
				}
			}
			Arrays.sort(arr);
		}

		while (taskCnt > 0) {
			int remove = 0;
			for (int i = 25; i >= 0; i--) {
				if (arr[i] == 0)
					break;

				if (--arr[i] == 0)
					remove++;
			}

			if (remove == taskCnt) {
				cnt += remove;
			} else {
				cnt += n;
			}

			taskCnt -= remove;

			Arrays.sort(arr);
		}
		return cnt;
	}

	public static void main(String[] args) {
		char[] tasks = { 'A', 'A', 'A', 'B', 'B', 'B' };
		System.out.println(new Solution().leastInterval(tasks, 3));
	}
}