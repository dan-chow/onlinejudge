public class Solution {
	public int findMaxConsecutiveOnes(int[] nums) {
		int i = 0;
		int j = 0;
		int maxLen = 0;
		while (true) {

			while (i < nums.length && nums[i] != 1)
				i++;
			j = i;
			while (j < nums.length && nums[j] == 1)
				j++;

			maxLen = maxLen > (j - i) ? maxLen : (j - i);

			if (i == nums.length || j == nums.length)
				break;

			i = j;
		}
		return maxLen;
	}

	public static void main(String[] args) {
		int[] nums = { 1, 1, 0, 1, 1, 1 };
		System.out.println(new Solution().findMaxConsecutiveOnes(nums));
	}
}