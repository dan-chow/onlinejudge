class ListNode {
	int val;
	ListNode next;

	ListNode(int x) {
		val = x;
	}
}

public class Solution {
	public ListNode swapPairs(ListNode head) {
		ListNode p;
		ListNode q;

		ListNode fakeHeader = new ListNode(-1);
		ListNode r = fakeHeader;
		while (head != null && head.next != null) {
			p = head;
			q = head.next;

			head = head.next.next;

			r.next = q;
			r = r.next;
			r.next = p;
			r = r.next;
		}

		if (head != null) {
			r.next = head;
			r = r.next;
		}

		r.next = null;

		return fakeHeader.next;
	}

	public static void main(String[] args) {
		ListNode head = new ListNode(1);
		head.next = new ListNode(2);
		head.next.next = new ListNode(3);
		head.next.next.next = new ListNode(4);

		ListNode result = new Solution().swapPairs(head);
		ListNode p = result;
		while (p != null) {
			System.out.println(p.val);
			p = p.next;
		}
	}
}