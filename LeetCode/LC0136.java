public class Solution {
	public int singleNumber(int[] nums) {
		int res = 0;
		for (int i : nums)
			res ^= i;
		return res;
	}

	public static void main(String[] args) {
		int[] nums = { 1, 1, 2, 2, 3, 4, 4 };
		System.out.println(new Solution().singleNumber(nums));
	}
}