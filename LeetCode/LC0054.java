import java.util.ArrayList;
import java.util.List;

public class Solution {
	public final static int L2R = 0;
	public final static int T2B = 1;
	public final static int R2L = 2;
	public final static int B2T = 3;

	public List<Integer> spiralOrder(int[][] matrix) {

		List<Integer> res = new ArrayList<>();
		if (matrix == null || matrix.length == 0)
			return res;
		int m = matrix.length;
		int n = matrix[0].length;

		int max = m * n;
		int top = -1, bottom = m, left = -1, right = n;
		int i = 0, j = 0, state = L2R;

		while (res.size() < max) {
			switch (state) {
			case L2R:
				res.add(matrix[i][j++]);
				if (j == right) {
					top++;
					i++;
					j--;
					state = (state + 1) % 4;
				}
				break;
			case T2B:
				res.add(matrix[i++][j]);
				if (i == bottom) {
					right--;
					j--;
					i--;
					state = (state + 1) % 4;
				}
				break;
			case R2L:
				res.add(matrix[i][j--]);
				if (j == left) {
					bottom--;
					j++;
					i--;
					state = (state + 1) % 4;
				}
				break;
			case B2T:
				res.add(matrix[i--][j]);
				if (i == top) {
					left++;
					i++;
					j++;
					state = (state + 1) % 4;
				}
				break;
			default:
				break;
			}
		}
		return res;
	}

	public static void main(String[] args) {
		int[][] matrix = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		List<Integer> res = new Solution().spiralOrder(matrix);
		for (int i : res)
			System.out.print(i + " ");

	}
}