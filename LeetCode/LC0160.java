class ListNode {
	int val;
	ListNode next;

	ListNode(int x) {
		val = x;
		next = null;
	}
}

public class Solution {
	public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
		if (headA == null || headB == null)
			return null;

		int len1 = getLength(headA);
		int len2 = getLength(headB);
		ListNode p = null, q = null;
		if (len1 > len2) {
			p = headA;
			q = headB;
		} else {
			p = headB;
			q = headA;
		}

		for (int i = 0; i < Math.abs(len1 - len2); i++)
			p = p.next;

		while (p != null && q != null && p != q) {
			p = p.next;
			q = q.next;
		}

		return p;
	}

	private int getLength(ListNode head) {
		int len = 0;
		ListNode p = head;
		while (p != null) {
			len++;
			p = p.next;
		}
		return len;
	}

	public static void main(String[] args) {
		ListNode headA = new ListNode(1);
		headA.next = new ListNode(2);
		headA.next.next = new ListNode(3);
		headA.next.next.next = new ListNode(4);

		ListNode headB = new ListNode(5);
		headB.next = headA.next.next;

		ListNode p = new Solution().getIntersectionNode(headA, headB);
		if (p != null)
			System.out.println(p.val);
	}
}