import java.util.Stack;

public class Solution {

	class Node {
		int idx;
		int val;

		public Node(int idx, int val) {
			this.idx = idx;
			this.val = val;
		}
	}

	public int[] nextGreaterElements(int[] nums) {

		int[] res = new int[nums.length];
		for (int i = 0; i < res.length; i++) {
			res[i] = -1;
		}

		Stack<Node> stack = new Stack<>();

		int iterCnt = 0;
		while (iterCnt < 2 * nums.length) {
			int i = iterCnt % nums.length;

			if (stack.isEmpty() || stack.peek().val > nums[i]) {
				stack.push(new Node(i, nums[i]));
			} else {
				while (!stack.isEmpty() && stack.peek().val < nums[i]) {
					Node node = stack.pop();
					if (res[node.idx] == -1) {
						res[node.idx] = nums[i];
					}
				}
				stack.push(new Node(i, nums[i]));
			}
			iterCnt++;
		}

		return res;
	}

	public static void main(String[] args) {
		int[] nums = { 1, 2, 1 };
		int[] res = new Solution().nextGreaterElements(nums);
		for (int i : res) {
			System.out.println(i);
		}
	}
}