import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Interval {
	int start;
	int end;

	Interval() {
		start = 0;
		end = 0;
	}

	Interval(int s, int e) {
		start = s;
		end = e;
	}
}

class Point implements Comparable<Point> {
	public final static int LEFT = 0;
	public final static int RIGHT = 1;

	int x;
	int label;

	public Point(int x, int label) {
		this.x = x;
		this.label = label;
	}

	@Override
	public int compareTo(Point other) {
		if (this.x != other.x)
			return this.x - other.x;
		return this.label - other.label;
	}

}

public class Solution {
	public List<Interval> merge(List<Interval> intervals) {
		List<Point> points = new ArrayList<>();
		for (Interval interval : intervals) {
			points.add(new Point(interval.start, Point.LEFT));
			points.add(new Point(interval.end, Point.RIGHT));
		}

		Collections.sort(points);

		List<Interval> res = new ArrayList<>();
		int start = 0;
		int cnt = 0;
		for (int i = 0; i < points.size(); i++) {
			if (cnt == 0)
				start = points.get(i).x;

			if (points.get(i).label == Point.LEFT)
				cnt++;
			else
				cnt--;

			if (cnt == 0)
				res.add(new Interval(start, points.get(i).x));
		}
		return res;
	}

	public static void main(String[] args) {
		List<Interval> intervals = new ArrayList<>();
		intervals.add(new Interval(1, 3));
		intervals.add(new Interval(2, 6));
		intervals.add(new Interval(8, 10));
		intervals.add(new Interval(15, 18));

		List<Interval> res = new Solution().merge(intervals);
		for (Interval interval : res)
			System.out.println("[" + interval.start + "," + interval.end + "]");
	}
}