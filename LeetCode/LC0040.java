import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution {
	public List<List<Integer>> combinationSum2(int[] candidates, int target) {
		List<List<Integer>> res = new ArrayList<>();
		if (candidates == null || candidates.length == 0)
			return res;

		Arrays.sort(candidates);

		List<Integer> tmp = new ArrayList<>();
		combinationSum2(candidates, 0, target, tmp, res);
		return res;
	}

	private void combinationSum2(int[] candidates, int start, int target, List<Integer> tmp, List<List<Integer>> res) {

		if (target == 0) {
			res.add(new ArrayList<Integer>(tmp));
			return;
		}

		for (int i = start; i < candidates.length; i++) {
			if (i > start && candidates[i] == candidates[i - 1])
				continue;

			if (candidates[i] > target)
				break;

			tmp.add(candidates[i]);
			combinationSum2(candidates, i + 1, target - candidates[i], tmp, res);
			tmp.remove(tmp.size() - 1);
		}
	}

	public static void main(String[] args) {
		int[] candidates = { 10, 1, 2, 7, 6, 1, 5 };
		List<List<Integer>> res = new Solution().combinationSum2(candidates, 8);
		for (List<Integer> list : res) {
			System.out.println(list);
		}
	}
}
