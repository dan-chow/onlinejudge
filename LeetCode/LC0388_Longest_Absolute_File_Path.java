import java.util.Stack;

public class Solution {

	public int lengthLongestPath(String input) {
		String[] lines = input.split("\n");

		int currLen = 0;
		int maxLen = 0;

		Stack<Integer> stack = new Stack<>();

		for (String line : lines) {
			int depth = line.lastIndexOf("\t") + 1;
			int len = line.length() - depth + 1;

			while (stack.size() > depth) {
				currLen -= stack.pop();
			}

			currLen += len;
			stack.push(len);

			if (line.contains(".")) {
				maxLen = Math.max(currLen - 1, maxLen);
			}
		}

		return maxLen;

	}

	public static void main(String[] args) {
		String tmp = "dir\n\tsubdir1\n\tsubdir2\n\t\tfile.ext";
		System.out.println(new Solution().lengthLongestPath(tmp));
	}
}