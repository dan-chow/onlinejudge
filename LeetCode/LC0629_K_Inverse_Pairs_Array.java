public class Solution {
	public int kInversePairs(int n, int k) {
		if (k < 0 || k > n * (n - 1) / 2)
			return 0;

		if (k == 0 || k == n * (n - 1) / 2)
			return 1;

		long mod = 1000000007;

		long[][] dp = new long[n + 1][k + 1];

		// put n as the nth, (n-1)th, ..., 1st number
		// dp[n][k] = dp[n-1][k] + dp[n-1][k-1] + ... + dp[n-1][k-(n-1)]

		// dp[n][k+1] - dp[n][k] = dp[n-1][k+1] - dp[n-1][k+1-n];

		for (int j = 0; j <= k; j++) {
			dp[0][j] = 0;
		}

		for (int i = 1; i <= n; i++) {
			dp[i][0] = 1;
		}

		for (int i = 2; i <= n; i++) {
			for (int j = 1; j <= k; j++) {
				long tmp = (j - i >= 0) ? dp[i - 1][j - i] : 0;
				dp[i][j] = dp[i][j - 1] + dp[i - 1][j] - tmp;
				dp[i][j] = (dp[i][j] + mod) % mod;
			}
		}

		return (int) dp[n][k];
	}

	public static void main(String[] args) {
		System.out.println(new Solution().kInversePairs(3, 0));
		System.out.println(new Solution().kInversePairs(3, 1));
	}
}