import java.util.HashMap;
import java.util.Random;

public class Solution {

	HashMap<Integer, int[]> map;

	public Solution(int[] nums) {
		map = new HashMap<>();

		int lastNum = nums[0];

		int[] idx = { 0, 0 };
		for (int i = 1; i < nums.length; i++) {
			if (nums[i] != lastNum) {

				map.put(lastNum, idx.clone());

				lastNum = nums[i];
				idx[0] = i;
				idx[1] = i;
			} else {
				idx[1]++;
			}

		}
		map.put(lastNum, idx.clone());
	}

	public int pick(int target) {
		int[] idx = map.get(target);
		if (idx == null) {
			return -1;
		}
		return idx[0] + new Random().nextInt(idx[1] - idx[0] + 1);
	}

	public static void main(String[] args) {
		int[] nums = new int[] { 1, 2, 3, 3, 3 };

		Solution s = new Solution(nums);
		System.out.println(s.pick(3));
		System.out.println(s.pick(1));

	}
}