public class Solution {
	public void moveZeroes(int[] nums) {
		int i = 0;
		while (i < nums.length && nums[i] != 0)
			i++;

		int j = i + 1;
		while (j < nums.length && nums[j] == 0)
			j++;

		while (j < nums.length) {
			nums[i] = nums[j];
			i++;
			j++;

			while (j < nums.length && nums[j] == 0)
				j++;
		}

		for (; i < nums.length; i++) {
			nums[i] = 0;
		}
	}

	public static void main(String[] args) {
		int[] nums = { 0, 1, 0, 3, 12 };
		new Solution().moveZeroes(nums);
		for (int i : nums) {
			System.out.println(i);
		}
	}
}