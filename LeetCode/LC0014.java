import java.util.Arrays;

public class Solution {
	public String longestCommonPrefix(String[] strs) {

		if (strs == null || strs.length == 0)
			return "";
		StringBuilder sb = new StringBuilder();
		Arrays.sort(strs);
		String a = strs[0];
		String b = strs[strs.length - 1];
		for (int i = 0; i < Math.min(a.length(), b.length()); i++) {
			if (a.charAt(i) == b.charAt(i))
				sb.append(a.charAt(i));
			else
				return sb.toString();
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		String[] strs = { "ab", "abcd", "abc", "abaa" };
		System.out.println(new Solution().longestCommonPrefix(strs));
	}
}