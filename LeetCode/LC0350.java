import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution {
	public int[] intersect(int[] nums1, int[] nums2) {
		Arrays.sort(nums1);
		Arrays.sort(nums2);

		int i1 = 0;
		int i2 = 0;

		List<Integer> list = new ArrayList<>();
		while (i1 < nums1.length && i2 < nums2.length) {
			if (nums1[i1] == nums2[i2]) {
				list.add(nums1[i1]);
				i1++;
				i2++;
			} else if (nums1[i1] < nums2[i2]) {
				i1++;
			} else {
				i2++;
			}
		}
		int[] arr = new int[list.size()];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = list.get(i);
		}
		return arr;
	}

	public static void main(String[] args) {
		int[] nums1 = { 1, 2, 2, 1 };
		int[] nums2 = { 2, 2 };
		int[] res = new Solution().intersect(nums1, nums2);
		for (int i : res)
			System.out.println(i);
	}
}