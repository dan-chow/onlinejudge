import java.util.LinkedList;
import java.util.Queue;

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Codec {

	// Encodes a tree to a single string.
	public String serialize(TreeNode root) {
		if (root == null)
			return "#";

		StringBuffer sb = new StringBuffer();
		sb.append(root.val).append(",");
		sb.append(serialize(root.left));
		sb.append(",");
		sb.append(serialize(root.right));
		return sb.toString();
	}

	// Decodes your encoded data to tree.
	public TreeNode deserialize(String data) {
		if (data.equals("#"))
			return null;
		Queue<String> queue = new LinkedList<>();
		String[] tokens = data.split(",");
		for (String str : tokens)
			queue.add(str);

		return buildTree(queue);
	}

	private TreeNode buildTree(Queue<String> nodeStrQueue) {
		if (nodeStrQueue == null || nodeStrQueue.size() == 0)
			return null;

		String nodeStr = nodeStrQueue.poll();
		if (nodeStr.equals("#"))
			return null;

		TreeNode root = new TreeNode(Integer.parseInt(nodeStr));
		root.left = buildTree(nodeStrQueue);
		root.right = buildTree(nodeStrQueue);
		return root;
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(3);
		root.right.left = new TreeNode(4);
		root.right.right = new TreeNode(5);

		String data = new Codec().serialize(root);
		System.out.println(new Codec().serialize(root));
		root = new Codec().deserialize(data);

		Queue<TreeNode> queue = new LinkedList<>();
		queue.add(root);
		while (!queue.isEmpty()) {
			int levelSize = queue.size();
			while (levelSize-- > 0) {
				TreeNode node = queue.poll();
				System.out.print(node.val + " ");
				if (node.left != null)
					queue.add(node.left);
				if (node.right != null)
					queue.add(node.right);
			}
			System.out.println();
		}
	}
}
