import java.util.ArrayList;
import java.util.List;

public class Solution {
	public List<Integer> grayCode(int n) {
		List<Integer> res = new ArrayList<>();
		res.add(0);

		// X00 -- X01 -- X11 -- X10
		// 000 -> 001 -> 011 -> 010
		// 100 <- 101 <- 111 <- 110

		for (int i = 0; i < n; i++) {
			for (int j = res.size() - 1; j >= 0; j--) {
				res.add(res.get(j) ^ (1 << i));
			}
		}
		return res;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().grayCode(2));
	}
}