
class ListNode {
	int val;
	ListNode next;

	ListNode(int x) {
		val = x;
	}
}

public class Solution {
	
	// less effective, but without reverse list
	public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		if (l1 == null)
			return l2;
		if (l2 == null)
			return l1;

		if (getLen(l1) < getLen(l2)) {
			ListNode tmp = l1;
			l1 = l2;
			l2 = tmp;
		}

		int carry = getCarry(l1, l2);
		if (carry != 0) {
			ListNode head = new ListNode(carry);
			head.next = l1;
			return head;
		}
		return l1;
	}

	private int getLen(ListNode head) {
		int len = 0;
		ListNode p = head;
		while (p != null) {
			len++;
			p = p.next;
		}
		return len;
	}

	private int getCarry(ListNode l1, ListNode l2) {
		if (l1 == null && l2 == null)
			return 0;

		if (getLen(l1) > getLen(l2)) {
			int tmpSum = l1.val + getCarry(l1.next, l2);
			l1.val = tmpSum % 10;
			return tmpSum / 10;
		} else {
			int tmpSum = l1.val + l2.val + getCarry(l1.next, l2.next);
			l1.val = tmpSum % 10;
			return tmpSum / 10;
		}
	}

	public static void main(String[] args) {
		ListNode l1 = new ListNode(7);
		l1.next = new ListNode(2);
		l1.next.next = new ListNode(4);
		l1.next.next.next = new ListNode(3);

		ListNode l2 = new ListNode(5);
		l2.next = new ListNode(6);
		l2.next.next = new ListNode(4);

		ListNode res = new Solution().addTwoNumbers(l1, l2);
		ListNode p = res;
		while (p != null) {
			System.out.println(p.val);
			p = p.next;
		}
	}
}