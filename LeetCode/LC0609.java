import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Solution {
	public List<List<String>> findDuplicate(String[] paths) {
		HashMap<String, List<String>> map = new HashMap<>();

		for (String path : paths) {
			String[] tokens = path.split(" ");
			for (int i = 1; i < tokens.length; i++) {
				String content = tokens[i].substring(tokens[i].indexOf("(") + 1, tokens[i].lastIndexOf(""));
				if (!map.containsKey(content))
					map.put(content, new ArrayList<String>());

				map.get(content).add(tokens[0] + "/" + tokens[i].substring(0, tokens[i].indexOf("(")));
			}
		}

		List<List<String>> res = new ArrayList<>();
		for (String content : map.keySet()) {
			if (map.get(content).size() > 1)
				res.add(map.get(content));
		}

		return res;
	}

	public static void main(String[] args) {
		String[] paths = { "root/a 1.txt(abcd) 2.txt(efgh)", "root/c 3.txt(abcd)", "root/c/d 4.txt(efgh)", "root 4.txt(efgh)" };
		List<List<String>> res = new Solution().findDuplicate(paths);
		for (List<String> list : res)
			System.out.print(list);
	}
}