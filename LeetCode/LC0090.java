import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution {
	public List<List<Integer>> subsetsWithDup(int[] nums) {
		Arrays.sort(nums);

		List<List<Integer>> res = new ArrayList<>();
		List<Integer> list = new ArrayList<>();
		subsetsWithDup(nums, 0, list, res);
		return res;
	}

	private void subsetsWithDup(int[] nums, int start, List<Integer> list, List<List<Integer>> res) {
		if (start > nums.length)
			return;

		res.add(new ArrayList<>(list));

		for (int i = start; i < nums.length; i++) {
			if (i > start && nums[i] == nums[i - 1])
				continue;

			list.add(nums[i]);
			subsetsWithDup(nums, i + 1, list, res);
			list.remove(list.size() - 1);
		}
	}

	public static void main(String[] args) {
		int[] nums = { 1, 2, 2 };
		List<List<Integer>> res = new Solution().subsetsWithDup(nums);
		System.out.println(res);
	}
}
