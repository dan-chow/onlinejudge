public class Solution {
	public int findLengthOfLCIS(int[] nums) {
		if (nums == null || nums.length == 0)
			return 0;

		int maxLen = 1;
		int i = 1;
		while (i < nums.length) {
			while (i < nums.length && nums[i] <= nums[i - 1])
				i++;

			if (i == nums.length)
				break;

			int len = 1;
			while (i < nums.length && (nums[i] - nums[i - 1]) > 0) {
				i++;
				len++;
			}

			maxLen = Math.max(maxLen, len);
		}

		return maxLen;
	}

	public static void main(String[] args) {
		int[] nums1 = { 1, 3, 5, 4, 7 };
		System.out.println(new Solution().findLengthOfLCIS(nums1));

		int[] nums2 = { 2, 2, 2, 2, 2 };
		System.out.println(new Solution().findLengthOfLCIS(nums2));
	}
}