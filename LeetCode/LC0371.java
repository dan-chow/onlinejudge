public class Solution {
	public int getSum(int a, int b) {

		int res = a ^ b;
		int carry = a & b;
		while (carry != 0) {
			int newCarry = carry << 1;
			carry = res & newCarry;
			res = res ^ newCarry;

		}
		return res;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().getSum(5, 6));
	}
}