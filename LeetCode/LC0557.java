public class Solution {
	public String reverseWords(String s) {
		StringBuffer sb = new StringBuffer();
		String[] tokens = s.split(" ");
		sb.append(reverse(tokens[0]));
		for (int i = 1; i < tokens.length; i++) {
			sb.append(" ").append(reverse(tokens[i]));
		}
		return sb.toString();
	}

	private String reverse(String str) {
		char[] strArr = str.toCharArray();
		for (int i = 0; i < strArr.length / 2; i++) {
			char tmp = strArr[i];
			strArr[i] = strArr[strArr.length - i - 1];
			strArr[strArr.length - i - 1] = tmp;
		}
		return (new String(strArr));
	}

	public static void main(String[] args) {
		String str = "Let's take LeetCode contest";
		System.out.println(new Solution().reverseWords(str));
	}
}