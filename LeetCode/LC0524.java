import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Solution {
	public String findLongestWord(String s, List<String> d) {
		d.sort(new Comparator<String>() {
			@Override
			public int compare(String str1, String str2) {
				if (str1.length() != str2.length())
					return str2.length() - str1.length();
				return str1.compareTo(str2);
			}
		});

		int[] idx = new int[d.size()];
		for (int i = 0; i < s.length(); i++) {
			for (int j = 0; j < d.size(); j++) {
				if (idx[j] < d.get(j).length() && s.charAt(i) == d.get(j).charAt(idx[j])) {
					idx[j]++;
				}
			}
		}

		for (int j = 0; j < d.size(); j++) {
			if (idx[j] == d.get(j).length())
				return d.get(j);
		}
		return "";
	}

	public static void main(String[] args) {
		List<String> d1 = new ArrayList<>();
		d1.add("ale");
		d1.add("apple");
		d1.add("monkey");
		d1.add("plea");

		System.out.println(new Solution().findLongestWord("abpcplea", d1));

		List<String> d2 = new ArrayList<>();
		d2.add("a");
		d2.add("b");
		d2.add("c");

		System.out.println(new Solution().findLongestWord("abpcplea", d2));
	}

}