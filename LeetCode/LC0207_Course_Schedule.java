import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

public class Solution {
	public boolean canFinish(int numCourses, int[][] prerequisites) {

		HashMap<Integer, HashSet<Integer>> map = new HashMap<>();
		for (int i = 0; i < numCourses; i++)
			map.put(i, new HashSet<Integer>());

		for (int[] edge : prerequisites) {
			if (!map.containsKey(edge[0]))
				map.put(edge[0], new HashSet<Integer>());

			map.get(edge[0]).add(edge[1]);
		}

		boolean[] visited = new boolean[numCourses];
		boolean[] visitStack = new boolean[numCourses];
		Queue<Integer> queue = new LinkedList<>();
		for (int i = 0; i < numCourses; i++) {
			if (visited[i])
				continue;

			Arrays.fill(visitStack, false);

			queue.clear();
			queue.add(i);

			while (queue.size() > 0) {
				int src = queue.poll();
				visited[src] = true;
				visitStack[src] = true;

				if (map.containsKey(src)) {
					for (int dst : map.get(src)) {
						if (visited[dst] && visitStack[dst])
							return false;

						if (!visited[dst])
							queue.add(dst);
					}
				}
			}

		}
		return true;
	}

	public static void main(String[] args) {
		int[][] prerequisites1 = { { 1, 0 } };
		System.out.println(new Solution().canFinish(2, prerequisites1));

		int[][] prerequisites2 = { { 1, 0 }, { 0, 1 } };
		System.out.println(new Solution().canFinish(2, prerequisites2));
	}

}