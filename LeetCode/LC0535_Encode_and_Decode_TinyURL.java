import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Codec {
	String dict = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM0123456789";

	Map<String, String> long2short = new HashMap<>();
	Map<String, String> short2long = new HashMap<>();

	// Encodes a URL to a shortened URL.
	public String encode(String longUrl) {

		while (!long2short.containsKey(longUrl)) {
			String shortUrl = randomString();
			if (short2long.containsKey(shortUrl))
				continue;

			short2long.put(shortUrl, longUrl);
			long2short.put(longUrl, shortUrl);
		}

		return long2short.get(longUrl);
	}

	private String randomString() {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < 7; i++) {
			int idx = new Random().nextInt(dict.length());
			sb.append(dict.charAt(idx));
		}
		return sb.toString();
	}

	// Decodes a shortened URL to its original URL.
	public String decode(String shortUrl) {
		return short2long.get(shortUrl);
	}

	public static void main(String[] args) {
		Codec codec = new Codec();
		String code = codec.encode("https://leetcode.com/problems/design-tinyurl");
		System.out.println(code);
		System.out.println(codec.decode(code));
	}
}
