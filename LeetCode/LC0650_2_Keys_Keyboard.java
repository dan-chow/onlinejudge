public class Solution {
	public int minSteps(int n) {
		int[] dp = new int[n + 1];
		for (int i = 2; i <= n; i++) {
			dp[i] = i;
			for (int maxDivisor = i - 1; maxDivisor > 1; maxDivisor--) {
				if (i % maxDivisor == 0) {
					dp[i] = dp[maxDivisor] + i / maxDivisor;
					break;
				}
			}
		}
		return dp[n];
	}

	public static void main(String[] args) {
		System.out.println(new Solution().minSteps(6));
	}
}