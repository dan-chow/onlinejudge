class ListNode {
	int val;
	ListNode next;

	ListNode(int x) {
		val = x;
	}
}

public class Solution {
	public void reorderList(ListNode head) {
		if (head == null || head.next == null)
			return;

		ListNode slow, fast;

		slow = head;
		fast = head.next;
		while (fast != null && fast.next != null) {
			slow = slow.next;
			fast = fast.next.next;
		}

		ListNode head2 = slow.next;
		slow.next = null;

		head2 = reverse(head2);
		merge(head, head2);
	}

	private ListNode reverse(ListNode head) {
		ListNode p, q;

		p = head.next;
		head.next = null;

		while (p != null) {
			q = p;
			p = p.next;

			q.next = head;
			head = q;
		}
		return head;
	}

	private ListNode merge(ListNode head1, ListNode head2) {
		ListNode p = head1;
		ListNode q = head2;
		ListNode r;
		while (q != null) {
			r = q;
			q = q.next;

			r.next = p.next;
			p.next = r;
			p = p.next.next;
		}

		return head1;
	}

	public static void main(String[] args) {
		ListNode head = new ListNode(1);
		head.next = new ListNode(2);
		head.next.next = new ListNode(3);
		head.next.next.next = new ListNode(4);

		new Solution().reorderList(head);
		ListNode p = head;
		while (p != null) {
			System.out.println(p.val);
			p = p.next;
		}
	}
}