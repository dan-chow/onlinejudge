import java.util.HashMap;

public class Solution {
	public String fractionToDecimal(int numerator, int denominator) {
		if (numerator == 0)
			return "0";

		StringBuffer res = new StringBuffer();

		res.append(((numerator > 0) ^ (denominator > 0)) ? "-" : "");

		long numeratorL = Math.abs((long) numerator);
		long denominatorL = Math.abs((long) denominator);

		res.append(numeratorL / denominatorL);
		numeratorL %= denominatorL;

		if (numeratorL == 0)
			return res.toString();

		res.append(".");
		HashMap<Long, Integer> map = new HashMap<Long, Integer>();
		map.put(numeratorL, res.length());
		while (numeratorL != 0) {
			numeratorL *= 10;
			res.append(numeratorL / denominatorL);
			numeratorL %= denominatorL;
			if (map.containsKey(numeratorL)) {
				res.insert(map.get(numeratorL), "(");
				res.append(")");
				break;
			} else {
				map.put(numeratorL, res.length());
			}
		}
		return res.toString();
	}

	public static void main(String[] args) {
		System.out.println(new Solution().fractionToDecimal(1, 2));
		System.out.println(new Solution().fractionToDecimal(2, 1));
		System.out.println(new Solution().fractionToDecimal(2, 3));
	}
}