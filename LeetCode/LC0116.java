import java.util.LinkedList;
import java.util.Queue;

class TreeLinkNode {
	int val;
	TreeLinkNode left, right, next;

	TreeLinkNode(int x) {
		val = x;
	}
}

public class Solution {
	public void connect(TreeLinkNode root) {
		if (root == null)
			return;

		Queue<TreeLinkNode> queue = new LinkedList<>();
		queue.add(root);
		int levelSize = 0;
		TreeLinkNode tmp = null;
		while (!queue.isEmpty()) {
			levelSize = queue.size();
			while (levelSize-- > 0) {
				tmp = queue.poll();
				if (levelSize > 0)
					tmp.next = queue.peek();
				if (tmp.left != null)
					queue.add(tmp.left);
				if (tmp.right != null)
					queue.add(tmp.right);
			}
		}
	}

	public static void main(String[] args) {
		TreeLinkNode root = new TreeLinkNode(1);
		root.left = new TreeLinkNode(2);
		root.left.left = new TreeLinkNode(4);
		root.left.right = new TreeLinkNode(5);
		root.right = new TreeLinkNode(3);
		root.right.left = new TreeLinkNode(6);
		root.right.right = new TreeLinkNode(7);

		new Solution().connect(root);

		TreeLinkNode p = root;
		while (p != null) {
			TreeLinkNode q = p;
			while (q != null) {
				System.out.print(q.val + " ");
				q = q.next;
			}
			System.out.println();
			p = p.left;
		}
	}
}