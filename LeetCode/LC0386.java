import java.util.ArrayList;
import java.util.List;

public class Solution {
	public List<Integer> lexicalOrder(int n) {
		List<Integer> list = new ArrayList<>();
		for (int i = 1; i <= 9; i++) {
			if (i <= n) {
				list.add(i);
				addToList(list, i, n);
			}
		}
		return list;
	}

	private void addToList(List<Integer> list, int prefix, int n) {
		if (prefix > n / 10)
			return;

		for (int i = 0; i <= 9; i++) {
			int tmp = prefix * 10 + i;
			if (tmp <= n) {
				list.add(tmp);
				addToList(list, tmp, n);
			}
		}
	}

	public static void main(String[] args) {
		List<Integer> list = new Solution().lexicalOrder(13);
		for (int i : list) {
			System.out.println(i);
		}
	}
}