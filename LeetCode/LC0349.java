import java.util.HashSet;

public class Solution {
	public int[] intersection(int[] nums1, int[] nums2) {

		HashSet<Integer> set1 = new HashSet<>();
		for (int i : nums1)
			set1.add(i);

		HashSet<Integer> set2 = new HashSet<>();
		for (int i : nums2)
			set2.add(i);

		set1.retainAll(set2);
		int[] res = new int[set1.size()];
		int idx = 0;
		for (int i : set1)
			res[idx++] = i;

		return res;
	}

	public static void main(String[] args) {
		int[] nums1 = { 1, 2, 2, 1 };
		int[] nums2 = { 2, 2 };
		int[] res = new Solution().intersection(nums1, nums2);
		for (int i : res)
			System.out.println(i);
	}
}