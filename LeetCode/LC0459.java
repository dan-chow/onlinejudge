public class Solution {
	public boolean repeatedSubstringPattern(String s) {
		for (int circle = 1; circle < s.length(); circle++) {
			if (s.length() % circle != 0)
				continue;

			boolean foundMatch = true;
			for (int startIdx = 0; startIdx < s.length(); startIdx++) {
				for (int nextIdx = startIdx + circle; nextIdx < s.length(); nextIdx += circle) {
					if (s.charAt(startIdx) != s.charAt(nextIdx)) {
						foundMatch = false;
						break;
					}
				}

				if (!foundMatch)
					break;
			}

			if (foundMatch)
				return true;
		}
		return false;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().repeatedSubstringPattern("abab"));
		System.out.println(new Solution().repeatedSubstringPattern("aba"));
		System.out.println(new Solution().repeatedSubstringPattern("abcabcabcabc"));
	}
}