import java.util.ArrayList;
import java.util.List;

public class Solution {
	public List<List<Integer>> subsets(int[] nums) {
		List<List<Integer>> res = new ArrayList<>();
		List<Integer> list = new ArrayList<>();
		subsets(nums, 0, list, res);
		return res;
	}

	private void subsets(int[] nums, int start, List<Integer> list, List<List<Integer>> res) {
		if (start > nums.length)
			return;

		res.add(new ArrayList<>(list));

		for (int i = start; i < nums.length; i++) {
			list.add(nums[i]);
			subsets(nums, i + 1, list, res);
			list.remove(list.size() - 1);
		}
	}

	public static void main(String[] args) {
		int[] nums = { 1, 2, 3 };
		List<List<Integer>> res = new Solution().subsets(nums);
		System.out.println(res);
	}

}