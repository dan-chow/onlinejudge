public class Solution {
	public int[] findErrorNums(int[] nums) {
		boolean[] dict = new boolean[nums.length];
		int dup = 0;
		int mis = 0;
		for (int n : nums) {
			if (!dict[n - 1])
				dict[n - 1] = true;
			else
				dup = n;
		}
		for (int i = 0; i < dict.length; i++)
			if (!dict[i])
				mis = i + 1;
		return new int[] { dup, mis };
	}

	public static void main(String[] args) {
		int[] nums = { 1, 2, 2 };
		int[] res = new Solution().findErrorNums(nums);
		for (int i : res)
			System.out.println(i);
	}
}