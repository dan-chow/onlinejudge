import java.util.Hashtable;

public class LRUCache {
	class DLinkNode {
		int key;
		int value;
		DLinkNode prev;
		DLinkNode next;
	}

	private void addNodeToHead(DLinkNode node) {
		node.next = this.head.next;
		this.head.next.prev = node;

		this.head.next = node;
		node.prev = this.head;
	}

	private void removeNode(DLinkNode node) {
		node.next.prev = node.prev;
		node.prev.next = node.next;
	}

	private int size;
	private int capacity;

	Hashtable<Integer, DLinkNode> table = null;
	DLinkNode head = null;
	DLinkNode tail = null;

	public LRUCache(int capacity) {
		this.size = 0;
		this.capacity = capacity;

		this.head = new DLinkNode();
		this.tail = new DLinkNode();
		this.table = new Hashtable<>();

		this.head.prev = null;
		this.head.next = this.tail;
		this.tail.prev = this.head;
		this.tail.next = null;
	}

	public int get(int key) {
		if (this.table.containsKey(key)) {
			DLinkNode node = this.table.get(key);
			removeNode(node);
			addNodeToHead(node);
			return node.value;
		}

		return -1;
	}

	public void put(int key, int value) {
		if (this.table.containsKey(key)) {
			DLinkNode node = this.table.get(key);
			removeNode(node);
			node.value = value;
			addNodeToHead(node);
			return;
		}

		if (this.size < this.capacity) {
			DLinkNode node = new DLinkNode();
			node.key = key;
			node.value = value;
			this.table.put(key, node);
			addNodeToHead(node);
			this.size++;
		} else {
			DLinkNode node = this.tail.prev;
			removeNode(node);
			this.table.remove(node.key);
			node.key = key;
			node.value = value;
			this.table.put(key, node);
			addNodeToHead(node);
		}

	}

	public static void main(String[] args) {

		LRUCache obj = new LRUCache(2);

		obj.put(1, 1);
		obj.put(2, 2);

		System.out.println(obj.get(1));

		obj.put(3, 3);

		System.out.println(obj.get(2));

		obj.put(4, 4);

		System.out.println(obj.get(1));
		System.out.println(obj.get(3));
		System.out.println(obj.get(4));

	}
}