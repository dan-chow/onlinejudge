import java.util.HashSet;

public class Solution {
	public String reverseVowels(String s) {
		HashSet<Character> vowels = new HashSet<>();
		vowels.add('a');
		vowels.add('e');
		vowels.add('i');
		vowels.add('o');
		vowels.add('u');
		vowels.add('A');
		vowels.add('E');
		vowels.add('I');
		vowels.add('O');
		vowels.add('U');

		char[] arr = s.toCharArray();
		int left = 0, right = arr.length - 1;

		while (left < right) {
			while (left < right && !vowels.contains(arr[left]))
				left++;
			while (right > left && !vowels.contains(arr[right]))
				right--;

			if (left >= right)
				break;

			char tmp = arr[left];
			arr[left] = arr[right];
			arr[right] = tmp;

			left++;
			right--;
		}

		return new String(arr);
	}

	public static void main(String[] args) {
		System.out.println(new Solution().reverseVowels("hello"));
		System.out.println(new Solution().reverseVowels("leetcode"));
	}
}