public class Solution {
	public boolean isPalindrome(String s) {
		s = s.toLowerCase();

		int left = 0;
		int right = s.length() - 1;
		while (left < right) {
			while (left < s.length() && !Character.isAlphabetic(s.charAt(left)) && !Character.isDigit(s.charAt(left)))
				left++;

			while (right >= 0 && !Character.isAlphabetic(s.charAt(right)) && !Character.isDigit(s.charAt(right)))
				right--;

			if (left > right)
				return true;

			if (s.charAt(left) == s.charAt(right)) {
				left++;
				right--;
			} else {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().isPalindrome("A man, a plan, a canal: Panama"));
		System.out.println(new Solution().isPalindrome("race a car"));
	}
}