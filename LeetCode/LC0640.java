class Coef {
	int a;
	int b;

	public Coef(int a, int b) {
		this.a = a;
		this.b = b;
	}
}

public class Solution {
	public String solveEquation(String equation) {

		String[] tokens = equation.split("=");
		Coef left = getCoef(tokens[0]);
		Coef right = getCoef(tokens[1]);

		System.out.println(left.a + " " + left.b);
		System.out.println(right.a + " " + right.b);

		if (left.a == right.a)
			if (left.b == right.b)
				return "Infinite solutions";
			else
				return "No solution";

		return "x=" + String.valueOf((right.b - left.b) / (left.a - right.a));

	}

	private Coef getCoef(String str) {
		Coef c = new Coef(0, 0);
		int sig = 1, tmp = 0;
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == 'x')
				continue;

			if (str.charAt(i) == '+' || str.charAt(i) == '-') {
				if (i > 0 && str.charAt(i - 1) == 'x') {
					boolean hasCoef = i - 1 > 0 && str.charAt(i - 2) != '+' && str.charAt(i - 2) != '-';
					tmp = hasCoef ? tmp : 1;

					c.a += sig * tmp;
				} else {
					c.b += sig * tmp;
				}

				sig = str.charAt(i) == '+' ? 1 : -1;
				tmp = 0;
			} else {
				tmp = tmp * 10 + str.charAt(i) - '0';
			}
		}

		int lastIdx = str.length() - 1;
		if (str.charAt(lastIdx) == 'x') {
			boolean hasCoef = lastIdx > 0 && str.charAt(lastIdx - 1) != '+' && str.charAt(lastIdx - 1) != '-';
			tmp = hasCoef ? tmp : 1;

			c.a += sig * tmp;
		} else {
			c.b += sig * tmp;
		}

		return c;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().solveEquation("x+5-3+x=6+x-2"));
		System.out.println(new Solution().solveEquation("x=x"));
		System.out.println(new Solution().solveEquation("2x=x"));
		System.out.println(new Solution().solveEquation("2x+3x-6x=x+2"));
		System.out.println(new Solution().solveEquation("x=x+2"));
		System.out.println(new Solution().solveEquation("0x=0"));
	}
}