import java.util.ArrayList;
import java.util.List;

public class Solution {
	public List<List<Integer>> combine(int n, int k) {
		List<List<Integer>> res = new ArrayList<>();
		List<Integer> tmpList = new ArrayList<>();

		subCombine(0, n, k, tmpList, res);
		return res;
	}

	private void subCombine(int start, int end, int k, List<Integer> tmpList, List<List<Integer>> res) {
		if (tmpList.size() == k) {
			res.add(new ArrayList<>(tmpList));
			return;
		}

		for (int i = start; i < end; i++) {
			tmpList.add(i + 1);
			subCombine(i + 1, end, k, tmpList, res);
			tmpList.remove(tmpList.size() - 1);
		}
	}

	public static void main(String[] args) {

		List<List<Integer>> res = new Solution().combine(4, 2);
		for (List<Integer> list : res)
			System.out.println(list);
	}
}