import java.util.Arrays;

public class Solution {
	public int nextGreaterElement(int n) {
		String str = String.valueOf(n);
		char[] arr = str.toCharArray();

		int i = arr.length - 1;
		for (; i > 0; i--) {
			if (arr[i] > arr[i - 1]) {
				break;
			}
		}

		if (i == 0)
			return -1;

		int min = arr[i];
		int minIdx = i;
		int j = i;
		for (; j < arr.length; j++) {
			if (arr[j] > arr[i - 1] && arr[j] < min) {
				min = arr[j];
				minIdx = j;
			}
		}

		char tmp = arr[minIdx];
		arr[minIdx] = arr[i - 1];
		arr[i - 1] = tmp;

		Arrays.sort(arr, i, arr.length);

		int res = 0;
		for (int k = 0; k < arr.length; k++) {
			res = res * 10 + arr[k] - '0';
			if (res > Integer.MAX_VALUE / 10)
				return -1;
		}
		return res;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().nextGreaterElement(12));
		System.out.println(new Solution().nextGreaterElement(21));
	}
}