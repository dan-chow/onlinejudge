public class Solution {
	public int removeDuplicates(int[] nums) {
		if (nums == null || nums.length == 0)
			return 0;

		int left = 0, right = 0;
		for (; right < nums.length - 1; right++) {
			while (right < nums.length - 1 && nums[right] == nums[right + 1])
				right++;

			if (right >= nums.length - 1)
				break;

			nums[left++] = nums[right];
		}

		nums[left++] = nums[nums.length - 1];
		return left;
	}

	public static void main(String[] args) {
		int[] nums = { 1, 1, 2 };
		System.out.println(new Solution().removeDuplicates(nums));
	}
}