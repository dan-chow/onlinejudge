public class Solution {
	public boolean judgeCircle(String moves) {
		int horizontal = 0, vertical = 0;
		for (int i = 0; i < moves.length(); i++) {
			switch (moves.charAt(i)) {
			case 'L':
				horizontal--;
				break;

			case 'R':
				horizontal++;
				break;
			case 'U':
				vertical++;
				break;
			case 'D':
				vertical--;
				break;
			default:
				break;
			}
		}
		return horizontal == 0 && vertical == 0;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().judgeCircle("UD"));
		System.out.println(new Solution().judgeCircle("LL"));
	}
}