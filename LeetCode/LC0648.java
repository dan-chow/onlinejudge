import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Solution {
	public String replaceWords(List<String> dict, String sentence) {
		Collections.sort(dict);
		String[] tokens = sentence.split(" ");
		StringBuffer sb = new StringBuffer();
		for (String word : tokens) {
			sb.append(findRoot(dict, word)).append(" ");
		}
		sb.setLength(sb.length() - 1);
		return sb.toString();
	}

	private String findRoot(List<String> dict, String word) {
		for (String root : dict)
			if (word.startsWith(root))
				return root;
		return word;
	}

	public static void main(String[] args) {
		List<String> dict = new ArrayList<>();
		dict.add("cat");
		dict.add("bat");
		dict.add("rat");

		System.out.println(new Solution().replaceWords(dict, "the cattle was rattled by the battery"));
	}
}