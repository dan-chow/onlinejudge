public class Solution {
	public void merge(int[] nums1, int m, int[] nums2, int n) {

		int p1 = m - 1;
		int p2 = n - 1;

		int q = m + n - 1;

		while (p1 >= 0 && p2 >= 0) {
			if (nums1[p1] > nums2[p2])
				nums1[q--] = nums1[p1--];
			else
				nums1[q--] = nums2[p2--];
		}

		while (p2 >= 0)
			nums1[q--] = nums2[p2--];
	}

	public static void main(String[] args) {
		int[] nums1 = { 1, 3, 5, 7, 9, 0, 0, 0, 0, 0 };
		int[] nums2 = { 2, 4, 6, 8 };

		new Solution().merge(nums1, 5, nums2, 4);
		for (int i = 0; i < 5 + 4; i++) {
			System.out.println(nums1[i]);
		}
	}
}