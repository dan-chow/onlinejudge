import java.util.Arrays;

public class Solution {
	public int findNumberOfLIS(int[] nums) {
		if (nums == null || nums.length == 0)
			return 0;

		int[] dp = new int[nums.length];
		int[] cnt = new int[nums.length];

		Arrays.fill(dp, 1);
		Arrays.fill(cnt, 1);

		int maxLen = 1;
		for (int i = 1; i < nums.length; i++) {
			for (int j = 0; j < i; j++) {
				if (nums[i] > nums[j]) {
					if (dp[j] + 1 > dp[i]) {
						dp[i] = dp[j] + 1;
						cnt[i] = cnt[j];
					} else if (dp[j] + 1 == dp[i]) {
						cnt[i] += cnt[j];
					}
				}
			}

			maxLen = Math.max(maxLen, dp[i]);
		}

		int totalCnt = 0;
		for (int i = 0; i < dp.length; i++) {
			if (dp[i] == maxLen)
				totalCnt += cnt[i];
		}

		return totalCnt;
	}

	public static void main(String[] args) {
		int[] nums1 = { 1, 3, 5, 4, 7 };
		System.out.println(new Solution().findNumberOfLIS(nums1));
		int[] nums2 = { 2, 2, 2, 2, 2 };
		System.out.println(new Solution().findNumberOfLIS(nums2));
	}
}