import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Solution {
	public List<Integer> postorderTraversal(TreeNode root) {
		List<Integer> list = new ArrayList<>();
		Stack<TreeNode> nodeStack = new Stack<>();
		Stack<Character> statusStack = new Stack<>();
		TreeNode p = root;
		while (p != null || !nodeStack.isEmpty()) {
			while (p != null) {
				nodeStack.push(p);
				statusStack.push('L');
				p = p.left;
			}

			p = nodeStack.pop();
			if (statusStack.pop() == 'L') {
				nodeStack.push(p);
				statusStack.push('R');
				p = p.right;
			} else {
				list.add(p.val);
				p = null;
			}
		}
		return list;
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(1);
		root.right = new TreeNode(2);
		root.right.left = new TreeNode(3);

		List<Integer> list = new Solution().postorderTraversal(root);
		for (int i : list)
			System.out.println(i);
	}

}