class TrieNode {
	char value;
	TrieNode[] child;
	boolean isLeaf;

	public TrieNode(char value) {
		this.value = value;
		this.child = new TrieNode[26];
		this.isLeaf = false;
	}
}

public class Trie {
	TrieNode root;

	/** Initialize your data structure here. */
	public Trie() {
		root = new TrieNode(' ');
	}

	/** Inserts a word into the trie. */
	public void insert(String word) {
		if (word == null || word.isEmpty())
			return;
		insert(root, word, 0);
	}

	private void insert(TrieNode root, String word, int idx) {
		if (root.child[word.charAt(idx) - 'a'] == null)
			root.child[word.charAt(idx) - 'a'] = new TrieNode(word.charAt(idx));

		if (idx == word.length() - 1) {
			root.child[word.charAt(idx) - 'a'].isLeaf = true;
			return;
		}

		insert(root.child[word.charAt(idx) - 'a'], word, idx + 1);
	}

	/** Returns if the word is in the trie. */
	public boolean search(String word) {
		if (word == null || word.isEmpty())
			return false;
		return search(root, word, 0);
	}

	private boolean search(TrieNode root, String word, int idx) {
		if (root.child[word.charAt(idx) - 'a'] == null)
			return false;

		if (idx == word.length() - 1)
			return root.child[word.charAt(idx) - 'a'].isLeaf;

		return search(root.child[word.charAt(idx) - 'a'], word, idx + 1);
	}

	/**
	 * Returns if there is any word in the trie that starts with the given
	 * prefix.
	 */
	public boolean startsWith(String prefix) {
		if (prefix == null || prefix.isEmpty())
			return false;
		return startsWith(root, prefix, 0);
	}

	private boolean startsWith(TrieNode root, String word, int idx) {
		if (root.child[word.charAt(idx) - 'a'] == null)
			return false;

		if (idx == word.length() - 1)
			return true;

		return startsWith(root.child[word.charAt(idx) - 'a'], word, idx + 1);
	}

	public static void main(String[] args) {

		Trie obj = new Trie();
		obj.insert("haha");
		System.out.println(obj.search("haha"));
		System.out.println(obj.startsWith("ha"));

	}
}
