public class Solution {
	public int longestLine(int[][] M) {
		int totalRows = M.length;
		if (totalRows == 0)
			return 0;

		int totalCols = M[0].length;

		int max = Integer.MIN_VALUE;
		StringBuffer sb = new StringBuffer();

		for (int row = 0; row < totalRows; row++) {
			sb.setLength(0);
			for (int col = 0; col < totalCols; col++)
				sb.append(M[row][col]);

			for (String s : sb.toString().split("0")) {
				if (s.length() > max)
					max = s.length();
			}
		}

		for (int col = 0; col < totalCols; col++) {
			sb.setLength(0);
			for (int row = 0; row < totalRows; row++)
				sb.append(M[row][col]);

			for (String s : sb.toString().split("0")) {
				if (s.length() > max)
					max = s.length();
			}
		}

		for (int row = 0; row < totalRows; row++) {
			sb.setLength(0);
			for (int colOffset = 0; row + colOffset < totalRows && colOffset < totalCols; colOffset++)
				sb.append(M[row + colOffset][colOffset]);

			for (String s : sb.toString().split("0")) {
				if (s.length() > max)
					max = s.length();
			}
		}

		for (int col = 1; col < totalCols; col++) {
			sb.setLength(0);
			for (int rowOffset = 0; rowOffset < totalRows && col + rowOffset < totalCols; rowOffset++)
				sb.append(M[rowOffset][col + rowOffset]);

			for (String s : sb.toString().split("0")) {
				if (s.length() > max)
					max = s.length();
			}
		}

		for (int row = 0; row < totalRows; row++) {
			sb.setLength(0);
			for (int colOffset = 0; row + colOffset < totalRows && totalCols - 1 - colOffset >= 0; colOffset++)
				sb.append(M[row + colOffset][totalCols - 1 - colOffset]);

			for (String s : sb.toString().split("0")) {
				if (s.length() > max)
					max = s.length();
			}
		}

		for (int col = 0; col < totalCols - 1; col++) {
			sb.setLength(0);

			for (int rowOffset = 0; rowOffset < totalRows && col - rowOffset >= 0; rowOffset++)
				sb.append(M[rowOffset][col - rowOffset]);

			for (String s : sb.toString().split("0")) {
				if (s.length() > max)
					max = s.length();
			}
		}

		return max == Integer.MIN_VALUE ? 0 : max;
	}

	public static void main(String[] args) {
		int[][] M = { { 0, 1, 1, 0 }, { 0, 1, 1, 0 }, { 0, 0, 0, 1 } };
		System.out.println(new Solution().longestLine(M));
	}

}