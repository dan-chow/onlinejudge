import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Athlete implements Comparable<Athlete> {
	int index;
	int score;

	public Athlete(int index, int score) {
		this.index = index;
		this.score = score;
	}

	@Override
	public int compareTo(Athlete other) {
		return other.score - this.score;
	}

}

public class Solution {
	public String[] findRelativeRanks(int[] nums) {
		if (nums == null || nums.length == 0)
			return new String[0];

		List<Athlete> athletes = new ArrayList<>();
		for (int i = 0; i < nums.length; i++)
			athletes.add(new Athlete(i, nums[i]));

		Collections.sort(athletes);

		String[] res = new String[nums.length];

		res[athletes.get(0).index] = "Gold Medal";
		if (nums.length == 1)
			return res;

		res[athletes.get(1).index] = "Silver Medal";
		if (nums.length == 2)
			return res;

		res[athletes.get(2).index] = "Bronze Medal";

		for (int i = 3; i < athletes.size(); i++)
			res[athletes.get(i).index] = String.valueOf(i + 1);
		return res;
	}

	public static void main(String[] args) {
		int[] nums = { 5, 4, 3, 2, 1 };
		String[] res = new Solution().findRelativeRanks(nums);
		for (String str : res)
			System.out.println(str);
	}
}