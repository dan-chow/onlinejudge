import java.util.Arrays;

public class Solution {
	public int findRadius(int[] houses, int[] heaters) {
		Arrays.sort(houses);
		Arrays.sort(heaters);

		int min = 0;
		for (int house : houses) {
			int searchResult = Arrays.binarySearch(heaters, house);
			if (searchResult >= 0)
				continue;

			int insertPoint = -(searchResult + 1);
			if (insertPoint == 0) {
				min = Math.max(min, heaters[0] - house);
			} else if (insertPoint == heaters.length) {
				min = Math.max(min, house - heaters[heaters.length - 1]);
			} else {
				min = Math.max(min, Math.min(heaters[insertPoint] - house, house - heaters[insertPoint - 1]));
			}
		}
		return min;
	}

	public static void main(String[] args) {
		int[] houses1 = { 1, 2, 3 };
		int[] heaters1 = { 2 };
		System.out.println(new Solution().findRadius(houses1, heaters1));

		int[] houses2 = { 1, 2, 3, 4 };
		int[] heaters2 = { 1, 4 };
		System.out.println(new Solution().findRadius(houses2, heaters2));
	}
}