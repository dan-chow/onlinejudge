import java.util.ArrayList;
import java.util.List;

public class Solution {
	public List<List<Integer>> permute(int[] nums) {
		List<List<Integer>> res = new ArrayList<>();
		List<Integer> tmpList = new ArrayList<>();
		boolean[] visited = new boolean[nums.length];
		subPermute(nums, visited, tmpList, res);
		return res;
	}

	private void subPermute(int[] nums, boolean[] visited, List<Integer> tmpList, List<List<Integer>> res) {
		if (tmpList.size() == nums.length){
			res.add(new ArrayList<>(tmpList));
			return;
		}
			

		for (int i = 0; i < visited.length; i++) {
			if (!visited[i]) {
				visited[i] = true;
				tmpList.add(nums[i]);
				subPermute(nums, visited, tmpList, res);
				tmpList.remove(tmpList.size() - 1);
				visited[i] = false;
			}
		}
	}

	public static void main(String[] args) {
		int[] nums = { 1, 2, 3 };
		List<List<Integer>> res = new Solution().permute(nums);
		for (List<Integer> list : res)
			System.out.println(list);
	}
}
