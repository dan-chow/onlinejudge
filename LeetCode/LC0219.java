import java.util.Hashtable;

public class Solution {
	public boolean containsNearbyDuplicate(int[] nums, int k) {
		if (nums == null || nums.length == 0)
			return false;

		Hashtable<Integer, Integer> table = new Hashtable<>();
		for (int i = 0; i < nums.length; i++) {
			if (table.containsKey(nums[i])) {
				if (Math.abs(i - table.remove(nums[i])) <= k)
					return true;
			}

			table.put(nums[i], i);
		}
		return false;
	}

	public static void main(String[] args) {
		int[] nums = { 1, 2, 1, 4, 5 };
		System.out.println(new Solution().containsNearbyDuplicate(nums, 2));
	}
}