import java.util.ArrayList;
import java.util.List;

public class Solution {

	public List<List<Integer>> combinationSum3(int k, int n) {
		List<List<Integer>> res = new ArrayList<>();

		List<Integer> tmp = new ArrayList<>();
		combinationSum3(1, k, n, tmp, res);
		return res;
	}

	private void combinationSum3(int start, int k, int n, List<Integer> tmp, List<List<Integer>> res) {
		if (k == 0) {
			if (n == 0)
				res.add(new ArrayList<Integer>(tmp));
			return;
		}

		for (int i = start; i <= 9; i++) {
			if (i > n)
				break;

			tmp.add(i);
			combinationSum3(i + 1, k - 1, n - i, tmp, res);
			tmp.remove(tmp.size() - 1);
		}

	}

	public static void main(String[] args) {

		List<List<Integer>> res1 = new Solution().combinationSum3(3, 7);
		for (List<Integer> list : res1) {
			System.out.println(list);
		}

		System.out.println();

		List<List<Integer>> res2 = new Solution().combinationSum3(3, 9);
		for (List<Integer> list : res2) {
			System.out.println(list);
		}
	}
}
