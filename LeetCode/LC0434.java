public class Solution {
	public int countSegments(String s) {
		int cnt = 0;
		for (int i = 0; i < s.length(); i++) {
			while (i < s.length() && s.charAt(i) == ' ')
				i++;

			if (i != s.length())
				cnt++;

			while (i < s.length() && s.charAt(i) != ' ')
				i++;
		}
		return cnt;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().countSegments("Hello, my name is John"));
	}
}