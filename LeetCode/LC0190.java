public class Solution {
	// you need treat n as an unsigned value
	public int reverseBits(int n) {
		int reverseOne = 1 << 31;

		int cnt = 32;
		int tmp = 1;
		int result = 0;
		while (cnt-- > 0) {
			if ((n & reverseOne) != 0)
				result += tmp;
			tmp *= 2;
			n = n << 1;
		}
		return result;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().reverseBits(43261596));
	}
}