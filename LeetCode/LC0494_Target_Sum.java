public class Solution {
	public int findTargetSumWays(int[] nums, int S) {

		int sum = 0;
		for (int n : nums)
			sum += n;

		if (S > sum || S < -sum)
			return 0;

		int[][] dp = new int[nums.length][2 * sum + 1];

		dp[0][sum + nums[0]] += 1;
		dp[0][sum - nums[0]] += 1;

		for (int i = 1; i < nums.length; i++) {
			for (int j = 0; j < 2 * sum + 1; j++) {
				if (j + nums[i] >= 0 && j + nums[i] < 2 * sum + 1)
					dp[i][j] += dp[i - 1][j + nums[i]];
				if (j - nums[i] >= 0 && j - nums[i] < 2 * sum + 1)
					dp[i][j] += dp[i - 1][j - nums[i]];
			}
		}
		return dp[nums.length - 1][sum + S];
	}

	public static void main(String[] args) {
		int[] nums1 = { 1, 1, 1, 1, 1 };
		System.out.println(new Solution().findTargetSumWays(nums1, 3));

		int[] nums2 = { 0, 0, 0, 0, 0, 0, 0, 0, 1 };
		System.out.println(new Solution().findTargetSumWays(nums2, 1));
	}
}