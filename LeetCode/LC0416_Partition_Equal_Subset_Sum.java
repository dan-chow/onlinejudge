public class Solution {
	public boolean canPartition(int[] nums) {
		if (nums == null || nums.length < 2)
			return false;

		int sum = 0;
		for (int n : nums)
			sum += n;

		if (sum % 2 == 1)
			return false;

		sum /= 2;

		return combinationSum(nums, sum);

	}

	private boolean combinationSum(int[] nums, int sum) {
		int m = nums.length + 1;
		int n = sum + 1;

		boolean dp[][] = new boolean[m][n];

		for (int i = 0; i < m; i++)
			dp[i][0] = true;

		for (int j = 1; j < n; j++)
			dp[0][j] = false;

		for (int i = 1; i < m; i++) {
			for (int j = 1; j < n; j++) {
				dp[i][j] = dp[i - 1][j];

				if (j - nums[i - 1] >= 0)
					dp[i][j] |= dp[i - 1][j - nums[i - 1]];
			}
		}

		return dp[m - 1][n - 1];
	}

	public static void main(String[] args) {
		int[] nums = { 1, 5, 11, 5 };
		System.out.println(new Solution().canPartition(nums));
	}

}