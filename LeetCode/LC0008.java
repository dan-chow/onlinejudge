public class Solution {
	public int myAtoi(String str) {
		boolean negative = false;
		int startIdx = 0;
		str = str.trim();

		if ("".equals(str))
			return 0;

		if (str.charAt(0) == '-') {
			negative = true;
			startIdx++;
		} else if (str.charAt(0) == '+') {
			startIdx++;
		}

		if (startIdx > str.length() - 1)
			return 0;

		if (str.charAt(startIdx) > '9' || str.charAt(startIdx) < '0') {
			return 0;
		}

		int num = 0;
		for (int i = startIdx; i < str.length(); i++) {
			if (str.charAt(i) > '9' || str.charAt(i) < '0')
				break;

			if (num > Integer.MAX_VALUE / 10) {
				return negative ? Integer.MIN_VALUE : Integer.MAX_VALUE;
			} else if (num == Integer.MAX_VALUE / 10) {
				if (!negative && str.charAt(i) > '7' && str.charAt(i) <= '9')
					return Integer.MAX_VALUE;
				else if (negative && str.charAt(i) > '8' && str.charAt(i) <= '9')
					return Integer.MIN_VALUE;

			}

			num = num * 10 + str.charAt(i) - '0';

		}
		return negative ? -1 * num : num;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().myAtoi("-2147483647"));
	}
}