import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.PriorityQueue;

public class Solution {
	public int scheduleCourse(int[][] courses) {
		Arrays.sort(courses, new Comparator<int[]>() {
			@Override
			public int compare(int[] c1, int[] c2) {
				return c1[1] - c2[1];
			}
		});

		PriorityQueue<Integer> pq = new PriorityQueue<>(Collections.reverseOrder());

		int time = 0;
		for (int[] course : courses) {
			time += course[0];
			pq.add(course[0]);

			if (time > course[1])
				time -= pq.poll();

		}
		return pq.size();
	}

	public static void main(String[] args) {
		int[][] courses = { { 100, 200 }, { 200, 1300 }, { 1000, 1250 }, { 2000, 3200 } };
		System.out.println(new Solution().scheduleCourse(courses));
	}
}