import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Solution {
	public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
		List<List<Integer>> list = new ArrayList<>();
		Deque<TreeNode> deque = new LinkedList<>();

		boolean leftToRight = true;

		if (root != null)
			deque.add(root);

		while (!deque.isEmpty()) {
			List<Integer> sublist = new ArrayList<>();
			int nodeNum = deque.size();
			while (nodeNum-- > 0) {
				if (leftToRight) {
					TreeNode node = deque.removeFirst();
					sublist.add(node.val);
					if (node.left != null)
						deque.addLast(node.left);
					if (node.right != null)
						deque.addLast(node.right);
				} else {
					TreeNode node = deque.removeLast();
					sublist.add(node.val);
					if (node.right != null)
						deque.addFirst(node.right);
					if (node.left != null)
						deque.addFirst(node.left);
				}
			}
			list.add(sublist);
			leftToRight = !leftToRight;
		}
		return list;
	}

	public static void main(String[] args) {

		TreeNode root = new TreeNode(3);
		root.left = new TreeNode(9);
		root.right = new TreeNode(20);
		root.right.left = new TreeNode(15);
		root.right.right = new TreeNode(7);

		List<List<Integer>> list = new Solution().zigzagLevelOrder(root);

		for (List<Integer> sublist : list)
			for (int i : sublist)
				System.out.println(i);

	}
}