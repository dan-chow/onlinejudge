import java.util.HashSet;

public class Solution {
	public boolean containsDuplicate(int[] nums) {
		HashSet<Integer> set = new HashSet<>();
		for (int i : nums)
			if (!set.add(i))
				return true;

		return false;
	}

	public static void main(String[] args) {
		int[] nums = { 1, 2, 3, 4, 5, 6, 4 };
		System.out.println(new Solution().containsDuplicate(nums));
	}
}