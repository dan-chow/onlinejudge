import java.util.ArrayList;
import java.util.List;

public class Solution {
	public List<Integer> findAnagrams(String s, String p) {
		List<Integer> res = new ArrayList<>();
		if (s == null || s.length() == 0 || p == null || p.length() == 0)
			return res;

		int[] dict = new int[26];
		for (char c : p.toCharArray())
			dict[c - 'a']++;

		// sliding window

		int left = 0;
		int right = 0;
		int len = p.length();
		while (right < s.length()) {
			if (dict[s.charAt(right) - 'a'] > 0)
				len--;

			dict[s.charAt(right) - 'a']--;
			right++;

			if (len == 0)
				res.add(left);

			if (right - left == p.length()) {
				if (dict[s.charAt(left) - 'a'] >= 0)
					len++;

				dict[s.charAt(left) - 'a']++;
				left++;
			}

		}

		return res;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().findAnagrams("cbaebabacd", "abc"));
		System.out.println(new Solution().findAnagrams("abab", "ab"));
	}
}