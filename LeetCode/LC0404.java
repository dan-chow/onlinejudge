public class Solution {
	public static class TreeNode {

		public int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int x) {
			val = x;
		}
	}

	public int sumOfLeftLeaves(TreeNode root) {
		if (root == null)
			return 0;

		return sumOfLeftLeaves(root.left, true) + sumOfLeftLeaves(root.right, false);
	}

	public int sumOfLeftLeaves(TreeNode root, boolean left) {
		if (root == null)
			return 0;

		if (root.left == null && root.right == null && left)
			return root.val;

		return sumOfLeftLeaves(root.left, true) + sumOfLeftLeaves(root.right, false);
	}

	public static void main(String[] args) {

		TreeNode root = new TreeNode(3);
		root.left = new TreeNode(9);
		root.right = new TreeNode(20);
		root.right.left = new TreeNode(15);
		root.right.right = new TreeNode(7);

		System.out.println(new Solution().sumOfLeftLeaves(root));
	}
}