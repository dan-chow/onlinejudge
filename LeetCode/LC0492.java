public class Solution {
	public int[] constructRectangle(int area) {
		for (int width = (int) Math.sqrt(area); width > 0; width--) {
			if (area % width == 0)
				return new int[] { area / width, width };
		}
		return new int[] { area, 1 };
	}

	public static void main(String[] args) {
		int[] res = new Solution().constructRectangle(4);
		System.out.println(res[0] + " " + res[1]);
	}
}