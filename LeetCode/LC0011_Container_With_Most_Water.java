public class Solution {
	public int maxArea(int[] height) {
		int left = 0;
		int right = height.length - 1;

		int max = Integer.MIN_VALUE;
		while (left < right) {
			max = Math.max(max, (right - left) * Math.min(height[left], height[right]));
			if (height[left] < height[right]) {
				left++;
			} else {
				right--;
			}
		}
		return max;
	}

	public static void main(String[] args) {
		int[] height = { 5, 3, 7, 3, 1 };
		System.out.println(new Solution().maxArea(height));
	}
}