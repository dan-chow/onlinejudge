public class Solution {
	public String removeKdigits(String num, int k) {

		int digits = num.length() - k;
		char[] stack = new char[num.length()];

		int top = 0;
		for (int i = 0; i < num.length(); ++i) {

			while (top > 0 && stack[top - 1] > num.charAt(i) && k > 0) {
				top -= 1;
				k -= 1;
			}
			stack[top++] = num.charAt(i);
		}

		int idx = 0;
		while (idx < digits && stack[idx] == '0')
			idx++;

		return idx == digits ? "0" : new String(stack, idx, digits - idx);
	}

	public static void main(String[] args) {
		System.out.println(new Solution().removeKdigits("1432219", 3));
		System.out.println(new Solution().removeKdigits("10200", 1));
		System.out.println(new Solution().removeKdigits("10", 2));
	}
}