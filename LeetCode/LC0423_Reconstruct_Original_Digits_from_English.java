public class Solution {
	public String originalDigits(String s) {
		int[] digits = new int[10];

		for (int i = 0; i < s.length(); i++) {
			switch (s.charAt(i)) {
			case 'z':
				digits[0]++;
				break;
			case 'w':
				digits[2]++;
				break;
			case 'u':
				digits[4]++;
				break;
			case 'x':
				digits[6]++;
				break;
			case 'g':
				digits[8]++;
				break;
			case 'h':
				digits[3]++;
				break;
			case 'f':
				digits[5]++;
				break;
			case 'v':
				digits[7]++;
				break;
			case 'o':
				digits[1]++;
				break;
			case 'i':
				digits[9]++;
				break;
			default:
			}
		}

		digits[3] = digits[3] - digits[8];
		digits[5] = digits[5] - digits[4];
		digits[7] = digits[7] - digits[5];
		digits[1] = digits[1] - digits[0] - digits[2] - digits[4];
		digits[9] = digits[9] - digits[8] - digits[6] - digits[5];

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < digits[i]; j++) {
				sb.append(i);
			}
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		System.out.println(new Solution().originalDigits("owoztneoer"));
		System.out.println(new Solution().originalDigits("fviefuro"));
	}
}