import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class Solution {
	public int leastBricks(List<List<Integer>> wall) {
		HashMap<Integer, Integer> map = new HashMap<>();
		for (int i = 0; i < wall.size(); i++) {
			int width = 0;
			for (int j = 0; j < wall.get(i).size() - 1; j++) {
				width += wall.get(i).get(j);
				if (!map.containsKey(width)) {
					map.put(width, 1);
				} else {
					map.put(width, map.get(width) + 1);
				}
			}
		}
		int min = wall.size();
		for (Entry<Integer, Integer> entry : map.entrySet()) {
			if (wall.size() - entry.getValue() < min) {
				min = wall.size() - entry.getValue();
			}
		}
		return min;
	}

	public static void main(String[] args) {

		// [[1,2,2,1],
		List<List<Integer>> wall = new ArrayList<>();
		wall.add(new ArrayList<Integer>());
		wall.get(0).add(1);
		wall.get(0).add(2);
		wall.get(0).add(2);
		wall.get(0).add(1);

		// [3,1,2],
		wall.add(new ArrayList<Integer>());
		wall.get(1).add(3);
		wall.get(1).add(1);
		wall.get(1).add(2);

		// [1,3,2],
		wall.add(new ArrayList<Integer>());
		wall.get(2).add(1);
		wall.get(2).add(3);
		wall.get(2).add(2);

		// [2,4],
		wall.add(new ArrayList<Integer>());
		wall.get(3).add(2);
		wall.get(3).add(4);

		// [3,1,2],
		wall.add(new ArrayList<Integer>());
		wall.get(4).add(3);
		wall.get(4).add(1);
		wall.get(4).add(2);

		// [1,3,1,1]]
		wall.add(new ArrayList<Integer>());
		wall.get(5).add(1);
		wall.get(5).add(3);
		wall.get(5).add(1);
		wall.get(5).add(1);

		Solution solution = new Solution();
		System.out.println(solution.leastBricks(wall));
	}
}