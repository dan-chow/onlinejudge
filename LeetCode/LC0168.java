public class Solution {
	public String convertToTitle(int n) {
		StringBuffer sb = new StringBuffer();
		while (n > 0) {
			int tmp = (n % 26);
			sb.append(tmp == 0 ? 'Z' : (char) ('A' - 1 + tmp));

			n -= tmp == 0 ? 26 : tmp;
			n /= 26;
		}
		return sb.reverse().toString();
	}

	public static void main(String[] args) {
		System.out.println(new Solution().convertToTitle(26));
		System.out.println(new Solution().convertToTitle(27));
		System.out.println(new Solution().convertToTitle(28));
		System.out.println(new Solution().convertToTitle(52));
	}
}