class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Solution {
	public boolean isSubtree(TreeNode s, TreeNode t) {
		if (s == null)
			return false;

		if (sameTree(s, t))
			return true;

		if (s.left != null && isSubtree(s.left, t))
			return true;

		return isSubtree(s.right, t);
	}

	private boolean sameTree(TreeNode left, TreeNode right) {
		if (left == null && right == null)
			return true;
		if (left == null || right == null)
			return false;
		if (left.val != right.val)
			return false;
		return sameTree(left.left, right.left) && sameTree(left.right, right.right);
	}

	public static void main(String[] args) {
		TreeNode s1 = new TreeNode(3);
		s1.left = new TreeNode(4);
		s1.right = new TreeNode(5);
		s1.left.left = new TreeNode(1);
		s1.left.right = new TreeNode(2);

		TreeNode t1 = new TreeNode(4);
		t1.left = new TreeNode(1);
		t1.right = new TreeNode(2);

		System.out.println(new Solution().isSubtree(s1, t1));

		TreeNode s2 = new TreeNode(3);
		s2.left = new TreeNode(4);
		s2.right = new TreeNode(5);
		s2.left.left = new TreeNode(1);
		s2.left.right = new TreeNode(2);
		s2.left.right.left = new TreeNode(0);

		TreeNode t2 = new TreeNode(4);
		t2.left = new TreeNode(1);
		t2.right = new TreeNode(2);

		System.out.println(new Solution().isSubtree(s2, t2));
	}
}