import java.util.HashSet;

public class Solution {

	public final static int LEFT = 0;
	public final static int BOTTOM = 1;
	public final static int RIGHT = 2;
	public final static int TOP = 3;

	public boolean isRectangleCover(int[][] rectangles) {

		int leftMost = Integer.MAX_VALUE;
		int rightMost = Integer.MIN_VALUE;
		int bottomMost = Integer.MAX_VALUE;
		int topMost = Integer.MIN_VALUE;
		int totalArea = 0;

		Point leftTop;
		Point leftBottom;
		Point rightTop;
		Point rightBottom;

		HashSet<Point> set = new HashSet<>();
		for (int[] rect : rectangles) {
			leftMost = Math.min(leftMost, rect[LEFT]);
			rightMost = Math.max(rightMost, rect[RIGHT]);
			bottomMost = Math.min(bottomMost, rect[BOTTOM]);
			topMost = Math.max(topMost, rect[TOP]);

			leftBottom = new Point(rect[LEFT], rect[BOTTOM]);
			leftTop = new Point(rect[LEFT], rect[TOP]);
			rightTop = new Point(rect[RIGHT], rect[TOP]);
			rightBottom = new Point(rect[RIGHT], rect[BOTTOM]);

			if (!set.add(leftBottom))
				set.remove(leftBottom);
			if (!set.add(leftTop))
				set.remove(leftTop);
			if (!set.add(rightTop))
				set.remove(rightTop);
			if (!set.add(rightBottom))
				set.remove(rightBottom);

			totalArea += (rect[RIGHT] - rect[LEFT]) * (rect[TOP] - rect[BOTTOM]);
		}

		if (totalArea != (rightMost - leftMost) * (topMost - bottomMost))
			return false;

		leftTop = new Point(leftMost, topMost);
		leftBottom = new Point(leftMost, bottomMost);
		rightTop = new Point(rightMost, topMost);
		rightBottom = new Point(rightMost, bottomMost);

		if (set.size() != 4)
			return false;

		if (!set.contains(leftTop) || !set.contains(leftBottom) || !set.contains(rightTop) || !set.contains(rightBottom))
			return false;

		return true;
	}

	class Point {
		int x;
		int y;

		public Point(int x, int y) {
			this.x = x;
			this.y = y;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof Point) {
				Point pt = (Point) obj;
				return (this.x == pt.x) && (this.y == pt.y);
			}
			return false;
		}

		@Override
		public int hashCode() {
			int result = this.x;
			result = 31 * result + this.y;
			return result;
		}

	}

	public static void main(String[] args) {
		int[][] a = { { 1, 1, 3, 3 }, { 3, 1, 4, 2 }, { 3, 2, 4, 4 }, { 1, 3, 2, 4 }, { 2, 3, 3, 4 } };
		int[][] b = { { 1, 1, 2, 3 }, { 1, 3, 2, 4 }, { 3, 1, 4, 2 }, { 3, 2, 4, 4 } };
		int[][] c = { { 1, 1, 3, 3 }, { 3, 1, 4, 2 }, { 1, 3, 2, 4 }, { 3, 2, 4, 4 } };
		int[][] d = { { 1, 1, 3, 3 }, { 3, 1, 4, 2 }, { 1, 3, 2, 4 }, { 2, 2, 4, 4 } };

		System.out.println(new Solution().isRectangleCover(a));
		System.out.println(new Solution().isRectangleCover(b));
		System.out.println(new Solution().isRectangleCover(c));
		System.out.println(new Solution().isRectangleCover(d));
	}
}