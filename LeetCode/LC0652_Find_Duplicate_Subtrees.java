import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Solution {
	public List<TreeNode> findDuplicateSubtrees(TreeNode root) {
		List<TreeNode> res = new ArrayList<>();
		List<TreeNode> rights = new ArrayList<>();
		findDuplicateSubtrees(root, rights, res);
		if (res.size() < 2)
			return res;

		Collections.sort(res, new Comparator<TreeNode>() {
			@Override
			public int compare(TreeNode node1, TreeNode node2) {
				return node1.val - node2.val;
			}
		});

		for (int i = 0; i < res.size() - 1; i++) {
			List<TreeNode> toRemove = new ArrayList<>();
			for (int j = i + 1; j < res.size(); j++) {
				if (isSameTree(res.get(i), res.get(j)))
					toRemove.add(res.get(j));
			}
			res.removeAll(toRemove);
		}

		return res;
	}

	private void findDuplicateSubtrees(TreeNode root, List<TreeNode> rights, List<TreeNode> res) {
		if (root == null)
			return;

		for (TreeNode right : rights) {
			if (hasSubTree(right, root)) {
				if (!res.contains(root))
					res.add(root);
				break;
			}
		}

		if (root.right == null) {
			findDuplicateSubtrees(root.left, rights, res);
			return;
		}

		rights.add(root.right);
		findDuplicateSubtrees(root.left, rights, res);
		rights.remove(root.right);

		findDuplicateSubtrees(root.right, rights, res);

	}

	private boolean hasSubTree(TreeNode root, TreeNode subTree) {
		if (isSameTree(root, subTree))
			return true;
		if (root.left != null && hasSubTree(root.left, subTree))
			return true;
		if (root.right != null && hasSubTree(root.right, subTree))
			return true;
		return false;
	}

	private boolean isSameTree(TreeNode root1, TreeNode root2) {
		if (root1 == null && root2 == null)
			return true;
		if (root1 == null || root2 == null)
			return false;

		return root1.val == root2.val && isSameTree(root1.left, root2.left) && isSameTree(root1.right, root2.right);
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.left.left = new TreeNode(4);
		root.right = new TreeNode(3);
		root.right.left = new TreeNode(2);
		root.right.left.left = new TreeNode(4);
		root.right.right = new TreeNode(4);

		// TreeNode root = new TreeNode(1);
		// root.left = new TreeNode(2);
		// root.right = new TreeNode(3);
		// root.left.left = new TreeNode(4);

		List<TreeNode> res = new Solution().findDuplicateSubtrees(root);

		for (TreeNode node : res)
			System.out.println(node.val);
	}
}