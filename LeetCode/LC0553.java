public class Solution {
	public String optimalDivision(int[] nums) {
		if (nums.length == 1)
			return String.valueOf(nums[0]);

		if (nums.length == 2)
			return new StringBuffer().append(nums[0]).append("/").append(nums[1]).toString();

		StringBuffer sb = new StringBuffer();
		sb.append(nums[0]).append("/(");
		for (int i = 1; i < nums.length; i++) {
			sb.append(nums[i]).append("/");
		}
		sb.setLength(sb.length() - 1);
		sb.append(")");
		return sb.toString();
	}

	public static void main(String[] args) {
		int[] nums = { 1000, 100, 10, 2 };
		System.out.println(new Solution().optimalDivision(nums));
	}
}