import java.util.HashMap;

public class Solution {
	public boolean canConstruct(String ransomNote, String magazine) {
		HashMap<Character, Integer> map = new HashMap<>();
		for (char c : magazine.toCharArray()) {
			if (!map.containsKey(c)) {
				map.put(c, 1);
			} else {
				map.put(c, map.get(c) + 1);
			}
		}
		for (char c : ransomNote.toCharArray()) {
			if (!map.containsKey(c))
				return false;

			map.put(c, map.get(c) - 1);
			if (map.get(c) == 0)
				map.remove(c);
		}

		return true;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().canConstruct("a", "b"));
		System.out.println(new Solution().canConstruct("aa", "ab"));
		System.out.println(new Solution().canConstruct("aa", "aab"));
	}
}