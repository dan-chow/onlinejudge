
class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Solution {

	public int pathSum(TreeNode root, int sum) {
		if (root == null)
			return 0;
		return subPathSum(root, sum) + pathSum(root.left, sum) + pathSum(root.right, sum);
	}

	private int subPathSum(TreeNode root, int sum) {
		if (root == null)
			return 0;

		int res = 0;
		if (sum == root.val)
			res++;

		res += subPathSum(root.left, sum - root.val);
		res += subPathSum(root.right, sum - root.val);
		return res;
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(10);
		root.left = new TreeNode(5);
		root.left.left = new TreeNode(3);
		root.left.left.left = new TreeNode(3);
		root.left.left.right = new TreeNode(-2);
		root.left.right = new TreeNode(2);
		root.left.right.right = new TreeNode(1);
		root.right = new TreeNode(-3);
		root.right.right = new TreeNode(11);

		System.out.println(new Solution().pathSum(root, 3));

	}
}