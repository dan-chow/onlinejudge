public class Solution {
	public boolean canJump(int[] nums) {
		if (nums == null || nums.length == 0)
			return false;

		int reach = 0;
		for (int i = 0; i < nums.length && i <= reach; i++)
			reach = Math.max(reach, i + nums[i]);

		return reach >= nums.length - 1;
	}

	public static void main(String[] args) {
		int[] nums1 = { 2, 3, 1, 1, 4 };
		System.out.println(new Solution().canJump(nums1));

		int[] nums2 = { 3, 2, 1, 0, 4 };
		System.out.println(new Solution().canJump(nums2));
	}
}