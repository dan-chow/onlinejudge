import java.util.Arrays;

public class Solution {
	public void nextPermutation(int[] nums) {
		int i = nums.length - 1;
		while (i > 0 && nums[i] <= nums[i - 1])
			i--;

		if (i == 0) {
			Arrays.sort(nums);
			return;
		}

		int min = nums[i], minIdx = i;
		for (int j = i + 1; j < nums.length; j++) {
			if (nums[j] > nums[i - 1] && nums[j] < min) {
				min = nums[j];
				minIdx = j;
			}
		}

		nums[minIdx] = nums[i - 1];
		nums[i - 1] = min;

		Arrays.sort(nums, i, nums.length);
	}

	public static void main(String[] args) {
		int[] nums1 = { 1, 2, 3 };
		new Solution().nextPermutation(nums1);
		for (int i : nums1)
			System.out.print(i + " ");
		System.out.println();

		int[] nums2 = { 3, 2, 1 };
		new Solution().nextPermutation(nums2);
		for (int i : nums2)
			System.out.print(i + " ");
		System.out.println();

		int[] nums3 = { 1, 1, 5 };
		new Solution().nextPermutation(nums3);
		for (int i : nums3)
			System.out.print(i + " ");
		System.out.println();
	}
}
