import java.util.Arrays;

public class Solution {
	public int triangleNumber(int[] nums) {
		Arrays.sort(nums);

		int totalCnt = 0;
		for (int i = 0; i < nums.length - 2; i++) {
			for (int j = i + 1; j < nums.length - 1; j++) {
				int beg = j + 1;
				int end = nums.length - 1;
				while (beg < end) {
					int mid = (beg + end) / 2;
					if (nums[mid] >= nums[i] + nums[j])
						end = mid - 1;
					else
						beg = mid + 1;
				}
				if (nums[beg] >= nums[i] + nums[j])
					totalCnt += beg - j - 1;
				else
					totalCnt += beg - j;
			}
		}
		return totalCnt;
	}

	public static void main(String[] args) {
		int[] nums = { 2, 2, 3, 4 };
		System.out.println(new Solution().triangleNumber(nums));
	}
}