import java.util.Arrays;

public class Solution {
	class Pair {
		int first;
		int second;

		public Pair(int first, int second) {
			this.first = first;
			this.second = second;
		}
	}

	public int threeSumClosest(int[] nums, int target) {
		Arrays.sort(nums);
		int diff = Integer.MAX_VALUE;
		int sum = 0;
		for (int i = 0; i < nums.length - 2; i++) {
			int j = i + 1;
			int k = nums.length - 1;
			while (j < k) {
				int tmpSum = nums[i] + nums[j] + nums[k];
				int tmpDiff = Math.abs(tmpSum - target);
				if (tmpDiff < diff) {
					diff = tmpDiff;
					sum = tmpSum;
				}
				if (tmpSum > target)
					k--;
				else if (tmpSum < target)
					j++;
				else
					return target;
			}
		}
		return sum;
	}

	public static void main(String[] args) {
		int[] nums = { -1, 2, 1, -4 };
		System.out.println(new Solution().threeSumClosest(nums, 1));
	}
}