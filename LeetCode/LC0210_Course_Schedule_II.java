import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

class Course implements Comparable<Course> {
	int id;
	int indegree;
	List<Course> nexts;

	public Course(int id) {
		this.id = id;
		this.indegree = 0;
		this.nexts = new ArrayList<>();
	}

	@Override
	public int compareTo(Course other) {
		return this.indegree - other.indegree;
	}

}

public class Solution {
	public int[] findOrder(int numCourses, int[][] prerequisites) {

		Course[] courses = new Course[numCourses];
		for (int i = 0; i < numCourses; i++)
			courses[i] = new Course(i);

		for (int[] c : prerequisites) {
			courses[c[0]].indegree++;
			courses[c[1]].nexts.add(courses[c[0]]);
		}

		Queue<Course> queue = new LinkedList<>();
		for (Course course : courses)
			if (course.indegree == 0)
				queue.add(course);

		List<Course> list = new ArrayList<>();
		while (!queue.isEmpty()) {
			Course c = queue.poll();
			list.add(c);
			for (Course next : c.nexts) {
				next.indegree--;

				if (next.indegree == 0)
					queue.add(next);
			}
		}

		if (list.size() < numCourses)
			return new int[] {};

		int[] res = new int[list.size()];
		for (int i = 0; i < list.size(); i++)
			res[i] = list.get(i).id;

		return res;
	}

	public static void main(String[] args) {

		int[][] prerequisites1 = { { 1, 0 } };
		int[] res1 = new Solution().findOrder(2, prerequisites1);
		for (int i : res1)
			System.out.println(i + " ");

		System.out.println();

		int[][] prerequisites2 = { { 1, 0 }, { 2, 0 }, { 3, 1 }, { 3, 2 } };
		int[] res2 = new Solution().findOrder(4, prerequisites2);
		for (int i : res2)
			System.out.println(i + " ");
	}
}