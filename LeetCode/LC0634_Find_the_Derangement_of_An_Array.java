public class Solution {
	public int findDerangement(int n) {
		if (n <= 1)
			return 0;

		int mod = 1000000007;

		int[] dp = new int[n + 1];

		dp[1] = 0;
		dp[2] = 1;

		// the first element has (n-1) choices
		// the second element depends on the first element's place

		for (int i = 3; i <= n; i++) {
			dp[i] = multiply(i - 1, dp[i - 1] + dp[i - 2], mod);
		}

		return dp[n];
	}

	private int multiply(int a, int b, int mod) {
		int result = 0;
		int tmp = b % mod;
		while (a > 0) {
			if (a % 2 == 1)
				result = (result + tmp) % mod;

			tmp = (tmp * 2) % mod;
			a /= 2;
		}
		return result;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().findDerangement(3));
		System.out.println(new Solution().findDerangement(13));
		System.out.println(new Solution().findDerangement(23));

	}
}