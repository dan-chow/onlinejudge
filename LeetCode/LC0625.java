import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution {
	public int smallestFactorization(int a) {
		if (a == 1)
			return 1;
		List<Character> list = new ArrayList<>();
		while (a > 1) {
			if (a % 9 == 0) {
				a = a / 9;
				list.add('9');
			} else if (a % 8 == 0) {
				a = a / 8;
				list.add('8');
			} else if (a % 7 == 0) {
				a /= 7;
				list.add('7');

			} else if (a % 6 == 0) {
				a /= 6;
				list.add('6');
			} else if (a % 5 == 0) {
				a /= 5;
				list.add('5');
			} else if (a % 4 == 0) {
				a /= 4;
				list.add('4');
			} else if (a % 3 == 0) {
				a /= 3;
				list.add('3');
			} else if (a % 2 == 0) {
				a /= 2;
				list.add('2');
			} else {
				return 0;
			}
		}

		char[] arr = new char[list.size()];
		for (int i = 0; i < arr.length; i++)
			arr[i] = list.get(i);

		Arrays.sort(arr);

		int result = 0;
		for (int i = 0; i < arr.length; i++) {
			if (result >= Integer.MAX_VALUE / 10)
				return 0;

			result = result * 10 + arr[i] - '0';
		}

		return result;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().smallestFactorization(48));
		System.out.println(new Solution().smallestFactorization(15));
	}
}