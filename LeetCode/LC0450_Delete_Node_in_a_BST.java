import java.util.LinkedList;
import java.util.Queue;

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Solution {
	public TreeNode deleteNode(TreeNode root, int key) {

		if (root == null)
			return null;

		if (key < root.val)
			root.left = deleteNode(root.left, key);
		else if (key > root.val)
			root.right = deleteNode(root.right, key);
		else {
			if (root.right == null)
				return root.left;
			else if (root.left == null)
				return root.right;

			TreeNode p = findMin(root.right);
			root.val = p.val;
			root.right = deleteNode(root.right, root.val);
		}
		return root;
	}

	private TreeNode findMin(TreeNode node) {
		if (node == null)
			return null;

		while (node.left != null)
			node = node.left;

		return node;
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(5);
		root.left = new TreeNode(3);
		root.right = new TreeNode(6);
		root.left.left = new TreeNode(2);
		root.left.right = new TreeNode(4);
		root.right.left = new TreeNode(7);

		root = new Solution().deleteNode(root, 3);
		Queue<TreeNode> queue = new LinkedList<>();
		queue.add(root);
		while (!queue.isEmpty()) {
			int levelSize = queue.size();
			while (levelSize-- > 0) {
				TreeNode node = queue.poll();
				System.out.print(node.val + " ");
				if (node.left != null)
					queue.add(node.left);
				if (node.right != null)
					queue.add(node.right);
			}
			System.out.println();
		}
	}
}