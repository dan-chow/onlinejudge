public class Solution {
	public String countAndSay(int n) {
		StringBuffer sb = new StringBuffer();
		sb.append(1);
		for (int i = 1; i < n; i++) {
			char last = sb.charAt(0);
			int cnt = 1;
			StringBuffer newSb = new StringBuffer();
			for (int j = 1; j < sb.length(); j++) {
				if (sb.charAt(j) != last) {
					newSb.append(cnt);
					newSb.append(last);

					last = sb.charAt(j);
					cnt = 1;
				} else {
					cnt++;
				}
			}
			newSb.append(cnt);
			newSb.append(last);

			sb = newSb;
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		System.out.println(new Solution().countAndSay(1));
		System.out.println(new Solution().countAndSay(4));
	}
}