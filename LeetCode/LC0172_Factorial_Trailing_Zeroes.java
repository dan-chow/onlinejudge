public class Solution {
	public int trailingZeroes(int n) {
		// how many 5s, 25s, 125s, ...
		// 100/25 = 20/5
		return n == 0 ? 0 : n / 5 + trailingZeroes(n / 5);
	}

	public static void main(String[] args) {
		System.out.println(new Solution().trailingZeroes(23));
		System.out.println(new Solution().trailingZeroes(100));
	}
}