import java.util.Arrays;
import java.util.Random;

public class Solution {
	private int[] original;
	private Random random;

	public Solution(int[] nums) {
		this.original = nums;
		this.random = new Random();
	}

	public int[] reset() {
		return this.original;
	}

	public int[] shuffle() {
		int[] tmpArr = Arrays.copyOf(this.original, this.original.length);
		// Fisher and Yates algorithm
		for (int i = tmpArr.length - 1; i > 0; i--) {
			int j = random.nextInt(i + 1);
			int tmp = tmpArr[i];
			tmpArr[i] = tmpArr[j];
			tmpArr[j] = tmp;
		}
		return tmpArr;

	}

	public static void main(String[] args) {
		int[] nums = { 1, 2, 3 };
		Solution solution = new Solution(nums);

		for (int i : solution.shuffle()) {
			System.out.println(i);
		}

		for (int i : solution.reset()) {
			System.out.println(i);
		}

		for (int i : solution.shuffle()) {
			System.out.println(i);
		}

	}
}