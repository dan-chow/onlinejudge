public class Solution {
	public int findMin(int[] nums) {
		int beg = 0;
		int end = nums.length - 1;
		while (beg < end) {
			int mid = (beg + end) / 2;
			if (nums[mid] > nums[end])
				beg = mid + 1;
			else if (nums[mid] < nums[beg])
				end = mid;
			else
				break;
		}
		return nums[beg];
	}

	public static void main(String[] args) {
		int[] nums = { 4, 5, 6, 7, 0, 1, 2 };
		System.out.println(new Solution().findMin(nums));
	}
}