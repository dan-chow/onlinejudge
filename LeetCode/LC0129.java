class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Solution {
	public int sumNumbers(TreeNode root) {
		if (root == null)
			return 0;

		if (root.left == null && root.right == null)
			return root.val;

		return sumNumbers(root.left, root.val) + sumNumbers(root.right, root.val);

	}

	private int sumNumbers(TreeNode root, int tmpSum) {
		if (root == null)
			return 0;

		tmpSum = tmpSum * 10 + root.val;
		if (root.left == null && root.right == null)
			return tmpSum;

		return sumNumbers(root.left, tmpSum) + sumNumbers(root.right, tmpSum);
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(3);

		System.out.println(new Solution().sumNumbers(root));
	}
}