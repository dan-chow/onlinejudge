public class Solution {
	public int searchInsert(int[] nums, int target) {
		int begin = 0;
		int end = nums.length - 1;
		while (begin <= end) {
			int mid = (begin + end) / 2;
			if (nums[mid] == target)
				return mid;
			if (nums[mid] < target)
				begin = mid + 1;
			else
				end = mid - 1;
		}
		return begin;
	}

	public static void main(String[] args) {
		int[] nums = { 1, 3, 5, 6 };

		System.out.println(new Solution().searchInsert(nums, 5));
		System.out.println(new Solution().searchInsert(nums, 2));
		System.out.println(new Solution().searchInsert(nums, 7));
		System.out.println(new Solution().searchInsert(nums, 0));

	}
}