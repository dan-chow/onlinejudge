import java.util.ArrayList;
import java.util.List;

class Time {
	public static int YEAR = 0;
	public static int MONTH = 1;
	public static int DAY = 2;
	public static int HOUR = 3;
	public static int MINUTE = 4;
	public static int SECOND = 5;

	int id;
	int year;
	int month;
	int day;
	int hour;
	int minute;
	int second;

	public Time(int id, String timestamp) {
		String[] tokens = timestamp.split(":");

		this.id = id;
		this.year = Integer.parseInt(tokens[0]);
		this.month = Integer.parseInt(tokens[1]);
		this.day = Integer.parseInt(tokens[2]);
		this.hour = Integer.parseInt(tokens[3]);
		this.minute = Integer.parseInt(tokens[4]);
		this.second = Integer.parseInt(tokens[5]);
	}

	public int compareTo(Time other, int gra) {
		if (this.year != other.year)
			return this.year - other.year;
		if (gra == YEAR)
			return 0;

		if (this.month != other.month)
			return this.month - other.month;
		if (gra == MONTH)
			return 0;

		if (this.day != other.day)
			return this.day - other.day;
		if (gra == DAY)
			return 0;

		if (this.hour != other.hour)
			return this.hour - other.hour;
		if (gra == HOUR)
			return 0;

		if (this.minute != other.minute)
			return this.minute - other.minute;
		if (gra == MINUTE)
			return 0;

		return this.second - other.second;
	}

	public static int getGra(String gra) {
		if ("Year".equals(gra))
			return Time.YEAR;
		else if ("Month".equals(gra))
			return Time.MONTH;
		else if ("Day".equals(gra))
			return Time.DAY;
		else if ("Hour".equals(gra))
			return Time.HOUR;
		else if ("Minute".equals(gra))
			return Time.MINUTE;
		else if ("Second".equals(gra))
			return Time.SECOND;
		return -1;
	}
}

public class LogSystem {

	List<Time> log;

	public LogSystem() {
		log = new ArrayList<>();
	}

	public void put(int id, String timestamp) {
		log.add(new Time(id, timestamp));
	}

	public List<Integer> retrieve(String s, String e, String gra) {
		Time start = new Time(-1, s);
		Time end = new Time(-1, e);
		int graInt = Time.getGra(gra);

		List<Integer> res = new ArrayList<>();
		for (Time t : log) {
			if (t.compareTo(start, graInt) >= 0 && t.compareTo(end, graInt) <= 0)
				res.add(t.id);
		}

		return res;
	}

	public static void main(String[] args) {
		LogSystem obj = new LogSystem();
		System.out.println(obj.retrieve("2016:01:01:01:01:01", "2017:01:01:23:00:00", "Year"));
		obj.put(1, "2017:01:01:23:59:59");
		obj.put(2, "2017:01:01:22:59:59");
		obj.put(3, "2016:01:01:00:00:00");
		System.out.println(obj.retrieve("2016:01:01:01:01:01", "2017:01:01:23:00:00", "Year"));
		System.out.println(obj.retrieve("2016:01:01:01:01:01", "2017:01:01:23:00:00", "Hour"));
	}
}