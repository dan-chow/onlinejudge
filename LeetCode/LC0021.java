public class Solution {

	public static class ListNode {
		int val;
		ListNode next;

		ListNode(int x) {
			val = x;
		}
	}

	public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
		if (l1 == null)
			return l2;
		if (l2 == null)
			return l1;

		ListNode p1 = l1;
		ListNode p2 = l2;
		ListNode res;
		if (p1.val < p2.val) {
			res = p1;
			p1 = p1.next;
		} else {
			res = p2;
			p2 = p2.next;
		}
		ListNode q = res;
		while (p1 != null && p2 != null) {
			if (p1.val < p2.val) {
				q.next = p1;
				p1 = p1.next;
				q = q.next;
			} else {
				q.next = p2;
				p2 = p2.next;
				q = q.next;
			}
		}
		if (p1 == null)
			q.next = p2;
		else
			q.next = p1;

		return res;
	}

	public static void main(String[] args) {
		ListNode l1 = new ListNode(1);
		l1.next = new ListNode(3);
		l1.next.next = new ListNode(5);
		l1.next.next.next = new ListNode(7);

		ListNode l2 = new ListNode(2);
		l2.next = new ListNode(4);
		l2.next.next = new ListNode(6);
		l2.next.next.next = new ListNode(8);

		ListNode res = new Solution().mergeTwoLists(l1, l2);
		ListNode p = res;
		while (p != null) {
			System.out.println(p.val);
			p = p.next;
		}
	}
}