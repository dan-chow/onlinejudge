import java.util.ArrayList;
import java.util.List;

public class Solution {
	public int shoppingOffers(List<Integer> price, List<List<Integer>> special, List<Integer> needs) {
		int max = Integer.MAX_VALUE;
		List<Integer> owned = new ArrayList<>();
		for (int i = 0; i < needs.size(); i++)
			owned.add(0);

		return sub(max, 0, price, special, owned, needs);
	}

	private int sub(int max, int curPrice, List<Integer> price, List<List<Integer>> special, List<Integer> owned, List<Integer> needs) {
		int withoutSpecial = curPrice;
		for (int item = 0; item < owned.size(); item++) {
			withoutSpecial += needs.get(item) * price.get(item);
			if (withoutSpecial > max)
				break;
		}
		if (withoutSpecial < max)
			max = withoutSpecial;

		for (int spIdx = 0; spIdx < special.size(); spIdx++) {
			boolean match = true;
			for (int item = 0; item < needs.size(); item++) {
				if (special.get(spIdx).get(item) > needs.get(item)) {
					match = false;
					break;
				}
			}
			if (!match)
				continue;

			List<Integer> newOwn = new ArrayList<>();
			List<Integer> newNeeds = new ArrayList<>();

			for (int item = 0; item < needs.size(); item++) {
				newOwn.add(owned.get(item) + special.get(spIdx).get(item));
				newNeeds.add(needs.get(item) - special.get(spIdx).get(item));
			}

			int tmp = sub(max, curPrice + special.get(spIdx).get(needs.size()), price, special, newOwn, newNeeds);
			if (tmp < max)
				max = tmp;
		}
		return max;
	}

	public static void main(String[] args) {
		List<Integer> price1 = new ArrayList<>();
		price1.add(2);
		price1.add(5);
		List<List<Integer>> special1 = new ArrayList<>();
		special1.add(new ArrayList<Integer>());
		special1.get(0).add(3);
		special1.get(0).add(0);
		special1.get(0).add(5);
		special1.add(new ArrayList<Integer>());
		special1.get(1).add(1);
		special1.get(1).add(2);
		special1.get(1).add(10);
		List<Integer> needs1 = new ArrayList<>();
		needs1.add(3);
		needs1.add(2);

		System.out.println(new Solution().shoppingOffers(price1, special1, needs1));

		List<Integer> price2 = new ArrayList<>();
		price2.add(2);
		price2.add(3);
		price2.add(4);
		List<List<Integer>> special2 = new ArrayList<>();
		special2.add(new ArrayList<Integer>());
		special2.get(0).add(1);
		special2.get(0).add(1);
		special2.get(0).add(0);
		special2.get(0).add(4);
		special2.add(new ArrayList<Integer>());
		special2.get(1).add(2);
		special2.get(1).add(2);
		special2.get(1).add(1);
		special2.get(1).add(9);
		List<Integer> needs2 = new ArrayList<>();
		needs2.add(1);
		needs2.add(2);
		needs2.add(1);

		System.out.println(new Solution().shoppingOffers(price2, special2, needs2));

	}
}
