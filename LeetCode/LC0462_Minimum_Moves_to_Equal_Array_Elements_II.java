import java.util.Arrays;

public class Solution {
	public int minMoves2(int[] nums) {
		Arrays.sort(nums);
		int begin = 0;
		int end = nums.length - 1;
		int moves = 0;
		while (begin < end) {
			moves += nums[end] - nums[begin];
			begin++;
			end--;
		}
		return moves;
	}

	public static void main(String[] args) {
		int[] nums = { 1, 2, 3 };
		System.out.println(new Solution().minMoves2(nums));
	}
}