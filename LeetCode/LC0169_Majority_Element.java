public class Solution {
	public int majorityElement(int[] nums) {
		int mIdx = 0;
		int cnt = 0;
		for (int i = 0; i < nums.length; i++) {
			if (cnt == 0)
				mIdx = i;

			if (nums[i] == nums[mIdx])
				cnt++;
			else
				cnt--;

		}
		return nums[mIdx];
	}

	public static void main(String[] args) {
		int[] nums = { 3, 2, 3 };
		System.out.println(new Solution().majorityElement(nums));
	}
}