public class NumArray {
	private int[] sums;

	public NumArray(int[] nums) {
		if (nums.length == 0)
			return;

		this.sums = new int[nums.length];
		this.sums[0] = nums[0];
		for (int i = 1; i < nums.length; i++) {
			this.sums[i] = nums[i] + this.sums[i - 1];
		}
	}

	public int sumRange(int i, int j) {
		if (i == 0)
			return sums[j];

		return sums[j] - sums[i - 1];
	}

	public static void main(String[] args) {
		int[] nums = { -2, 0, 3, -5, 2, -1 };
		NumArray arr = new NumArray(nums);
		System.out.println(arr.sumRange(0, 2));
		System.out.println(arr.sumRange(2, 5));
		System.out.println(arr.sumRange(0, 5));
	}
}