public class Solution {
	public boolean checkRecord(String s) {
		int absentCnt = 0;
		int lateCnt = 0;
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == 'L') {
				if (lateCnt > 1)
					return false;
				else
					lateCnt++;
			} else {
				lateCnt = 0;
				if (s.charAt(i) == 'A') {
					absentCnt++;
					if (absentCnt > 1)
						return false;
				}
			}
		}
		return true;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().checkRecord("PPALLP"));
		System.out.println(new Solution().checkRecord("PPALLL"));
	}
}