public class Solution {
	public String addBoldTag(String s, String[] dict) {
		if (dict == null || dict.length == 0)
			return s;

		boolean[] bold = new boolean[s.length()];
		int minLen = Integer.MAX_VALUE;
		for (String word : dict)
			if (word.length() < minLen)
				minLen = word.length();

		int pointer = 0;
		while (pointer < s.length()) {
			for (String word : dict) {
				int idx = s.substring(pointer).indexOf(word);
				if (idx != -1) {
					for (int i = 0; i < word.length(); i++)
						bold[pointer + idx + i] = true;
				}
			}
			pointer += minLen > 1 ? minLen - 1 : minLen;
		}
		StringBuffer sb = new StringBuffer();
		int idx = 0;
		boolean processBold = false;
		while (idx < s.length()) {
			if (bold[idx] && !processBold) {
				sb.append("<b>");
				processBold = true;
			}
			if (!bold[idx] && processBold) {
				sb.append("</b>");
				processBold = false;
			}
			sb.append(s.charAt(idx));
			idx++;
		}

		if (processBold)
			sb.append("</b>");

		return sb.toString();

	}

	public static void main(String[] args) {
		String[] dict1 = { "abc", "123" };
		System.out.println(new Solution().addBoldTag("abcxyz123", dict1));

		String[] dict2 = { "aaa", "aab", "bc" };
		System.out.println(new Solution().addBoldTag("aaabbcc", dict2));

		String[] dict3 = {};
		System.out.println(new Solution().addBoldTag("aaabbcc", dict3));
	}
}