public class Solution {
	public int arrangeCoins(int n) {
		long L = (long) n;
		long beg = 1;

		long end = (long) Math.sqrt(2 * L + 1);
		while (beg <= end) {
			long mid = (beg + end) / 2;
			long coins = mid * (mid + 1) / 2;
			if (coins == L)
				return (int) mid;
			else if (coins > L)
				end = mid - 1;
			else
				beg = mid + 1;
		}
		return (int) end;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().arrangeCoins(5));
		System.out.println(new Solution().arrangeCoins(8));
	}
}