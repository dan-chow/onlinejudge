import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Solution {
	public List<List<Integer>> levelOrder(TreeNode root) {
		List<List<Integer>> list = new ArrayList<>();
		Queue<TreeNode> queue = new LinkedList<>();

		if (root != null)
			queue.add(root);

		int level = 0;
		while (!queue.isEmpty()) {
			int levelNodeNum = queue.size();
			list.add(new ArrayList<Integer>());
			for (int i = 0; i < levelNodeNum; i++) {
				TreeNode node = queue.poll();
				list.get(level).add(node.val);
				if (node.left != null)
					queue.add(node.left);
				if (node.right != null)
					queue.add(node.right);
			}
			level++;
		}
		return list;

	}

	public static void main(String[] args) {

		TreeNode root = new TreeNode(3);
		root.left = new TreeNode(9);
		root.right = new TreeNode(20);
		root.right.left = new TreeNode(15);
		root.right.right = new TreeNode(7);

		List<List<Integer>> list = new Solution().levelOrder(root);

		for (List<Integer> sublist : list)
			for (int i : sublist)
				System.out.println(i);

	}
}