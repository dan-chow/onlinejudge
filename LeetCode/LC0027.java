public class Solution {
	public int removeElement(int[] nums, int val) {
		int newLen = nums.length;

		int i = 0;
		int j = nums.length - 1;

		while (i <= j) {
			while (i < nums.length && nums[i] != val)
				i++;
			while (j >= 0 && nums[j] == val) {
				j--;
				newLen--;
			}

			if (i < j) {
				nums[i] = nums[j];
				newLen--;
				i++;
				j--;
			}

		}
		return newLen;
	}

	public static void main(String[] args) {
		int[] nums = { 3, 2, 2, 3 };
		System.out.println(new Solution().removeElement(nums, 3));
		for (int n : nums)
			System.out.println(n);
	}
}