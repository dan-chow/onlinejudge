public class Solution {
	public String convertToBase7(int num) {
		if (num == 0)
			return "0";

		StringBuffer sb = new StringBuffer();
		boolean sign = false;
		if (num < 0) {
			sign = true;
			num = -num;
		}
		while (num > 0) {
			sb.append(num % 7);
			num = num / 7;
		}
		if (sign)
			return sb.reverse().insert(0, '-').toString();
		else
			return sb.reverse().toString();
	}

	public static void main(String[] args) {
		System.out.println(new Solution().convertToBase7(100));
		System.out.println(new Solution().convertToBase7(-7));
	}
}