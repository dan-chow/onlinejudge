public class Solution {
	public int[][] matrixReshape(int[][] nums, int r, int c) {

		if (nums == null || nums.length == 0 || nums[0].length == 0)
			return nums;

		int m = nums.length;
		int n = nums[0].length;
		if (m * n != r * c)
			return nums;

		int[][] res = new int[r][c];
		int row0 = 0, col0 = 0, row1 = 0, col1 = 0;
		for (int i = 0; i < m * n; i++) {
			res[row1][col1] = nums[row0][col0];

			col0 = (col0 + 1) % n;
			row0 = (col0 == 0) ? row0 + 1 : row0;

			col1 = (col1 + 1) % c;
			row1 = (col1 == 0) ? row1 + 1 : row1;
		}
		return res;
	}

	public static void main(String[] args) {
		int[][] nums = { { 1, 2 }, { 3, 4 } };
		int[][] res1 = new Solution().matrixReshape(nums, 1, 4);
		for (int[] row : res1) {
			for (int i : row) {
				System.out.print(i + " ");
			}
			System.out.println();
		}

		int[][] res2 = new Solution().matrixReshape(nums, 2, 4);
		for (int[] row : res2) {
			for (int i : row) {
				System.out.print(i + " ");
			}
			System.out.println();
		}
	}
}
