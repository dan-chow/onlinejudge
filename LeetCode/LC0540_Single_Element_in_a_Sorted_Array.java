public class Solution {
	public int singleNonDuplicate(int[] nums) {

		int begin = 0;
		int end = nums.length - 1;
		while (begin < end) {
			int mid = (begin + end) / 2;
			if (mid % 2 == 0) {
				if (nums[mid] == nums[mid ^ 1]) {
					begin = mid + 2;
				} else {
					end = mid;
				}
			} else {
				if (nums[mid] == nums[mid ^ 1]) {
					begin = mid + 1;
				} else {
					end = mid - 1;
				}
			}

		}

		return nums[begin];
	}

	public static void main(String[] args) {
		int[] nums1 = { 1, 1, 2, 3, 3, 4, 4, 8, 8 };
		System.out.println(new Solution().singleNonDuplicate(nums1));
		int[] nums2 = { 3, 3, 7, 7, 10, 11, 11 };
		System.out.println(new Solution().singleNonDuplicate(nums2));
	}
}