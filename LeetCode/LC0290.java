import java.util.HashMap;

public class Solution {
	public boolean wordPattern(String pattern, String str) {
		HashMap<Character, String> map = new HashMap<>();
		String[] tokens = str.split(" ");
		if (pattern.length() != tokens.length)
			return false;
		for (int i = 0; i < pattern.length(); i++) {
			if (map.containsKey(pattern.charAt(i))) {
				if (!map.get(pattern.charAt(i)).equals(tokens[i]))
					return false;
			} else if (map.containsValue(tokens[i])) {
				return false;
			} else {
				map.put(pattern.charAt(i), tokens[i]);
			}

		}
		return true;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().wordPattern("abba", "dog cat cat dog"));
		System.out.println(new Solution().wordPattern("abba", "dog cat cat fish"));
		System.out.println(new Solution().wordPattern("aaaa", "dog cat cat dog"));
		System.out.println(new Solution().wordPattern("abba", "dog dog dog dog"));
	}
}