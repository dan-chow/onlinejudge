import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Solution {
	HashMap<Character, String> map;

	public Solution() {
		map = new HashMap<>();
		map.put('2', "abc");
		map.put('3', "def");
		map.put('4', "ghi");
		map.put('5', "jkl");
		map.put('6', "mno");
		map.put('7', "pqrs");
		map.put('8', "tuv");
		map.put('9', "wxyz");
	}

	public List<String> letterCombinations(String digits) {
		List<String> list = new ArrayList<>();
		if ("".equals(digits.trim()))
			return list;

		char firstChar = digits.charAt(0);
		for (char c : map.get(firstChar).toCharArray()) {
			String prefix = String.valueOf(c);
			String suffix = digits.substring(1);
			addToList(list, prefix, suffix);
		}
		return list;
	}

	public void addToList(List<String> list, String prefix, String suffix) {
		if (suffix.length() == 0) {
			list.add(prefix);
			return;
		}

		char firstChar = suffix.charAt(0);
		for (char c : map.get(firstChar).toCharArray()) {
			String newPrefix = prefix + c;
			String newSuffix = suffix.substring(1);
			addToList(list, newPrefix, newSuffix);
		}
	}

	public static void main(String[] args) {
		List<String> res = new Solution().letterCombinations("23");
		for (String s : res)
			System.out.println(s);
	}
}