import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution {
	public List<List<Integer>> fourSum(int[] nums, int target) {

		List<List<Integer>> res = new ArrayList<>();
		if (nums == null || nums.length < 4)
			return res;

		Arrays.sort(nums);

		for (int i = 0; i < nums.length - 3;) {
			for (int j = i + 1; j < nums.length - 2;) {
				int tmpTarget = target - nums[i] - nums[j];

				int m = j + 1, n = nums.length - 1;
				while (m < n) {
					int tmpSum = nums[m] + nums[n];
					if (tmpSum == tmpTarget) {
						List<Integer> list = new ArrayList<>();
						list.add(nums[i]);
						list.add(nums[j]);
						list.add(nums[m]);
						list.add(nums[n]);
						res.add(list);

						m++;
						n--;

						while (n > m && nums[n] == nums[n + 1])
							n--;
						while (m < n && nums[m] == nums[m - 1])
							m++;
					} else if (tmpSum > tmpTarget) {
						n--;
						while (n > m && nums[n] == nums[n + 1])
							n--;
					} else {
						m++;
						while (m < n && nums[m] == nums[m - 1])
							m++;
					}
				}
				j++;
				while (j < nums.length && nums[j] == nums[j - 1])
					j++;
			}

			i++;
			while (i < nums.length && nums[i] == nums[i - 1])
				i++;
		}
		return res;
	}

	public static void main(String[] args) {
		int[] nums = { 1, 0, -1, 0, -2, 2 };
		List<List<Integer>> res = new Solution().fourSum(nums, 0);
		for (List<Integer> list : res)
			System.out.println(list);
	}
}