class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Solution {
	public int findTilt(TreeNode root) {
		if (root == null)
			return 0;

		int tilt = Math.abs(getSum(root.left) - getSum(root.right));

		return tilt + findTilt(root.left) + findTilt(root.right);
	}

	private int getSum(TreeNode root) {
		if (root == null)
			return 0;

		int sum = root.val;
		if (root.left != null)
			sum += getSum(root.left);
		if (root.right != null)
			sum += getSum(root.right);
		return sum;
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(3);
		System.out.println(new Solution().findTilt(root));
	}
}