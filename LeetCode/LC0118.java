import java.util.ArrayList;
import java.util.List;

public class Solution {
	public List<List<Integer>> generate(int numRows) {
		List<List<Integer>> res = new ArrayList<>();

		if (numRows == 0)
			return res;

		List<Integer> list = new ArrayList<>();

		list.add(1);
		res.add(new ArrayList<>(list));

		for (int i = 1; i < numRows; i++) {
			List<Integer> tmp = new ArrayList<>();
			tmp.add(1);
			for (int j = 1; j < list.size(); j++) {
				tmp.add(list.get(j) + list.get(j - 1));
			}
			tmp.add(1);

			res.add(new ArrayList<>(tmp));
			list = tmp;
		}
		return res;
	}

	public static void main(String[] args) {
		List<List<Integer>> res = new Solution().generate(5);
		for (List<Integer> list : res)
			System.out.println(list);
	}
}