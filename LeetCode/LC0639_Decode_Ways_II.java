public class Solution {

	public int numDecodings(String s) {

		int mod = 1000000000 + 7;
		if (s == null || s.isEmpty() || s.startsWith("0"))
			return 0;

		long[] dp = new long[s.length() + 1];
		dp[0] = 1;

		if (s.charAt(0) == '*')
			dp[1] = 9;
		else
			dp[1] = 1;

		for (int i = 2; i <= s.length(); i++) {
			if (s.charAt(i - 1) == '0') {

				if (s.charAt(i - 2) == '1' || s.charAt(i - 2) == '2')
					dp[i] = (dp[i] + dp[i - 2]) % mod;
				else if (s.charAt(i - 2) == '*')
					dp[i] = (dp[i] + 2 * dp[i - 2]) % mod;
				else
					return 0;

			} else if (s.charAt(i - 1) == '*') {

				dp[i] = (dp[i] + 9 * dp[i - 1]) % mod;

				if (s.charAt(i - 2) == '1')
					dp[i] = (dp[i] + 9 * dp[i - 2]) % mod;
				else if (s.charAt(i - 2) == '2')
					dp[i] = (dp[i] + 6 * dp[i - 2]) % mod;
				else if (s.charAt(i - 2) == '*')
					dp[i] = (dp[i] + 15 * dp[i - 2]) % mod;

			} else {
				dp[i] += dp[i - 1];

				if (s.charAt(i - 2) == '1' && s.charAt(i - 1) >= '0' && s.charAt(i - 1) <= '9')
					dp[i] = (dp[i] + dp[i - 2]) % mod;
				else if (s.charAt(i - 2) == '2' && s.charAt(i - 1) >= '0' && s.charAt(i - 1) <= '6')
					dp[i] = (dp[i] + dp[i - 2]) % mod;
				else if (s.charAt(i - 2) == '*') {
					if (s.charAt(i - 1) >= '0' && s.charAt(i - 1) <= '9')
						dp[i] = (dp[i] + dp[i - 2]) % mod;
					if (s.charAt(i - 1) >= '0' && s.charAt(i - 1) <= '6')
						dp[i] = (dp[i] + dp[i - 2]) % mod;
				}
			}
		}

		return (int) dp[s.length()];
	}

	public static void main(String[] args) {
		System.out.println(new Solution().numDecodings("*"));
		System.out.println(new Solution().numDecodings("1*"));
	}

}