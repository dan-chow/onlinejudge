class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Solution {
	public int diameterOfBinaryTree(TreeNode root) {
		if (root == null)
			return 0;
		if (root.left == null && root.right == null)
			return 0;

		int max = maxHeight(root.left) + maxHeight(root.right);
		if (root.left != null) {
			int maxLeft = diameterOfBinaryTree(root.left);
			max = maxLeft > max ? maxLeft : max;
		}
		if (root.right != null) {
			int maxRight = diameterOfBinaryTree(root.right);
			max = maxRight > max ? maxRight : max;
		}
		return max;
	}

	private int maxHeight(TreeNode root) {
		if (root == null)
			return 0;
		return 1 + Math.max(maxHeight(root.left), maxHeight(root.right));
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.left.left = new TreeNode(4);
		root.left.right = new TreeNode(5);
		root.right = new TreeNode(3);

		System.out.println(new Solution().diameterOfBinaryTree(root));
	}

}