public class Solution {
	public int numberOfArithmeticSlices(int[] A) {
		boolean[][] dp = new boolean[A.length][A.length];

		for (int i = 0; i <= A.length - 3; i++) {
			if (A[i] + A[i + 2] == 2 * A[i + 1])
				dp[i][i + 2] = true;
			else
				continue;

			for (int j = i + 3; j < A.length; j++) {
				if (A[j] + A[j - 2] == 2 * A[j - 1])
					dp[i][j] = true;
				else
					break;
			}
		}

		int cnt = 0;
		for (int i = 0; i <= A.length - 3; i++)
			for (int j = i + 2; j < A.length; j++)
				if (dp[i][j])
					cnt++;
		return cnt;
	}

	public static void main(String[] args) {
		int[] A = { 1, 2, 3, 4 };
		System.out.println(new Solution().numberOfArithmeticSlices(A));
	}
}