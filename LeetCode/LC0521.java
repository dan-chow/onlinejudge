public class Solution {
	public int findLUSlength(String a, String b) {
		if (a == null || a.isEmpty())
			return (b == null || b.isEmpty()) ? -1 : b.length();
		if (b == null || b.isEmpty())
			return (a == null || a.isEmpty()) ? -1 : a.length();

		return a.equals(b) ? -1 : Math.max(a.length(), b.length());
	}

	public static void main(String[] args) {
		System.out.println(new Solution().findLUSlength("aba", "cdc"));
	}
}