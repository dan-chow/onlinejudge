import java.util.LinkedList;
import java.util.Queue;

public class MyStack {
	private Queue<Integer> primaryQueue;
	private Queue<Integer> secondaryQueue;

	/** Initialize your data structure here. */
	public MyStack() {
		this.primaryQueue = new LinkedList<>();
		this.secondaryQueue = new LinkedList<>();
	}

	/** Push element x onto stack. */
	public void push(int x) {

		secondaryQueue.add(x);
		while (!primaryQueue.isEmpty())
			secondaryQueue.add(primaryQueue.poll());

		Queue<Integer> tmp = primaryQueue;
		primaryQueue = secondaryQueue;
		secondaryQueue = tmp;
	}

	/** Removes the element on top of the stack and returns that element. */
	public int pop() {
		return primaryQueue.poll();
	}

	/** Get the top element. */
	public int top() {
		return primaryQueue.peek();
	}

	/** Returns whether the stack is empty. */
	public boolean empty() {
		return primaryQueue.isEmpty();
	}

	public static void main(String[] args) {
		MyStack myStack = new MyStack();

		myStack.push(1);
		myStack.push(2);
		myStack.push(3);
		myStack.push(4);

		System.out.println(myStack.top());
		System.out.println(myStack.pop());

		System.out.println(myStack.pop());

		System.out.println(myStack.empty());

		System.out.println(myStack.pop());
		System.out.println(myStack.pop());

		System.out.println(myStack.empty());
	}
}
