public class Solution {
	// return n > 0 && Integer.bitCount(n) == 1;
	public boolean isPowerOfTwo(int n) {
		if (n == 0)
			return false;

		while (n % 2 == 0)
			n /= 2;

		return n == 1;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().isPowerOfTwo(32));
	}
}