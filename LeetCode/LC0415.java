public class Solution {

	public String addStrings(String num1, String num2) {

		if (num1.length() > num2.length())
			return addStrings(num2, num1);

		StringBuffer sum = new StringBuffer();

		int carray = 0;
		int tmp = 0;
		int i = 0;
		for (; i < num1.length(); i++) {
			tmp = num1.charAt(num1.length() - 1 - i) - '0' + num2.charAt(num2.length() - 1 - i) - '0' + carray;
			carray = tmp / 10;
			sum.append(tmp % 10);
		}

		for (; i < num2.length(); i++) {
			tmp = num2.charAt(num2.length() - 1 - i) - '0' + carray;
			carray = tmp / 10;
			sum.append(tmp % 10);
		}

		if (carray > 0)
			sum.append(carray);

		return sum.reverse().toString();

	}

	public static void main(String[] args) {
		System.out.println(new Solution().addStrings("255", "255"));
	}

}
