import java.util.ArrayList;
import java.util.List;

public class Solution {
	public List<String> readBinaryWatch(int num) {
		int[] hoursBits = { 8, 4, 2, 1 };
		int[] minitesBits = { 32, 16, 8, 4, 2, 1 };

		List<String> result = new ArrayList<>();

		for (int i = 0; i <= num; i++) {
			List<Integer> hours = buildNums(hoursBits, i);
			List<Integer> minites = buildNums(minitesBits, num - i);
			for (int h : hours) {
				if (h >= 12)
					continue;

				for (int m : minites) {
					if (m >= 60)
						continue;

					result.add(new StringBuffer().append(h).append(":").append(String.format("%02d", m)).toString());
				}
			}
		}
		return result;
	}

	private List<Integer> buildNums(int[] arr, int cnt) {
		List<Integer> list = new ArrayList<>();
		buildNums(arr, cnt, 0, 0, list);
		return list;
	}

	private void buildNums(int[] arr, int cnt, int pos, int tmpSum, List<Integer> list) {
		if (cnt == 0) {
			list.add(tmpSum);
			return;
		}

		for (int i = pos; i < arr.length; i++) {
			buildNums(arr, cnt - 1, i + 1, tmpSum + arr[i], list);
		}
	}

	public static void main(String[] args) {
		System.out.println(new Solution().readBinaryWatch(1));

	}
}
