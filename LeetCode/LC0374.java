class GuessGame {
	int target = 6;

	public GuessGame() {
	}

	public GuessGame(int target) {
		this.target = target;
	}

	protected int guess(int num) {
		if (this.target == num)
			return 0;

		if (this.target > num)
			return 1;

		return -1;
	}
}

public class Solution extends GuessGame {
	public Solution(int target) {
		super(target);
	}

	public int guessNumber(int n) {
		int begin = 1;
		int end = n;
		while (begin <= end) {
			int mid = begin + (end - begin) / 2;
			int guessResult = guess(mid);
			if (guessResult == 0)
				return mid;
			else if (guessResult > 0)
				begin = mid + 1;
			else
				end = mid - 1;
		}
		return -1;
	}

	public static void main(String[] args) {
		int n = 2126753390;
		int target = 1702766719;
		System.out.println(new Solution(target).guessNumber(n));
	}
}