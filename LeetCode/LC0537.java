public class Solution {
	public String complexNumberMultiply(String a, String b) {

		int A = Integer.parseInt(a.split("\\+")[0]);
		int B = Integer.parseInt(a.split("\\+")[1].replace("i", ""));

		int C = Integer.parseInt(b.split("\\+")[0]);
		int D = Integer.parseInt(b.split("\\+")[1].replace("i", ""));

		int real = A * C - B * D;
		int img = A * D + B * C;

		return String.format("%d+%di", real, img);
	}

	public static void main(String[] args) {
		System.out.println(new Solution().complexNumberMultiply("1+1i", "1+1i"));
		System.out.println(new Solution().complexNumberMultiply("1+-1i", "1+-1i"));
	}
}
