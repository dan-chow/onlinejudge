class ListNode {
	int val;
	ListNode next;

	ListNode(int x) {
		val = x;
	}
}

public class Solution {
	public ListNode insertionSortList(ListNode head) {
		if (head == null || head.next == null)
			return head;

		ListNode p = head.next, q = null;
		head.next = null;

		while (p != null) {
			q = p;
			p = p.next;
			head = insert(head, q);
		}
		return head;
	}

	private ListNode insert(ListNode head, ListNode node) {
		if (head.val >= node.val) {
			node.next = head;
			head = node;
			return head;
		}

		ListNode q = head, p = null;
		while (q != null && q.val < node.val) {
			p = q;
			q = q.next;
		}

		node.next = p.next;
		p.next = node;
		return head;
	}

	public static void main(String[] args) {
		ListNode head = new ListNode(3);
		head.next = new ListNode(5);
		head.next.next = new ListNode(1);
		head.next.next.next = new ListNode(4);
		head.next.next.next.next = new ListNode(2);

		head = new Solution().insertionSortList(head);

		ListNode p = head;
		while (p != null) {
			System.out.println(p.val);
			p = p.next;
		}
	}
}