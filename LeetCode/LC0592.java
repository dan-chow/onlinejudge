public class Solution {
	public String fractionAddition(String expression) {
		int sign = 1;
		int numerator = 0;
		int denominator = 0;

		if (expression == null || expression.length() == 0)
			return "";

		boolean processNumerator = true;

		int i = 0;
		if (expression.charAt(i) == '+' || expression.charAt(i) == '-')
			sign = expression.charAt(i++) == '+' ? 1 : -1;

		for (; i < expression.length(); i++) {
			if (expression.charAt(i) == '+' || expression.charAt(i) == '-') {
				processNumerator = true;
				break;
			} else if (expression.charAt(i) == '/') {
				processNumerator = false;
			} else if (processNumerator)
				numerator = numerator * 10 + expression.charAt(i) - '0';
			else
				denominator = denominator * 10 + expression.charAt(i) - '0';
		}

		if (i >= expression.length() - 1)
			return (sign == 1 ? "" : "-") + numerator + "/" + denominator;

		int newSign = expression.charAt(i++) == '+' ? 1 : -1;
		int newNumerator = 0;
		int newDenominator = 0;
		for (; i < expression.length(); i++) {
			if (i == expression.length() - 1 || expression.charAt(i) == '+' || expression.charAt(i) == '-') {
				if (i == expression.length() - 1)
					newDenominator = newDenominator * 10 + expression.charAt(i) - '0';

				processNumerator = true;

				if (numerator == 0) {
					sign = newSign;
					numerator = newNumerator;
					denominator = newDenominator;
					continue;
				}

				int gcm = gcm(denominator, newDenominator);
				numerator = sign * gcm / denominator * numerator;
				newNumerator = newSign * gcm / newDenominator * newNumerator;

				numerator = numerator + newNumerator;
				sign = numerator >= 0 ? 1 : -1;
				numerator = sign * numerator;

				if (i != expression.length() - 1)
					newSign = expression.charAt(i) == '+' ? 1 : -1;
				newNumerator = 0;
				newDenominator = 0;

				if (numerator == 0) {
					numerator = 0;
					denominator = 0;
					continue;
				}

				int gcd = gcd(numerator, gcm);
				numerator = numerator / gcd;
				denominator = gcm / gcd;
			} else if (expression.charAt(i) == '/')
				processNumerator = false;
			else if (processNumerator)
				newNumerator = newNumerator * 10 + expression.charAt(i) - '0';
			else
				newDenominator = newDenominator * 10 + expression.charAt(i) - '0';
		}

		return (sign == 1 ? "" : "-") + numerator + "/" + (denominator == 0 ? 1 : denominator);

	}

	public static int gcd(int m, int n) {
		if (m < n)
			return gcd(n, m);
		if (n == 0)
			return m;
		return gcd(n, m % n);
	}

	public static int gcm(int m, int n) {
		return m * n / gcd(m, n);
	}

	public static void main(String[] args) {
		System.out.println(new Solution().fractionAddition("-1/2+1/2"));
		System.out.println(new Solution().fractionAddition("-1/2+1/2+1/3"));
		System.out.println(new Solution().fractionAddition("1/3-1/2"));
		System.out.println(new Solution().fractionAddition("5/3+1/3"));
	}
}