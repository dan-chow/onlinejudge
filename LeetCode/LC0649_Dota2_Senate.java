import java.util.LinkedList;
import java.util.Queue;

public class Solution {
	public String predictPartyVictory(String senate) {
		if (senate.isEmpty())
			return "";

		int rCnt = 0;
		int dCnt = 0;
		Queue<Character> queue = new LinkedList<>();
		for (int i = 0; i < senate.length(); i++) {
			char c = senate.charAt(i);

			if (c == 'R')
				rCnt++;
			else
				dCnt++;

			queue.add(c);
		}

		int banedR = 0;
		int banedD = 0;

		while (!queue.isEmpty() && rCnt > 0 && dCnt > 0) {
			char c = queue.poll();
			if (c == 'R') {
				if (banedR > 0) {
					rCnt--;
					banedR--;
				} else {
					banedD++;
					queue.add(c);
				}
			} else {
				if (banedD > 0) {
					dCnt--;
					banedD--;
				} else {
					banedR++;
					queue.add(c);
				}
			}
		}

		return rCnt > 0 ? "Radiant" : "Dire";
	}

	public static void main(String[] args) {
		System.out.println(new Solution().predictPartyVictory("RD"));
		System.out.println(new Solution().predictPartyVictory("RDD"));
		System.out.println(new Solution().predictPartyVictory("DDRRR"));
	}
}