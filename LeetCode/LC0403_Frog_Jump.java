import java.util.HashMap;
import java.util.HashSet;

public class Solution {
	public boolean canCross(int[] stones) {
		if (stones == null || stones.length == 0)
			return true;

		HashMap<Integer, HashSet<Integer>> map = new HashMap<>();
		for (int i = 0; i < stones.length; i++)
			map.put(stones[i], new HashSet<Integer>());
		map.get(0).add(1);

		for (int i = 0; i < map.size(); i++) {
			for (int step : map.get(stones[i])) {
				int reach = stones[i] + step;
				if (reach == stones[stones.length - 1])
					return true;
				if (map.containsKey(reach)) {
					map.get(reach).add(step);
					if (step - 1 > 0)
						map.get(reach).add(step - 1);
					map.get(reach).add(step + 1);
				}
			}
		}
		return false;
	}

	public static void main(String[] args) {
		int[] stones1 = { 0, 1, 3, 5, 6, 8, 12, 17 };
		System.out.println(new Solution().canCross(stones1));

		int[] stones2 = { 0, 1, 2, 3, 4, 8, 9, 11 };
		System.out.println(new Solution().canCross(stones2));
	}
}