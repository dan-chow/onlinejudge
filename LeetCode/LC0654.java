import java.util.LinkedList;
import java.util.Queue;

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Solution {
	public TreeNode constructMaximumBinaryTree(int[] nums) {
		return constructMaximumBinaryTree(nums, 0, nums.length - 1);
	}

	private TreeNode constructMaximumBinaryTree(int[] nums, int beg, int end) {
		if (beg > end)
			return null;

		int max = nums[beg];
		int maxIdx = beg;

		for (int i = beg; i <= end; i++) {
			if (nums[i] > max) {
				max = nums[i];
				maxIdx = i;
			}
		}

		TreeNode node = new TreeNode(max);
		node.left = constructMaximumBinaryTree(nums, beg, maxIdx - 1);
		node.right = constructMaximumBinaryTree(nums, maxIdx + 1, end);

		return node;
	}

	public static void main(String[] args) {
		int[] nums = { 3, 2, 1, 6, 0, 5 };
		TreeNode node = new Solution().constructMaximumBinaryTree(nums);
		Queue<TreeNode> queue = new LinkedList<>();
		queue.add(node);
		while (!queue.isEmpty()) {
			int levelSize = queue.size();
			while (levelSize-- > 0) {
				TreeNode tmp = queue.poll();
				System.out.print(tmp.val + " ");

				if (tmp.left != null)
					queue.add(tmp.left);

				if (tmp.right != null)
					queue.add(tmp.right);
			}
			System.out.println();
		}
	}
}