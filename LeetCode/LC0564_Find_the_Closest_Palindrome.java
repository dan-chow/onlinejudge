public class Solution {
	public String nearestPalindromic(String n) {
		if (n.equals("1"))
			return "0";

		String nearest = mirror(n);
		long minDiff = nearest.equals(n) ? Long.MAX_VALUE : Math.abs(Long.parseLong(nearest) - Long.parseLong(n));

		String mirrorInc = mirror(incrementMid(n, (n.length() - 1) / 2));
		long diffInc = mirrorInc.equals(n) ? Long.MAX_VALUE : Math.abs(Long.parseLong(mirrorInc) - Long.parseLong(n));
		if (diffInc < minDiff) {
			minDiff = diffInc;
			nearest = mirrorInc;
		}

		String mirrorDesc = mirror(decrementMid(n, (n.length() - 1) / 2));
		long diffDesc = mirrorDesc.equals(n) ? Long.MAX_VALUE : Math.abs(Long.parseLong(mirrorDesc) - Long.parseLong(n));
		return minDiff < diffDesc ? nearest : mirrorDesc;

	}

	private String mirror(String s) {
		char[] arr = s.toCharArray();
		for (int i = 0; i < s.length() / 2; i++)
			arr[arr.length - 1 - i] = arr[i];

		return new String(arr);
	}

	private String incrementMid(String n, int idx) {
		StringBuffer sb = new StringBuffer(n);
		if (sb.charAt(idx) == '9') {
			while (idx >= 0 && sb.charAt(idx) == '9') {
				sb.setCharAt(idx, '0');
				idx--;
			}
		}

		if (idx < 0) {
			sb.insert(0, '1');
		} else {
			sb.setCharAt(idx, (char) (sb.charAt(idx) + 1));
		}
		return sb.toString();
	}

	private String decrementMid(String n, int idx) {
		StringBuffer sb = new StringBuffer(n);
		if (sb.charAt(idx) == '0') {
			while (idx >= 0 && sb.charAt(idx) == '0') {
				sb.setCharAt(idx, '9');
				idx--;
			}
		}

		if (idx == 0 && sb.charAt(idx) == '1') {
			sb.delete(idx, idx + 1);
			sb.setCharAt((sb.length() - 1) / 2, '9');
		} else {
			sb.setCharAt(idx, (char) (sb.charAt(idx) - 1));
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		System.out.println(new Solution().nearestPalindromic("123"));
	}
}