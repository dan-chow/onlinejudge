public class Solution {
	public int islandPerimeter(int[][] grid) {
		int totalOnes = 0;
		int totalNbrs = 0;
		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid[i].length; j++) {
				if (grid[i][j] != 0) {
					totalOnes++;
					totalNbrs += getNbrNum(grid, i, j);
				}
			}
		}
		return 4 * totalOnes - totalNbrs;

	}

	private int getNbrNum(int[][] grid, int row, int col) {
		int nbrCnt = 0;
		if (row > 0)
			if (grid[row - 1][col] != 0)
				nbrCnt++;

		if (col > 0)
			if (grid[row][col - 1] != 0)
				nbrCnt++;

		if (row < grid.length - 1)
			if (grid[row + 1][col] != 0)
				nbrCnt++;

		if (col < grid[0].length - 1)
			if (grid[row][col + 1] != 0)
				nbrCnt++;

		return nbrCnt;
	}

	public static void main(String[] args) {
		int[][] grid = { { 0, 1, 0, 0 }, { 1, 1, 1, 0 }, { 0, 1, 0, 0 }, { 1, 1, 0, 0 } };
		System.out.println(new Solution().islandPerimeter(grid));
	}
}