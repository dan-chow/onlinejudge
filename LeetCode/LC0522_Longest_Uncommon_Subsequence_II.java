import java.util.Arrays;
import java.util.Comparator;

public class Solution {
	public int findLUSlength(String[] strs) {
		Arrays.sort(strs, new Comparator<String>() {

			@Override
			public int compare(String s1, String s2) {
				if (s1.length() != s2.length())
					return s2.length() - s1.length();
				return s1.compareTo(s2);
			}
		});

		for (int i = 0; i < strs.length;) {
			int j = i + 1;
			while (j < strs.length && strs[j].equals(strs[i]))
				j++;

			if (j != i + 1) {
				i = j;
				continue;
			}

			boolean isOk = true;
			for (int prev = 0; prev < i; prev++) {
				if (isSub(strs[prev], strs[i])) {
					isOk = false;
					break;
				}
			}

			if (isOk)
				return strs[i].length();
			else
				i++;
		}
		return -1;
	}

	private boolean isSub(String longStr, String shortStr) {
		int j = 0;
		for (int i = 0; i < longStr.length(); i++) {
			if (longStr.charAt(i) == shortStr.charAt(j))
				j++;

			if (j == shortStr.length())
				return true;
		}

		return false;
	}

	public static void main(String[] args) {
		String[] strs = { "aba", "cdc", "eae" };
		System.out.println(new Solution().findLUSlength(strs));
	}
}