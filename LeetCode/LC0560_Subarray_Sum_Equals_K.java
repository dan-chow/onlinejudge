import java.util.HashMap;

public class Solution {
	public int subarraySum(int[] nums, int k) {

		HashMap<Integer, Integer> sumCntMap = new HashMap<>();
		sumCntMap.put(0, 1);

		int tmpSum = 0, cnt = 0;
		for (int i = 0; i < nums.length; i++) {
			tmpSum += nums[i];
			if (sumCntMap.containsKey(tmpSum - k))
				cnt += sumCntMap.get(tmpSum - k);

			sumCntMap.put(tmpSum, sumCntMap.getOrDefault(tmpSum, 0) + 1);
		}
		return cnt;
	}

	public static void main(String[] args) {
		int[] nums = { 1, 1, 1 };
		System.out.println(new Solution().subarraySum(nums, 2));
	}
}