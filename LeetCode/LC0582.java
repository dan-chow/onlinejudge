import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Solution {
	public List<Integer> killProcess(List<Integer> pid, List<Integer> ppid, int kill) {
		List<Integer> result = new ArrayList<Integer>();
		HashMap<Integer, HashSet<Integer>> map = new HashMap<>();

		for (int i = 0; i < ppid.size(); i++) {
			if (!map.containsKey(ppid.get(i)))
				map.put(ppid.get(i), new HashSet<Integer>());

			map.get(ppid.get(i)).add(pid.get(i));
		}

		Queue<Integer> queue = new LinkedList<Integer>();
		queue.add(kill);
		while (!queue.isEmpty()) {
			kill = queue.poll();
			result.add(kill);
			if (map.containsKey(kill)) {
				for (int p : map.get(kill)) {
					queue.add(p);
				}
			}
		}

		return result;
	}

	public static void main(String[] args) {
		List<Integer> pid = new ArrayList<>();
		pid.add(1);
		pid.add(3);
		pid.add(10);
		pid.add(5);

		List<Integer> ppid = new ArrayList<>();
		ppid.add(3);
		ppid.add(0);
		ppid.add(5);
		ppid.add(3);

		List<Integer> res = new Solution().killProcess(pid, ppid, 5);

		for (int i : res)
			System.out.println(i);

	}
}