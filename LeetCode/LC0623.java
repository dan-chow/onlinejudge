import java.util.LinkedList;
import java.util.Queue;

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Solution {
	public TreeNode addOneRow(TreeNode root, int v, int d) {

		if (d == 1) {
			TreeNode newRoot = new TreeNode(v);
			newRoot.left = root;
			return newRoot;
		}
		if (root == null)
			return null;

		Queue<TreeNode> prevLevel = new LinkedList<>();
		Queue<TreeNode> nextLevel = new LinkedList<>();

		int depth = 2;
		int levelSize = 1;

		prevLevel.add(root);
		if (root.left != null)
			nextLevel.add(root.left);
		if (root.right != null)
			nextLevel.add(root.right);

		while (depth++ < d) {

			Queue<TreeNode> tmp = prevLevel;
			prevLevel = nextLevel;
			nextLevel = tmp;
			nextLevel.clear();

			levelSize = prevLevel.size();

			while (levelSize-- > 0) {
				TreeNode node = prevLevel.poll();
				prevLevel.add(node);
				if (node.left != null)
					nextLevel.add(node.left);

				if (node.right != null)
					nextLevel.add(node.right);
			}
		}

		levelSize = prevLevel.size();
		while (levelSize-- > 0) {
			TreeNode node = prevLevel.poll();

			TreeNode left = new TreeNode(v);
			TreeNode right = new TreeNode(v);

			if (node.left != null)
				left.left = nextLevel.poll();

			if (node.right != null)
				right.right = nextLevel.poll();

			node.left = left;
			node.right = right;
		}

		return root;
	}

	public static void main(String[] args) {
		TreeNode root1 = new TreeNode(4);
		root1.left = new TreeNode(2);
		root1.left.left = new TreeNode(3);
		root1.left.right = new TreeNode(1);
		root1.right = new TreeNode(6);
		root1.right.left = new TreeNode(5);

		TreeNode res1 = new Solution().addOneRow(root1, 1, 2);
		System.out.println(res1);

		TreeNode root2 = new TreeNode(4);
		root2.left = new TreeNode(2);
		root2.left.left = new TreeNode(3);
		root2.left.right = new TreeNode(1);

		TreeNode res2 = new Solution().addOneRow(root2, 1, 3);
		System.out.println(res2);

	}
}