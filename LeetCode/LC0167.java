public class Solution {

	public int[] twoSum(int[] numbers, int target) {
		int i = 0;
		int j = numbers.length - 1;
		while (i <= j) {
			if (numbers[i] + numbers[j] > target)
				j--;
			else if (numbers[i] + numbers[j] < target)
				i++;
			else {
				int[] res = { i + 1, j + 1 };
				return res;
			}

		}
		return null;
	}

	public static void main(String[] args) {
		int[] numbers = { 2, 7, 11, 15 };
		int[] res = new Solution().twoSum(numbers, 9);

		for (int i : res)
			System.out.println(i);

	}
}