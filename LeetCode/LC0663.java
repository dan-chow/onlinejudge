class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Solution {
	public boolean checkEqualTree(TreeNode root) {
		if (root == null || (root.left == null && root.right == null))
			return false;

		int sum = getSum(root);
		if (sum % 2 != 0)
			return false;
		return containSumTree(root, sum / 2);
	}

	private int getSum(TreeNode root) {
		if (root == null)
			return 0;

		return root.val + getSum(root.left) + getSum(root.right);
	}

	private boolean containSumTree(TreeNode root, int sum) {
		if (root == null)
			return false;

		if (getSum(root) == sum)
			return true;
		else
			return containSumTree(root.left, sum) || containSumTree(root.right, sum);
	}

	public static void main(String[] args) {
		TreeNode root1 = new TreeNode(5);
		root1.left = new TreeNode(10);
		root1.right = new TreeNode(10);
		root1.right.left = new TreeNode(2);
		root1.right.right = new TreeNode(3);
		System.out.println(new Solution().checkEqualTree(root1));

		TreeNode root2 = new TreeNode(1);
		root2.left = new TreeNode(2);
		root2.right = new TreeNode(10);
		root2.right.left = new TreeNode(2);
		root2.right.right = new TreeNode(20);
		System.out.println(new Solution().checkEqualTree(root2));
	}
}