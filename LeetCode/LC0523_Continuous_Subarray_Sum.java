import java.util.HashSet;

public class Solution {
	public boolean checkSubarraySum(int[] nums, int k) {
		if (k == 0) {
			if (nums.length < 2)
				return false;
			for (int i = 0; i < nums.length - 1; i++)
				if (nums[i] == nums[i + 1])
					return true;
			return false;
		}

		HashSet<Integer> sumSet = new HashSet<>();
		int tmpSum = 0;
		for (int i = 0; i < nums.length; i++) {
			tmpSum = (tmpSum + nums[i]) % k;

			if (sumSet.contains(tmpSum))
				return true;

			if (tmpSum == 0 && nums[i] % k != 0)
				return true;

			sumSet.add(tmpSum);
		}
		return false;
	}

	public static void main(String[] args) {
		int[] nums1 = { 23, 2, 4, 6, 7 };
		System.out.println(new Solution().checkSubarraySum(nums1, 6));

		int[] nums2 = { 23, 2, 6, 4, 7 };
		System.out.println(new Solution().checkSubarraySum(nums2, 6));
	}
}