import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution {
	public List<List<Integer>> threeSum(int[] nums) {
		Arrays.sort(nums);

		List<List<Integer>> list = new ArrayList<>();
		for (int i = 0; i < nums.length - 2; i++) {
			if (i > 0 && nums[i] == nums[i - 1])
				continue;

			int j = i + 1;
			int k = nums.length - 1;
			while (j < k) {
				if (nums[i] + nums[j] + nums[k] > 0)
					k--;
				else if (nums[i] + nums[j] + nums[k] < 0)
					j++;
				else {
					list.add(Arrays.asList(nums[i], nums[j], nums[k]));

					while (j < k && nums[j + 1] == nums[j])
						j++;
					while (k > j && nums[k - 1] == nums[k])
						k--;

					j++;
					k--;
				}
			}
		}
		return list;
	}

	public static void main(String[] args) {
		int[] nums = { -1, 0, 1, 2, -1, -4 };

		List<List<Integer>> list = new Solution().threeSum(nums);
		for (List<Integer> sublist : list) {
			for (int i : sublist)
				System.out.println(i);
		}
	}
}