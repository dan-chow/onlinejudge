public class Solution {
	public int integerReplacement(int n) {

		int step = 0;
		while (n > 1) {
			if (n % 2 == 0) {
				n /= 2;
			} else if (n == Integer.MAX_VALUE) {
				// n+1 will overflow
				n = (n - 1) / 2 + 1;
				step++;
			} else if ((n + 1) % 4 == 0 && n - 1 != 2) {
				n++;
			} else {
				n--;
			}

			step++;
		}
		return step;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().integerReplacement(8));
		System.out.println(new Solution().integerReplacement(7));
	}
}