import java.util.ArrayList;
import java.util.List;

public class Solution {
	public List<String> generateParenthesis(int n) {
		List<String> res = new ArrayList<>();
		generateParenthesis("", n, 0, res);
		return res;
	}

	private void generateParenthesis(String str, int toOpen, int toClose, List<String> res) {
		if (toOpen == 0) {
			StringBuffer sb = new StringBuffer(str);
			for (int i = 0; i < toClose; i++)
				sb.append(")");
			res.add(sb.toString());
			return;
		}

		generateParenthesis(str + "(", toOpen - 1, toClose + 1, res);

		if (toClose > 0)
			generateParenthesis(str + ")", toOpen, toClose - 1, res);
	}

	public static void main(String[] args) {
		List<String> res = new Solution().generateParenthesis(3);
		System.out.println(res);
	}

}