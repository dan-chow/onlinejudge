public class Solution {
	public String longestPalindrome(String s) {
		if (s == null || s.length() == 0)
			return "";

		int start = s.length() - 1;
		int end = s.length() - 1;

		// dp[i][j] = isPalindrome(s.substring(i, j))
		boolean[][] dp = new boolean[s.length()][s.length()];

		for (int i = s.length() - 1; i >= 0; i--) {
			dp[i][i] = true;
			if (i > 0 && s.charAt(i - 1) == s.charAt(i)) {
				dp[i - 1][i] = true;
				if (end == start) {
					end = i;
					start = i - 1;
				}
			}

			for (int j = i + 2; j < s.length(); j++) {
				dp[i][j] = dp[i + 1][j - 1] && (s.charAt(i) == s.charAt(j));
				if (dp[i][j] && (j - i) > end - start) {
					end = j;
					start = i;
				}
			}
		}

		return s.substring(start, end + 1);
	}

	public static void main(String[] args) {
		System.out.println(new Solution().longestPalindrome("babad"));
		System.out.println(new Solution().longestPalindrome("cbbd"));
	}
}