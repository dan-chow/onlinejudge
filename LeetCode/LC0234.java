class ListNode {
	int val;
	ListNode next;

	ListNode(int x) {
		val = x;
	}
}

public class Solution {
	public boolean isPalindrome(ListNode head) {
		if (head == null || head.next == null)
			return true;

		ListNode p = head, q = head.next;
		while (q != null && q.next != null) {
			p = p.next;
			q = q.next.next;
		}

		p = p.next;

		ListNode halfHead = reverse(p);
		p = halfHead;
		q = head;

		boolean res = true;
		while (p != null) {
			if (p.val != q.val) {
				res = false;
				break;
			}

			p = p.next;
			q = q.next;
		}

		halfHead = reverse(halfHead);
		return res;
	}

	private ListNode reverse(ListNode head) {
		if (head == null || head.next == null)
			return head;

		ListNode p = head.next, q = null;
		head.next = null;

		while (p != null) {
			q = p;
			p = p.next;

			q.next = head;
			head = q;
		}
		return head;
	}

	public static void main(String[] args) {
		ListNode head = new ListNode(1);
		head.next = new ListNode(2);
		head.next.next = new ListNode(2);
		head.next.next.next = new ListNode(1);
		// head.next.next.next.next = new ListNode(1);

		System.out.println(new Solution().isPalindrome(head));
	}
}