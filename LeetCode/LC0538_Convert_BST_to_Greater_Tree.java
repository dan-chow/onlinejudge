import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Solution {
	public TreeNode convertBST(TreeNode root) {
		if (root == null)
			return root;

		Stack<TreeNode> stack = new Stack<>();

		int tmpSum = 0;
		TreeNode p = root;
		while (p != null) {
			stack.push(p);
			p = p.right;
		}

		while (!stack.isEmpty()) {
			p = stack.pop();

			tmpSum += p.val;
			p.val = tmpSum;

			if (p.left != null) {
				p = p.left;
				while (p != null) {
					stack.push(p);
					p = p.right;
				}
			}

		}

		return root;
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(5);
		root.left = new TreeNode(2);
		root.right = new TreeNode(13);

		// TreeNode root = new TreeNode(1);
		// root.left = new TreeNode(0);
		// root.left.left = new TreeNode(-2);
		// root.right = new TreeNode(4);
		// root.right.left = new TreeNode(3);

		new Solution().convertBST(root);
		Queue<TreeNode> queue = new LinkedList<>();
		queue.add(root);
		while (!queue.isEmpty()) {
			int levelSize = queue.size();
			while (levelSize-- > 0) {
				TreeNode node = queue.poll();
				System.out.print(node.val + " ");
				if (node.left != null)
					queue.add(node.left);
				if (node.right != null)
					queue.add(node.right);
			}
			System.out.println();
		}
	}
}