
public class Solution {

	public static class ListNode {
		int val;
		ListNode next;

		ListNode(int x) {
			val = x;
		}
	}

	public ListNode reverseList(ListNode head) {

		if (head == null)
			return head;

		ListNode newHead = head;
		head = head.next;
		newHead.next = null;

		while (head != null) {
			ListNode tmp = head;
			head = head.next;

			tmp.next = newHead;
			newHead = tmp;
		}
		return newHead;
	}

	public static void main(String[] args) {
		ListNode head = new ListNode(1);
		head.next = new ListNode(2);
		head.next.next = new ListNode(3);
		head.next.next.next = new ListNode(4);

		ListNode reversHead = new Solution().reverseList(head);
		ListNode p = reversHead;
		while (p != null) {
			System.out.println(p.val);
			p = p.next;
		}
	}
}