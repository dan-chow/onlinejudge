public class Solution {
	public int lastRemaining(int n) {
		int num = n;
		int first = 1;
		int step = 1;
		boolean leftToRight = true;
		while (num > 1) {
			if (leftToRight) {
				first += step;
				num /= 2;
				step *= 2;
				leftToRight = !leftToRight;
			} else {
				if (num % 2 == 0) {
					num /= 2;
					step *= 2;
				} else {
					first += step;
					num /= 2;
					step *= 2;
				}
				leftToRight = !leftToRight;
			}
		}
		return first;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().lastRemaining(9));
	}
}