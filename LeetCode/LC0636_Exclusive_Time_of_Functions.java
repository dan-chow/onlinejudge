import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

class Point implements Comparable<Point> {
	public final static int START = 0;
	public final static int END = 1;

	int id;
	int label;
	int startTime;
	int runTime;

	public Point(String log) {
		String[] tokens = log.split(":");
		this.id = Integer.parseInt(tokens[0]);
		this.label = tokens[1].equals("start") ? START : END;
		this.startTime = Integer.parseInt(tokens[2]);
		if (this.label == END)
			this.startTime++;
		this.runTime = 0;
	}

	@Override
	public int compareTo(Point other) {
		if (this.startTime != other.startTime)
			return this.startTime - other.startTime;

		return other.label - this.label;
	}
}

public class Solution {
	public int[] exclusiveTime(int n, List<String> logs) {
		int[] res = new int[n];
		List<Point> points = new ArrayList<>();
		for (String log : logs)
			points.add(new Point(log));
		Collections.sort(points);
		Stack<Point> stack = new Stack<>();
		for (Point p : points) {
			if (p.label == Point.START) {
				if (!stack.isEmpty())
					stack.peek().runTime += p.startTime - stack.peek().startTime;
				stack.push(p);
			} else {
				Point top = stack.pop();
				top.runTime += p.startTime - top.startTime;
				res[top.id] += top.runTime;

				if (!stack.isEmpty())
					stack.peek().startTime = p.startTime;
			}
		}
		return res;
	}

	public static void main(String[] args) {
		List<String> logs = new ArrayList<>();
		logs.add("0:start:0");
		logs.add("1:start:2");
		logs.add("1:end:5");
		logs.add("0:end:6");
		int[] res = new Solution().exclusiveTime(2, logs);
		for (int i : res)
			System.out.println(i);
	}
}