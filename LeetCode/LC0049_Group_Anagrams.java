import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Solution {

	public List<List<String>> groupAnagrams(String[] strs) {

		List<List<String>> res = new ArrayList<>();
		HashMap<String, List<String>> map = new HashMap<>();

		for (String s : strs) {
			char[] arr = s.toCharArray();
			Arrays.sort(arr);
			String newS = new String(arr);

			if (!map.containsKey(newS))
				map.put(newS, new ArrayList<String>());

			map.get(newS).add(s);
		}

		for (List<String> list : map.values())
			res.add(list);

		return res;
	}

	public static void main(String[] args) {
		String[] strs = { "eat", "tea", "tan", "ate", "nat", "bat" };
		List<List<String>> res = new Solution().groupAnagrams(strs);
		for (List<String> list : res)
			System.out.println(list);
	}
}