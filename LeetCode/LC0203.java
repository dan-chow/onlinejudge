
class ListNode {
	int val;
	ListNode next;

	ListNode(int x) {
		val = x;
	}
}

public class Solution {
	public ListNode removeElements(ListNode head, int val) {
		if (head == null)
			return head;

		ListNode p = head;
		ListNode q = p.next;
		while (q != null) {
			if (q.val == val) {
				p.next = q.next;
				q = p.next;
			} else {
				q = q.next;
				p = p.next;
			}
		}

		if (head.val == val)
			head = head.next;

		return head;
	}

	public static void main(String[] args) {
		ListNode head = new ListNode(1);
		head.next = new ListNode(2);
		head.next.next = new ListNode(6);
		head.next.next.next = new ListNode(3);
		head.next.next.next.next = new ListNode(4);
		head.next.next.next.next.next = new ListNode(5);
		head.next.next.next.next.next.next = new ListNode(6);

		head = new Solution().removeElements(head, 6);
		ListNode p = head;
		while (p != null) {
			System.out.println(p.val);
			p = p.next;
		}
	}
}