class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Solution {
	public int countNodes(TreeNode root) {
		int totalHight = height(root);
		if (totalHight < 0)
			return 0;

		int rightHeight = height(root.right);
		if (rightHeight == totalHight - 1) {
			return (1 << totalHight) + countNodes(root.right);
		} else {
			return (1 << (totalHight - 1)) + countNodes(root.left);
		}
	}

	private int height(TreeNode root) {
		TreeNode p = root;
		int cnt = -1;
		while (p != null) {
			p = p.left;
			cnt++;
		}
		return cnt;
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(3);
		root.left.left = new TreeNode(4);
		root.left.right = new TreeNode(5);
		root.right.left = new TreeNode(6);
		root.right.right = new TreeNode(7);
		root.left.left.left = new TreeNode(8);

		System.out.println(new Solution().countNodes(root));
	}
}