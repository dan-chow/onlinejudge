import java.util.HashMap;
import java.util.Map.Entry;

public class Solution {
	public int longestSubstring(String s, int k) {
		HashMap<Character, Integer> map = new HashMap<>();
		for (char c : s.toCharArray()) {
			if (map.containsKey(c)) {
				map.put(c, map.get(c) + 1);
			} else {
				map.put(c, 1);
			}
		}

		int max = Integer.MIN_VALUE;
		for (Entry<Character, Integer> entry : map.entrySet()) {
			if (entry.getValue() < k) {
				for (String t : s.split(entry.getKey() + "")) {
					max = Math.max(max, longestSubstring(t, k));
				}
				return max == Integer.MIN_VALUE ? 0 : max;
			}
		}
		return s.length();
	}

	public static void main(String[] args) {
		System.out.println(new Solution().longestSubstring("aaabb", 3));
		System.out.println(new Solution().longestSubstring("ababbc", 2));
	}
}