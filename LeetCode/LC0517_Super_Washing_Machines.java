public class Solution {
	public int findMinMoves(int[] machines) {
		if (machines == null || machines.length == 0)
			return 0;

		// nums = { 0, 0,11, 5}
		// avgs = { 4, 4, 4, 4}
		// diff = {-4,-4, 7, 1}

		int sum = 0;
		for (int m : machines)
			sum += m;

		if (sum % machines.length != 0)
			return -1;

		int avg = sum / machines.length;
		int cnt = 0;
		int max = 0;
		for (int m : machines) {
			cnt += m - avg;
			max = Math.max(max, Math.max(Math.abs(cnt), m - avg));
		}
		return max;
	}

	public static void main(String[] args) {
		int[] machines1 = { 1, 0, 5 };
		System.out.println(new Solution().findMinMoves(machines1));

		int[] machines2 = { 0, 3, 0 };
		System.out.println(new Solution().findMinMoves(machines2));

		int[] machines3 = { 0, 2, 0 };
		System.out.println(new Solution().findMinMoves(machines3));
	}
}