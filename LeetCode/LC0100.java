public class Solution {
	public static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode(int x) {
			val = x;
		}
	}

	public boolean isSameTree(TreeNode p, TreeNode q) {
		if (p == null || q == null) {
			if (p == null && q == null)
				return true;
			else
				return false;
		}

		if (p.val != q.val)
			return false;

		return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
	}

	public static void main(String[] args) {
		TreeNode tree1 = new TreeNode(3);
		tree1.left = new TreeNode(5);
		tree1.left.left = new TreeNode(6);
		tree1.left.right = new TreeNode(2);
		tree1.left.right.left = new TreeNode(7);
		tree1.left.right.right = new TreeNode(4);
		tree1.right = new TreeNode(1);
		tree1.right.left = new TreeNode(0);
		tree1.right.right = new TreeNode(8);

		TreeNode tree2 = new TreeNode(3);
		tree2.left = new TreeNode(5);
		tree2.left.left = new TreeNode(6);
		tree2.left.right = new TreeNode(2);
		tree2.left.right.left = new TreeNode(7);
		tree2.left.right.right = new TreeNode(4);
		tree2.right = new TreeNode(1);
		tree2.right.left = new TreeNode(0);
		tree2.right.right = new TreeNode(6);

		System.out.println(new Solution().isSameTree(tree1, tree2));
	}
}