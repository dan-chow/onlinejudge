public class Solution {
	public boolean checkInclusion(String s1, String s2) {
		if (s1 == null)
			return true;

		if (s2 == null || s2.length() < s1.length())
			return false;

		int[] dict = new int[26];
		for (char c : s1.toCharArray())
			dict[c - 'a']++;

		int cnt = s1.length();
		int left = 0;
		int right = 0;

		while (right < s2.length()) {
			if (dict[s2.charAt(right) - 'a'] > 0)
				cnt--;

			dict[s2.charAt(right) - 'a']--;
			right++;

			if (cnt == 0)
				return true;

			if (right - left == s1.length()) {
				if (dict[s2.charAt(left) - 'a'] >= 0)
					cnt++;

				dict[s2.charAt(left) - 'a']++;
				left++;
			}
		}
		return false;

	}

	public static void main(String[] args) {
		System.out.println(new Solution().checkInclusion("ab", "eidbaooo"));
		System.out.println(new Solution().checkInclusion("ab", "eidboaoo"));
	}
}
