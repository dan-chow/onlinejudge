import java.util.ArrayList;
import java.util.List;

public class Solution {
	public List<Integer> getRow(int rowIndex) {
		List<Integer> list = new ArrayList<>();

		list.add(1);
		for (int i = 0; i < rowIndex; i++) {
			List<Integer> tmp = new ArrayList<>();
			tmp.add(1);
			for (int j = 1; j < list.size(); j++) {
				tmp.add(list.get(j) + list.get(j - 1));
			}
			tmp.add(1);
			list = tmp;
		}
		return list;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().getRow(0));
		System.out.println(new Solution().getRow(3));
	}
}