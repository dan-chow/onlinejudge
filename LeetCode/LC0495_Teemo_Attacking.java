import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Point implements Comparable<Point> {
	public final static int LEFT = 0;
	public final static int RIGHT = 1;

	int time;
	int label;

	public Point(int time, int label) {
		this.time = time;
		this.label = label;
	}

	@Override
	public int compareTo(Point other) {
		if (this.time != other.time)
			return this.time - other.time;

		return this.label - other.label;
	}
}

public class Solution {
	public int findPoisonedDuration(int[] timeSeries, int duration) {
		List<Point> points = new ArrayList<>();
		for (int i = 0; i < timeSeries.length; i++) {
			points.add(new Point(timeSeries[i] - 1, Point.LEFT));
			points.add(new Point(timeSeries[i] - 1 + duration, Point.RIGHT));
		}
		Collections.sort(points);

		int cnt = 0;
		int start = 0;
		int totalTime = 0;
		for (int i = 0; i < points.size(); i++) {
			if (cnt == 0)
				start = points.get(i).time;

			if (points.get(i).label == Point.LEFT)
				cnt++;
			else
				cnt--;

			if (cnt == 0)
				totalTime += points.get(i).time - start;
		}
		return totalTime;
	}

	public static void main(String[] args) {
		int[] ts1 = { 1, 4 };
		System.out.println(new Solution().findPoisonedDuration(ts1, 2));

		int[] ts2 = { 1, 2 };
		System.out.println(new Solution().findPoisonedDuration(ts2, 2));
	}
}