
class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Solution {
	public boolean isSymmetric(TreeNode root) {
		if (root == null)
			return true;
		return symmetricEqual(root.left, root.right);

	}

	private boolean symmetricEqual(TreeNode left, TreeNode right) {
		if (left == null && right == null)
			return true;
		if (left == null || right == null)
			return false;

		if (left.val != right.val)
			return false;

		return symmetricEqual(left.right, right.left) && symmetricEqual(left.left, right.right);
	}

	public static void main(String[] args) {
		TreeNode root1 = new TreeNode(1);
		root1.left = new TreeNode(2);
		root1.left.left = new TreeNode(3);
		root1.left.right = new TreeNode(4);
		root1.right = new TreeNode(2);
		root1.right.left = new TreeNode(4);
		root1.right.right = new TreeNode(3);

		System.out.println(new Solution().isSymmetric(root1));

		TreeNode root2 = new TreeNode(1);
		root2.left = new TreeNode(2);
		root2.left.right = new TreeNode(3);
		root2.right = new TreeNode(2);
		root2.right.right = new TreeNode(3);

		System.out.println(new Solution().isSymmetric(root2));
	}
}