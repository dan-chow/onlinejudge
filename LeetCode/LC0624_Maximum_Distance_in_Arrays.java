public class Solution {
	public int maxDistance(int[][] arrays) {

		if (arrays.length == 1)
			return -1;

		int result = Integer.MIN_VALUE;
		int min = arrays[0][0];
		int max = arrays[0][arrays[0].length - 1];

		for (int i = 1; i < arrays.length; i++) {
			if (arrays[i][arrays[i].length - 1] - min > result) {
				result = arrays[i][arrays[i].length - 1] - min;
			}
			if (max - arrays[i][0] > result) {
				result = max - arrays[i][0];
			}

			max = Math.max(max, arrays[i][arrays[i].length - 1]);
			min = Math.min(min, arrays[i][0]);
		}

		return result;
	}

	public static void main(String[] args) {
		int[][] arrays = { { 1, 2, 3 }, { 4, 5 }, { 1, 2, 3 } };
		System.out.println(new Solution().maxDistance(arrays));
	}
}