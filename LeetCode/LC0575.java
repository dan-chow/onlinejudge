import java.util.HashSet;

public class Solution {
	public int distributeCandies(int[] candies) {
		HashSet<Integer> set = new HashSet<>();
		for (int candy : candies)
			set.add(candy);

		if (set.size() >= candies.length / 2)
			return candies.length / 2;

		return set.size();
	}

	public static void main(String[] args) {
		int[] candies1 = { 1, 1, 2, 2, 3, 3 };
		System.out.println(new Solution().distributeCandies(candies1));
		int[] candies2 = { 1, 1, 2, 3 };
		System.out.println(new Solution().distributeCandies(candies2));
	}
}