import java.util.ArrayList;
import java.util.List;

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Solution {
	public List<List<Integer>> pathSum(TreeNode root, int sum) {
		List<List<Integer>> res = new ArrayList<>();
		if (root == null)
			return res;

		List<Integer> path = new ArrayList<>();
		path.add(root.val);
		findPath(root, sum, path, res);
		return res;
	}

	private void findPath(TreeNode root, int sum, List<Integer> tmpPath, List<List<Integer>> res) {
		if (root == null)
			return;

		if (root.val == sum && root.left == null && root.right == null)
			res.add(new ArrayList<>(tmpPath));

		if (root.left != null) {
			tmpPath.add(root.left.val);
			findPath(root.left, sum - root.val, tmpPath, res);
			tmpPath.remove(tmpPath.size() - 1);
		}
		if (root.right != null) {
			tmpPath.add(root.right.val);
			findPath(root.right, sum - root.val, tmpPath, res);
			tmpPath.remove(tmpPath.size() - 1);
		}
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(5);
		root.left = new TreeNode(4);
		root.left.left = new TreeNode(11);
		root.left.left.left = new TreeNode(7);
		root.left.left.right = new TreeNode(2);
		root.right = new TreeNode(8);
		root.right.left = new TreeNode(13);
		root.right.right = new TreeNode(4);
		root.right.right.left = new TreeNode(5);
		root.right.right.right = new TreeNode(1);

		List<List<Integer>> res = new Solution().pathSum(root, 22);
		for (List<Integer> list : res)
			System.out.println(list);
	}
}