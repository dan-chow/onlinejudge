
class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Solution {
	public String tree2str(TreeNode t) {
		if (t == null)
			return "";

		StringBuffer sb = new StringBuffer();
		sb.append(t.val);
		if (t.right != null) {
			sb.append("(").append(tree2str(t.left)).append(")");
			sb.append("(").append(tree2str(t.right)).append(")");
		} else if (t.left != null) {
			sb.append("(").append(tree2str(t.left)).append(")");
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		TreeNode root1 = new TreeNode(1);
		root1.left = new TreeNode(2);
		root1.left.left = new TreeNode(4);
		root1.right = new TreeNode(3);
		System.out.println(new Solution().tree2str(root1));

		TreeNode root2 = new TreeNode(1);
		root2.left = new TreeNode(2);
		root2.left.right = new TreeNode(4);
		root2.right = new TreeNode(3);
		System.out.println(new Solution().tree2str(root2));
	}
}