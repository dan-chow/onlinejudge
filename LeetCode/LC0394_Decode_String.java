import java.util.Stack;

public class Solution {
	public String decodeString(String s) {
		Stack<Integer> cntStack = new Stack<>();
		Stack<String> strStack = new Stack<>();

		StringBuffer tmp = new StringBuffer();

		for (int i = 0; i < s.length(); i++) {

			if (s.charAt(i) >= '0' && s.charAt(i) <= '9') {
				int num = 0;
				while (s.charAt(i) >= '0' && s.charAt(i) <= '9') {
					num = num * 10 + s.charAt(i) - '0';
					i++;
				}
				cntStack.push(num);
				i--;
			} else if (s.charAt(i) == '[') {
				strStack.push(tmp.toString());
				tmp.setLength(0);
			} else if (s.charAt(i) == ']') {
				StringBuffer newStr = new StringBuffer(strStack.pop());
				int count = cntStack.pop();
				for (int t = 0; t < count; t++) {
					newStr.append(tmp.toString());
				}
				tmp = newStr;
			} else {
				tmp.append(s.charAt(i));
			}
		}

		return tmp.toString();

	}

	public static void main(String[] args) {
		System.out.println(new Solution().decodeString("3[a]2[bc]"));
		System.out.println(new Solution().decodeString("3[a2[c]]"));
		System.out.println(new Solution().decodeString("2[abc]3[cd]ef"));
	}
}