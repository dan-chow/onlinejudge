import java.util.ArrayList;
import java.util.List;

public class Solution {
	public List<Integer> findDisappearedNumbers(int[] nums) {
		boolean[] appear = new boolean[nums.length];
		for (int n : nums)
			appear[n - 1] = true;
		List<Integer> list = new ArrayList<>();
		for (int i = 0; i < appear.length; i++)
			if (!appear[i])
				list.add(i + 1);
		return list;
	}

	public static void main(String[] args) {
		int[] nums = { 4, 3, 2, 7, 8, 2, 3, 1 };
		List<Integer> res = new Solution().findDisappearedNumbers(nums);
		for (int i : res)
			System.out.println(i);
	}
}