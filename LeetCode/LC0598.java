public class Solution {
	public int maxCount(int m, int n, int[][] ops) {
		if (ops == null || ops.length == 0)
			return m * n;
		int minX = Integer.MAX_VALUE;
		int minY = Integer.MAX_VALUE;
		for (int[] op : ops) {
			if (op[0] < minX)
				minX = op[0];
			if (op[1] < minY)
				minY = op[1];
		}
		return minX * minY;
	}

	public static void main(String[] args) {
		int[][] ops = { { 2, 2 }, { 3, 3 } };
		System.out.println(new Solution().maxCount(3, 3, ops));

	}
}