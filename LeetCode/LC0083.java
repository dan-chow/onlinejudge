
class ListNode {
	int val;
	ListNode next;

	ListNode(int x) {
		val = x;
	}
}

public class Solution {
	public ListNode deleteDuplicates(ListNode head) {
		ListNode p = head;
		while (p != null && p.next != null) {
			if (p.val == p.next.val) {
				p.next = p.next.next;
			} else {
				p = p.next;
			}
		}
		return head;
	}

	public static void main(String[] args) {
		ListNode head1 = new ListNode(1);
		head1.next = new ListNode(1);
		head1.next.next = new ListNode(2);

		ListNode res1 = new Solution().deleteDuplicates(head1);
		ListNode p = res1;
		while (p != null) {
			System.out.println(p.val);
			p = p.next;
		}

		ListNode head2 = new ListNode(1);
		head2.next = new ListNode(1);
		head2.next.next = new ListNode(2);
		head2.next.next.next = new ListNode(3);
		head2.next.next.next.next = new ListNode(3);

		ListNode res2 = new Solution().deleteDuplicates(head2);
		ListNode q = res2;
		while (q != null) {
			System.out.println(q.val);
			q = q.next;
		}
	}
}