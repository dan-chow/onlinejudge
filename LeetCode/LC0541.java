public class Solution {
	public String reverseStr(String s, int k) {
		char[] arr = s.toCharArray();
		for (int i = 0; i < arr.length; i += 2 * k) {
			if (i + k - 1 < arr.length) {
				reverse(arr, i, i + k - 1);
			} else {
				reverse(arr, i, arr.length - 1);
			}
		}
		return new String(arr);
	}

	private void reverse(char[] arr, int beginIdx, int endIdx) {
		for (int i = 0; i <= (endIdx - beginIdx) / 2; i++) {
			char tmp = arr[beginIdx + i];
			arr[beginIdx + i] = arr[endIdx - i];
			arr[endIdx - i] = tmp;
		}
	}

	public static void main(String[] args) {
		System.out.println(new Solution().reverseStr("abcdefg", 2));
	}
}