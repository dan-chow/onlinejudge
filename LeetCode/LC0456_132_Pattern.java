public class Solution {
	public boolean find132pattern(int[] nums) {
		if (nums == null || nums.length < 3)
			return false;

		int min = Integer.MAX_VALUE;
		for (int j = 0; j < nums.length - 1; j++) {
			for (int k = j + 1; k < nums.length; k++) {
				if (nums[k] > min && nums[k] < nums[j])
					return true;

			}
			min = Math.min(min, nums[j]);
		}

		return false;
	}

	public static void main(String[] args) {
		int[] nums1 = { 1, 2, 3, 4 };
		System.out.println(new Solution().find132pattern(nums1));
		int[] nums2 = { 3, 1, 4, 2 };
		System.out.println(new Solution().find132pattern(nums2));
		int[] nums3 = { -1, 3, 2, 0 };
		System.out.println(new Solution().find132pattern(nums3));

	}
}