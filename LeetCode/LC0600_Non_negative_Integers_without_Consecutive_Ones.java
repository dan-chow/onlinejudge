public class Solution {
	public int findIntegers(int num) {
		StringBuffer sb = new StringBuffer(Integer.toBinaryString(num)).reverse();

		int[] endsWithZero = new int[sb.length()];
		int[] endsWithOne = new int[sb.length()];
		endsWithZero[0] = 1;
		endsWithOne[0] = 1;
		for (int i = 1; i < sb.length(); i++) {
			endsWithZero[i] = endsWithZero[i - 1] + endsWithOne[i - 1];
			endsWithOne[i] = endsWithZero[i - 1];
		}

		int result = endsWithZero[sb.length() - 1] + endsWithOne[sb.length() - 1];
		for (int i = sb.length() - 2; i >= 0; i--) {
			if (sb.charAt(i) == '1' && sb.charAt(i + 1) == '1')
				break;
			if (sb.charAt(i) == '0' && sb.charAt(i + 1) == '0')
				result -= endsWithOne[i];
		}
		return result;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().findIntegers(5));
	}
}