import java.util.HashSet;

public class Solution {
	public int findMaximumXOR(int[] nums) {
		int mask = 0;
		HashSet<Integer> set = new HashSet<>();

		int max = 0;
		for (int i = 31; i >= 0; i--) {
			set.clear();

			mask |= (1 << i);
			for (int num : nums)
				set.add(num & mask);

			int tmp = max | (1 << i);
			for (int num : set) {
				if (set.contains(num ^ tmp)) {
					max = tmp;
					break;
				}
			}
		}
		return max;
	}

	public static void main(String[] args) {
		int[] nums = { 3, 10, 5, 25, 2, 8 };
		System.out.println(new Solution().findMaximumXOR(nums));
	}
}