public class Solution {

	public static class ListNode {
		int val;
		ListNode next;

		ListNode(int x) {
			val = x;
		}
	}

	public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		if (l1 == null)
			return l2;
		if (l2 == null)
			return l2;

		ListNode p1 = l1;
		ListNode p2 = l2;
		int tmpSum = p1.val + p2.val;
		ListNode res = new ListNode(tmpSum % 10);
		ListNode q = res;
		int carray = tmpSum / 10;
		p1 = p1.next;
		p2 = p2.next;

		while (p1 != null && p2 != null) {
			tmpSum = p1.val + p2.val + carray;
			q.next = new ListNode(tmpSum % 10);
			q = q.next;
			carray = tmpSum / 10;

			p1 = p1.next;
			p2 = p2.next;
		}
		if (p1 == null)
			p1 = p2;

		while (p1 != null) {
			tmpSum = p1.val + carray;
			q.next = new ListNode(tmpSum % 10);
			q = q.next;
			carray = tmpSum / 10;

			p1 = p1.next;
		}

		if (carray > 0)
			q.next = new ListNode(1);

		return res;
	}

	public static void main(String[] args) {
		ListNode l1 = new ListNode(2);
		l1.next = new ListNode(4);
		l1.next.next = new ListNode(3);

		ListNode l2 = new ListNode(5);
		l2.next = new ListNode(6);
		l2.next.next = new ListNode(4);

		ListNode res = new Solution().addTwoNumbers(l1, l2);
		ListNode p = res;
		while (p != null) {
			System.out.println(p.val);
			p = p.next;
		}

	}
}