public class Solution {
	public int divide(int dividend, int divisor) {
		if (divisor == 0)
			return 0;

		long res = dividePositive(dividend, divisor);
		res = Math.min(Integer.MAX_VALUE, res);
		res = Math.max(Integer.MIN_VALUE, res);
		return (int) res;
	}

	private long dividePositive(long dividend, long divisor) {
		boolean sign = true;

		if (dividend < 0) {
			sign = !sign;
			dividend = -dividend;
		}

		if (divisor < 0) {
			sign = !sign;
			divisor = -divisor;
		}

		if (dividend < divisor)
			return 0;

		long sum = divisor;
		long mul = 1;
		while (sum + sum <= dividend) {
			sum += sum;
			mul += mul;
		}

		long res = mul + dividePositive(dividend - sum, divisor);
		return sign ? res : -res;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().divide(4, 2));
		System.out.println(new Solution().divide(Integer.MIN_VALUE, -1));
		System.out.println(new Solution().divide(Integer.MAX_VALUE, 1));
	}
}