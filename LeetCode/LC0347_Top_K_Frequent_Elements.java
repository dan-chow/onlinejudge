import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class Solution {
	public List<Integer> topKFrequent(int[] nums, int k) {
		List<List<Integer>> buckets = new ArrayList<>();
		for (int i = 0; i < nums.length + 1; i++)
			buckets.add(new ArrayList<Integer>());

		HashMap<Integer, Integer> freqMap = new HashMap<>();
		for (int n : nums)
			freqMap.put(n, freqMap.getOrDefault(n, 0) + 1);

		for (Entry<Integer, Integer> entry : freqMap.entrySet())
			buckets.get(entry.getValue()).add(entry.getKey());

		List<Integer> res = new ArrayList<>();
		for (int i = nums.length; i >= 0; i--) {
			res.addAll(buckets.get(i));
			k -= buckets.get(i).size();
			if (k == 0)
				break;
		}
		return res;
	}

	public static void main(String[] args) {
		int[] nums = { 1, 1, 1, 2, 2, 3 };
		System.out.println(new Solution().topKFrequent(nums, 2));
	}
}