public class Solution {
	public final static String[] ROMAN_NUM = { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };
	public final static int[] INT_NUM = { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };

	public String intToRoman(int num) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < INT_NUM.length; i++) {
			if (num == 0)
				break;

			while (num >= INT_NUM[i]) {
				sb.append(ROMAN_NUM[i]);
				num -= INT_NUM[i];
			}
		}

		return sb.toString();
	}

	public static void main(String[] args) {
		System.out.println(new Solution().intToRoman(3999));
	}
}