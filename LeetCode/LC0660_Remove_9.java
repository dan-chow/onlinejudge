class Solution {
	public int newInteger(int n) {
		StringBuffer sb = new StringBuffer();
		while (n > 0) {
			sb.append(n % 9);
			n /= 9;
		}
		return Integer.parseInt(sb.reverse().toString());
	}
}