public class Solution {
	public int reverse(int x) {
		if (x == Integer.MIN_VALUE)
			return 0;

		if (x < 0)
			return -reverse(-x);

		int result = 0;
		while (x > 0) {
			if (result > Integer.MAX_VALUE / 10)
				return 0;

			if (result == Integer.MAX_VALUE / 10 && (x % 10) > 7)
				return 0;

			result = result * 10 + x % 10;
			x /= 10;
		}
		return result;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().reverse(1534236469));
		System.out.println(new Solution().reverse(-123));
	}
}