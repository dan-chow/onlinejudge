import java.util.HashMap;
import java.util.HashSet;

class MagicDictionary {
	HashMap<Integer, HashSet<String>> map;

	/** Initialize your data structure here. */
	public MagicDictionary() {
		map = new HashMap<>();
	}

	/** Build a dictionary through a list of words */
	public void buildDict(String[] dict) {
		for (String str : dict) {
			if (!map.containsKey(str.length()))
				map.put(str.length(), new HashSet<String>());

			map.get(str.length()).add(str);
		}
	}

	/**
	 * Returns if there is any word in the trie that equals to the given word
	 * after modifying exactly one character
	 */
	public boolean search(String word) {
		if (!map.containsKey(word.length()))
			return false;

		HashSet<String> set = map.get(word.length());
		char[] arr = word.toCharArray();
		for (int i = 0; i < word.length(); i++) {
			for (char c = 'a'; c <= 'z'; c++) {
				if (c == word.charAt(i))
					continue;

				arr[i] = c;

				if (set.contains(new String(arr)))
					return true;

				arr[i] = word.charAt(i);
			}
		}
		return false;
	}

	public static void main(String[] args) {
		String[] dict = { "hello", "leetcode" };
		MagicDictionary obj = new MagicDictionary();
		obj.buildDict(dict);
		System.out.println(obj.search("hello"));
		System.out.println(obj.search("hhllo"));
		System.out.println(obj.search("hell"));
		System.out.println(obj.search("leetcoded"));
	}
}
