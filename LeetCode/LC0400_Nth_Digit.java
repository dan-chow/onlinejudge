public class Solution {
	public int findNthDigit(int n) {
		long digits = 1;
		long count = 9;
		long start = 1;
		while (n > digits * count) {
			n -= digits * count;
			System.out.println(digits * count);
			digits++;
			count *= 10;
			start *= 10;
		}

		start += (n - 1) / digits;
		return String.valueOf(start).charAt((int) ((n - 1) % digits)) - '0';
	}

	public static void main(String[] args) {
		System.out.println(new Solution().findNthDigit(3));
		System.out.println(new Solution().findNthDigit(11));
		System.out.println(new Solution().findNthDigit(2147483647));

	}
}