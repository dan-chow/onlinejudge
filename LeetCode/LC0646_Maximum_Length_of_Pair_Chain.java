import java.util.Arrays;
import java.util.Comparator;

public class Solution {
	public int findLongestChain(int[][] pairs) {
		Arrays.sort(pairs, new Comparator<int[]>() {

			@Override
			public int compare(int[] p1, int[] p2) {
				if (p1[0] != p2[0])
					return p1[0] - p2[0];
				return p1[1] - p2[1];
			}
		});

		int[] dp = new int[pairs.length];
		dp[0] = 1;

		for (int j = 1; j < pairs.length; j++) {
			int max = 1;
			for (int i = 0; i < j; i++) {
				if (pairs[i][1] < pairs[j][0])
					max = Math.max(max, dp[i] + 1);
			}
			dp[j] = max;
		}
		return dp[pairs.length - 1];
	}

	public static void main(String[] args) {
		int[][] pairs = { { 3, 4 }, { 1, 2 }, { 2, 3 } };
		System.out.println(new Solution().findLongestChain(pairs));
	}
}