import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

class Item implements Comparable<Item> {
	int ch;
	int cnt;

	public Item(int ch, int cnt) {
		this.ch = ch;
		this.cnt = cnt;
	}

	@Override
	public int compareTo(Item other) {
		return other.cnt - this.cnt;
	}
}

public class Solution {
	public String frequencySort(String s) {
		HashMap<Character, Integer> map = new HashMap<>();
		for (int i = 0; i < s.length(); i++)
			map.put(s.charAt(i), map.getOrDefault(s.charAt(i), 0) + 1);

		List<Item> items = new ArrayList<>();
		for (Entry<Character, Integer> entry : map.entrySet())
			items.add(new Item(entry.getKey(), entry.getValue()));

		Collections.sort(items);

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < items.size(); i++) {
			for (int j = 0; j < items.get(i).cnt; j++)
				sb.append((char) items.get(i).ch);
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		System.out.println(new Solution().frequencySort("tree"));
		System.out.println(new Solution().frequencySort("cccaaa"));
		System.out.println(new Solution().frequencySort("Aabb"));
	}
}