import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution {
	private String[] keyboardRows;

	public Solution() {
		keyboardRows = new String[3];
		keyboardRows[0] = "qwertyuiop";
		keyboardRows[1] = "asdfghjkl";
		keyboardRows[2] = "zxcvbnm";
	}

	public String[] findWords(String[] words) {
		List<String> res = new ArrayList<>(Arrays.asList(words));

		for (String w : words) {
			String tmp = w.toLowerCase();
			if (keyboardRows[0].indexOf(tmp.charAt(0)) >= 0) {
				for (char c : tmp.toCharArray()) {
					if (keyboardRows[0].indexOf(c) < 0) {
						res.remove(w);
						break;
					}
				}

			} else if (keyboardRows[1].indexOf(tmp.charAt(0)) >= 0) {
				for (char c : tmp.toCharArray()) {
					if (keyboardRows[1].indexOf(c) < 0) {
						res.remove(w);
						break;
					}
				}
			} else if (keyboardRows[2].indexOf(tmp.charAt(0)) >= 0) {
				for (char c : tmp.toCharArray()) {
					if (keyboardRows[2].indexOf(c) < 0) {
						res.remove(w);
						break;
					}
				}
			}
		}
		return res.toArray(new String[0]);

	}

	public static void main(String[] args) {
		String[] words = { "Hello", "Alaska", "Dad", "Peace" };
		String[] res = new Solution().findWords(words);
		for (String s : res)
			System.out.println(s);
	}
}