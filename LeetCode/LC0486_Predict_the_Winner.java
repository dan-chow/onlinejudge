public class Solution {
	public boolean PredictTheWinner(int[] nums) {
		int n = nums.length;
		int[][] dp = new int[nums.length][nums.length];
		for (int i = 0; i < nums.length; i++)
			dp[i][i] = nums[i];

		for (int len = 1; len < nums.length; len++) {
			for (int i = 0; i < n - len; i++) {
				int j = i + len;
				dp[i][j] = Math.max(nums[i] - dp[i + 1][j], nums[j] - dp[i][j - 1]);
			}
		}
		return dp[0][n - 1] >= 0;
	}

	public static void main(String[] args) {
		int[] nums1 = { 1, 5, 2 };
		System.out.println(new Solution().PredictTheWinner(nums1));

		int[] nums2 = { 1, 5, 233, 7 };
		System.out.println(new Solution().PredictTheWinner(nums2));
	}
}