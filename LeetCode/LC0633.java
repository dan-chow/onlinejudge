public class Solution {
	public boolean judgeSquareSum(int c) {

		for (int a = 0; a <= Math.sqrt(c); a++) {
			int aa = a * a;
			int b = (int) Math.sqrt(c - aa);

			if (aa + b * b == c)
				return true;
		}
		return false;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().judgeSquareSum(5));
		System.out.println(new Solution().judgeSquareSum(3));
	}
}