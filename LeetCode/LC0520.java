public class Solution {
	public boolean detectCapitalUse(String word) {
		int capCnt = 0;
		int i = 0;
		while (i < word.length() && (word.charAt(i) >= 'A' && word.charAt(i) <= 'Z')) {
			capCnt++;
			i++;
		}

		if (capCnt > 1 && i != word.length())
			return false;

		for (; i < word.length(); i++)
			if (word.charAt(i) > 'z' || word.charAt(i) < 'a')
				return false;
		return true;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().detectCapitalUse("USA"));
		System.out.println(new Solution().detectCapitalUse("FlaG"));
	}
}