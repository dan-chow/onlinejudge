public class Solution {
	public static final int RED = 0;
	public static final int WHITE = 1;
	public static final int BLUE = 2;

	public void sortColors(int[] nums) {
		int left = 0;
		int right = nums.length - 1;
		for (int i = 0; i < nums.length; i++) {
			while (nums[i] == BLUE && i < right)
				swap(nums, i, right--);
			while (nums[i] == RED && i > left)
				swap(nums, i, left++);

		}
	}

	private void swap(int[] nums, int a, int b) {
		int tmp = nums[a];
		nums[a] = nums[b];
		nums[b] = tmp;
	}

	public static void main(String[] args) {
		int[] nums = { 2, 2, 1, 1, 0, 2, 1, 2, 0, 0, 1 };
		new Solution().sortColors(nums);
		for (int i = 0; i < nums.length; i++)
			System.out.println(nums[i]);
	}
}