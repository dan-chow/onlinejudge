import java.util.HashSet;

public class Solution {
	public int longestPalindrome(String s) {
		if (s.length() == 0)
			return 0;

		HashSet<Character> set = new HashSet<>();
		int cnt = 0;
		for (int i = 0; i < s.length(); i++) {
			if (!set.add(s.charAt(i))) {
				set.remove(s.charAt(i));
				cnt++;
			}
		}
		return set.isEmpty() ? 2 * cnt : 2 * cnt + 1;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().longestPalindrome("abccccdd"));
	}
}