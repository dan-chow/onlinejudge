public class Solution {
	public boolean isPalindrome(int x) {
		if (x < 0)
			return false;

		int order = 1;
		for (; x / order >= 10; order *= 10)
			;

		while (x > 0) {
			int left = x / order;
			int right = x % 10;
			if (left != right)
				return false;

			x = (x % order) / 10;
			order /= 100;
		}
		return true;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().isPalindrome(1001));
	}
}