public class Solution {
	private static final int x = 0;
	private static final int y = 1;

	public boolean validSquare(int[] p1, int[] p2, int[] p3, int[] p4) {
		if (distanceSquare(p1, p2) == 0 || distanceSquare(p1, p3) == 0 || distanceSquare(p1, p4) == 0)
			return false;

		if (distanceSquare(p1, p2) == distanceSquare(p1, p3))
			return validDiagLines(p1, p4, p2, p3);
		else if (distanceSquare(p1, p2) == distanceSquare(p1, p4))
			return validDiagLines(p1, p3, p2, p4);
		else
			return validDiagLines(p1, p2, p3, p4);
	}

	private int distanceSquare(int[] p1, int[] p2) {
		return (p1[0] - p2[0]) * (p1[0] - p2[0]) + ((p1[1] - p2[1])) * ((p1[1] - p2[1]));
	}

	private boolean validDiagLines(int[] a1, int[] a2, int[] b1, int[] b2) {
		if (a1[x] == a2[x]) {
			if (b1[y] != b2[y])
				return false;
			return equals(midY(a1, a2), b1[y]) && equals(a1[x], midX(b1, b2));
		}

		if (b1[x] == b2[x]) {
			return validDiagLines(b1, b2, a1, a2);
		}

		if (distanceSquare(a1, a2) != distanceSquare(b1, b2))
			return false;

		double kaa = 1.0 * (a2[y] - a1[y]) / (a2[x] - a1[x]);
		double kbb = 1.0 * (b2[y] - b1[y]) / (b2[x] - b1[x]);
		if (!equals(kaa * kbb, -1))
			return false;

		return equals(midX(a1, a2), midX(b1, b2)) && equals(midY(a1, a2), midY(b1, b2));
	}

	private boolean equals(double a, double b) {
		return Math.abs(a - b) < 1e-6;
	}

	private double midX(int[] p1, int[] p2) {
		return 0.5 * (p2[x] + p1[x]);
	}

	private double midY(int[] p1, int[] p2) {
		return 0.5 * (p2[y] + p1[y]);
	}

	public static void main(String[] args) {
		int[] p1 = { 0, 0 };
		int[] p2 = { 1, 1 };
		int[] p3 = { 1, 0 };
		int[] p4 = { 0, 1 };

		System.out.println(new Solution().validSquare(p1, p2, p3, p4));
	}
}