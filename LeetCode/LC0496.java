import java.util.Hashtable;
import java.util.Stack;

public class Solution {
	public int[] nextGreaterElement(int[] findNums, int[] nums) {
		if (findNums.length == 0)
			return new int[0];

		Stack<Integer> stack = new Stack<>();
		Hashtable<Integer, Integer> table = new Hashtable<>();
		for (int i : nums) {
			if (stack.isEmpty() || i < stack.peek()) {
				stack.push(i);
			} else {
				while (!stack.empty() && stack.peek() < i) {
					table.put(stack.pop(), i);
				}
				stack.push(i);
			}
		}
		int[] res = new int[findNums.length];
		for (int i = 0; i < findNums.length; i++) {
			if (table.containsKey(findNums[i])) {
				res[i] = table.get(findNums[i]);
			} else {
				res[i] = -1;
			}
		}
		return res;
	}

	public static void main(String[] args) {
		int[] nums11 = { 4, 1, 2 };
		int[] nums12 = { 1, 3, 4, 2 };
		int[] res1 = new Solution().nextGreaterElement(nums11, nums12);
		for (int i : res1)
			System.out.println(i);

		int[] nums21 = { 2, 4 };
		int[] nums22 = { 1, 2, 3, 4 };
		int[] res2 = new Solution().nextGreaterElement(nums21, nums22);
		for (int i : res2)
			System.out.println(i);

	}
}