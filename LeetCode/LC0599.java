import java.util.HashSet;

public class Solution {
	public String[] findRestaurant(String[] list1, String[] list2) {
		if (list1 == null || list1.length == 0 || list2 == null || list2.length == 0)
			return new String[0];

		int minIdxSum = Integer.MAX_VALUE;
		HashSet<String> set = new HashSet<>();

		for (int i = 0; i < list1.length; i++) {
			for (int j = 0; j < list2.length; j++) {
				if (list1[i].equals(list2[j])) {
					if ((i + j) == minIdxSum) {
						set.add(list1[i]);
					}
					if ((i + j) < minIdxSum) {
						minIdxSum = i + j;
						set.clear();
						set.add(list1[i]);
					}
				}
			}
		}

		String[] result = new String[set.size()];

		int i = 0;
		for (String s : set) {
			result[i++] = s;
		}

		return result;
	}

	public static void main(String[] args) {
		String[] list11 = { "Shogun", "Tapioca Express", "Burger King", "KFC" };
		String[] list12 = { "Piatti", "The Grill at Torrey Pines", "Hungry Hunter Steakhouse", "Shogun" };
		String[] result1 = new Solution().findRestaurant(list11, list12);
		for (String s : result1)
			System.out.println(s);

		String[] list21 = { "Shogun", "Tapioca Express", "Burger King", "KFC" };
		String[] list22 = { "KFC", "Shogun", "Burger King" };
		String[] result2 = new Solution().findRestaurant(list21, list22);
		for (String s : result2)
			System.out.println(s);
	}
}