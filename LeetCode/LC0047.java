import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution {
	public List<List<Integer>> permuteUnique(int[] nums) {
		Arrays.sort(nums);
		boolean[] visited = new boolean[nums.length];
		List<List<Integer>> res = new ArrayList<>();
		List<Integer> tmpList = new ArrayList<>();
		subPermuteUnique(nums, visited, tmpList, res);
		return res;
	}

	private void subPermuteUnique(int[] nums, boolean[] visited, List<Integer> tmpList, List<List<Integer>> res) {
		if (tmpList.size() == nums.length) {
			res.add(new ArrayList<>(tmpList));
			return;
		}

		for (int i = 0; i < nums.length; i++) {
			if (!visited[i]) {
				if (i > 0 && nums[i] == nums[i - 1] && !visited[i - 1])
					continue;

				visited[i] = true;
				tmpList.add(nums[i]);
				subPermuteUnique(nums, visited, tmpList, res);
				tmpList.remove(tmpList.size() - 1);
				visited[i] = false;
			}
		}
	}

	public static void main(String[] args) {
		int[] nums = { 1, 1, 2 };
		List<List<Integer>> res = new Solution().permuteUnique(nums);
		for (List<Integer> list : res)
			System.out.println(list);
	}
}