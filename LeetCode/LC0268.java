public class Solution {
	public int missingNumber(int[] nums) {
		long realSum = nums.length * (nums.length + 1) / 2;
		long actualSum = 0;
		for (int n : nums)
			actualSum += n;

		return (int) (realSum - actualSum);
	}

	public static void main(String[] args) {
		int[] nums = { 0, 1, 3 };
		System.out.println(new Solution().missingNumber(nums));
	}
}