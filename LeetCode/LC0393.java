public class Solution {
	public final static int MSK11 = 0b10000000;
	public final static int RES11 = 0b00000000;

	public final static int MSK21 = 0b11100000;
	public final static int RES21 = 0b11000000;
	public final static int MSK22 = 0b11000000;
	public final static int RES22 = 0b10000000;

	public final static int MSK31 = 0b11110000;
	public final static int RES31 = 0b11100000;
	public final static int MSK32 = 0b11000000;
	public final static int RES32 = 0b10000000;
	public final static int MSK33 = 0b11000000;
	public final static int RES33 = 0b10000000;

	public final static int MSK41 = 0b11111000;
	public final static int RES41 = 0b11110000;
	public final static int MSK42 = 0b11000000;
	public final static int RES42 = 0b10000000;
	public final static int MSK43 = 0b11000000;
	public final static int RES43 = 0b10000000;
	public final static int MSK44 = 0b11000000;
	public final static int RES44 = 0b10000000;

	public boolean validUtf8(int[] data) {
		int i = 0;
		while (i < data.length) {
			if ((data[i] & MSK11) == RES11)
				i++;
			else if (data.length - i < 2)
				return false;
			else if ((data[i] & MSK21) == RES21 && (data[i + 1] & MSK22) == RES22)
				i += 2;
			else if (data.length - i < 3)
				return false;
			else if ((data[i] & MSK31) == RES31 && (data[i + 1] & MSK32) == RES32 && (data[i + 2] & MSK33) == RES33)
				i += 3;
			else if (data.length - i < 4)
				return false;
			else if ((data[i] & MSK41) == RES41 && (data[i + 1] & MSK42) == RES42 && (data[i + 2] & MSK43) == RES43 && (data[i + 3] & MSK44) == RES44)
				i += 4;
			else {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		int[] a = { 197, 130, 1 };
		int[] b = { 235, 140, 4 };
		System.out.println(new Solution().validUtf8(a));
		System.out.println(new Solution().validUtf8(b));
	}
}