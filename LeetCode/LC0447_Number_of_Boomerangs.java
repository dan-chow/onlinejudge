import java.util.HashMap;

public class Solution {
	public int numberOfBoomerangs(int[][] points) {
		int res = 0;

		HashMap<Integer, Integer> map = new HashMap<>();
		for (int i = 0; i < points.length; i++) {
			for (int j = 0; j < points.length; j++) {
				if (i == j)
					continue;

				int dist = distSquare(points[i], points[j]);
				map.put(dist, map.getOrDefault(dist, 0) + 1);
			}

			for (int val : map.values())
				res += val * (val - 1);

			map.clear();
		}

		return res;
	}

	private int distSquare(int[] p1, int[] p2) {
		int dx = p2[0] - p1[0], dy = p2[1] - p1[1];
		return dx * dx + dy * dy;
	}

	public static void main(String[] args) {
		int[][] points = { { 0, 0 }, { 1, 0 }, { 2, 0 } };
		System.out.println(new Solution().numberOfBoomerangs(points));
	}
}