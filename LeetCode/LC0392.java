public class Solution {
	public boolean isSubsequence(String s, String t) {
		if (s.length() > t.length())
			return false;

		int i = 0;
		int j = 0;
		while (i < s.length() && j < t.length()) {
			if (s.charAt(i) == t.charAt(j)) {
				i++;
				j++;
			} else {
				j++;
			}
		}

		return (i == s.length());
	}

	public static void main(String[] args) {
		System.out.println(new Solution().isSubsequence("abc", "ahbgdc"));
		System.out.println(new Solution().isSubsequence("axc", "ahbgdc"));
	}
}