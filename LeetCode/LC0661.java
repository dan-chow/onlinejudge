public class Solution {
	public int[][] imageSmoother(int[][] M) {
		if (M == null || M.length == 0)
			return M;

		int rows = M.length;
		if (rows == 0)
			return M;
		int cols = M[0].length;

		int[][] res = new int[rows][cols];

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				res[i][j] = getAverage(M, i, j);
			}
		}
		return res;
	}

	private int getAverage(int[][] matrix, int i, int j) {
		int m = matrix.length, n = matrix[0].length;

		int sum = matrix[i][j], num = 1;
		if (i > 0) {
			sum += matrix[i - 1][j];
			num++;
		}
		if (j > 0) {
			sum += matrix[i][j - 1];
			num++;
		}
		if (i < m - 1) {
			sum += matrix[i + 1][j];
			num++;
		}
		if (j < n - 1) {
			sum += matrix[i][j + 1];
			num++;
		}
		if (i > 0 && j > 0) {
			sum += matrix[i - 1][j - 1];
			num++;
		}
		if (i > 0 && j < n - 1) {
			sum += matrix[i - 1][j + 1];
			num++;
		}

		if (i < m - 1 && j > 0) {
			sum += matrix[i + 1][j - 1];
			num++;
		}

		if (i < m - 1 && j < n - 1) {
			sum += matrix[i + 1][j + 1];
			num++;
		}

		return sum / num;
	}

	public static void main(String[] args) {
		int[][] M1 = { { 1, 1, 1 }, { 1, 0, 1 }, { 1, 1, 1 } };
		int[][] res1 = new Solution().imageSmoother(M1);
		for (int[] row : res1) {
			for (int i : row)
				System.out.print(i + " ");
			System.out.println();
		}

	}
}