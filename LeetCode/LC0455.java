import java.util.Arrays;

public class Solution {
	public int findContentChildren(int[] g, int[] s) {
		if (g == null || g.length == 0 || s == null || s.length == 0)
			return 0;

		Arrays.sort(g);
		Arrays.sort(s);
		int cnt = 0, gIdx = 0, sIdx = 0;
		while (gIdx < g.length && sIdx < s.length) {
			if (g[gIdx] <= s[sIdx]) {
				cnt++;
				gIdx++;
				sIdx++;
			} else {
				sIdx++;
			}
		}
		return cnt;
	}

	public static void main(String[] args) {
		int[] g1 = { 1, 2, 3 };
		int[] s1 = { 1, 1 };
		System.out.println(new Solution().findContentChildren(g1, s1));

		int[] g2 = { 1, 2 };
		int[] s2 = { 1, 2, 3 };
		System.out.println(new Solution().findContentChildren(g2, s2));
	}
}