import java.util.ArrayList;
import java.util.List;

public class Solution {
	public List<Integer> cheapestJump(int[] A, int B) {
		List<Integer> res = new ArrayList<>();
		if (A == null || A.length == 0)
			return res;

		int N = A.length;

		if (A[N - 1] == -1)
			return res;

		int[] prev = new int[N];
		int[] cost = new int[N];

		prev[N - 1] = -1;
		cost[N - 1] = 0;

		for (int i = N - 2; i >= 0; i--) {
			if (A[i] == -1) {
				cost[i] = -1;
				continue;
			}

			int min = Integer.MAX_VALUE;
			int minIdx = -1;

			for (int j = 1; j <= B && i + j < N; j++) {
				if (cost[i + j] != -1 && cost[i + j] < min) {
					min = cost[i + j];
					minIdx = i + j;
				}
			}

			if (minIdx == -1) {
				cost[i] = -1;
				continue;
			}

			prev[i] = minIdx;
			cost[i] = A[i] + cost[minIdx];
		}

		int node = 0;
		if (cost[node] == -1)
			return res;

		while (node != -1) {
			res.add(node + 1);
			node = prev[node];
		}

		return res;
	}

	public static void main(String[] args) {
		int[] A = { 1, 2, 4, -1, 2 };
		System.out.println(new Solution().cheapestJump(A, 2));
		System.out.println(new Solution().cheapestJump(A, 1));
	}
}