import java.util.Arrays;

public class Solution {
	public int[] plusOne(int[] digits) {
		int[] tmpRes = new int[digits.length + 1];

		int tmpSum = digits[digits.length - 1] + 1;

		tmpRes[tmpRes.length - 1] = tmpSum % 10;
		int carray = tmpSum / 10;
		for (int i = 1; i < digits.length; i++) {
			tmpSum = digits[digits.length - i - 1] + carray;
			tmpRes[tmpRes.length - i - 1] = tmpSum % 10;
			carray = tmpSum / 10;
		}
		if (carray > 0)
			tmpRes[0] = 1;

		if (tmpRes[0] == 0) {
			int[] res = Arrays.copyOfRange(tmpRes, 1, tmpRes.length);
			return res;
		}

		return tmpRes;
	}

	public static void main(String[] args) {
		int[] digits = { 9, 9, 9 };
		int[] res = new Solution().plusOne(digits);
		for (int i : res)
			System.out.println(i);
	}
}