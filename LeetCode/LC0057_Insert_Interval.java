import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Interval {
	int start;
	int end;

	Interval() {
		start = 0;
		end = 0;
	}

	Interval(int s, int e) {
		start = s;
		end = e;
	}
}

class Point implements Comparable<Point> {
	public final static int LEFT = 0;
	public final static int RIGHT = 1;

	int x;
	int label;

	public Point(int x, int label) {
		this.x = x;
		this.label = label;
	}

	@Override
	public int compareTo(Point other) {
		if (this.x != other.x)
			return this.x - other.x;
		return this.label - other.label;
	}
}

public class Solution {
	public List<Interval> insert(List<Interval> intervals, Interval newInterval) {
		List<Interval> res = new ArrayList<>();

		List<Point> points = new ArrayList<>();
		for (Interval interval : intervals) {
			points.add(new Point(interval.start, Point.LEFT));
			points.add(new Point(interval.end, Point.RIGHT));
		}
		points.add(new Point(newInterval.start, Point.LEFT));
		points.add(new Point(newInterval.end, Point.RIGHT));

		Collections.sort(points);

		int start = 0;
		int cnt = 0;
		for (int i = 0; i < points.size(); i++) {
			if (cnt == 0)
				start = points.get(i).x;

			if (points.get(i).label == Point.LEFT)
				cnt++;
			else
				cnt--;

			if (cnt == 0)
				res.add(new Interval(start, points.get(i).x));
		}

		return res;
	}

	public static void main(String[] args) {

		List<Interval> intervals1 = new ArrayList<>();
		intervals1.add(new Interval(1, 3));
		intervals1.add(new Interval(6, 9));
		List<Interval> res1 = new Solution().insert(intervals1, new Interval(2, 5));
		for (Interval i : res1)
			System.out.println(i.start + " " + i.end);

		System.out.println();

		List<Interval> intervals2 = new ArrayList<>();
		intervals2.add(new Interval(1, 2));
		intervals2.add(new Interval(3, 5));
		intervals2.add(new Interval(6, 7));
		intervals2.add(new Interval(8, 10));
		intervals2.add(new Interval(12, 16));
		List<Interval> res2 = new Solution().insert(intervals2, new Interval(4, 9));
		for (Interval i : res2)
			System.out.println(i.start + " " + i.end);
	}
}