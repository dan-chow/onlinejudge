public class Solution {
	public int thirdMax(int[] nums) {

		Integer first = null, second = null, third = null;

		for (Integer n : nums) {
			if (n.equals(first) || n.equals(second) || n.equals(third))
				continue;

			if (first == null || n > first) {
				third = second;
				second = first;
				first = n;
			} else if (second == null || n > second) {
				third = second;
				second = n;
			} else if (third == null || n > third) {
				third = n;
			}
		}

		return third != null ? third : first;
	}

	public static void main(String[] args) {
		int[] nums1 = { 3, 2, 1 };
		System.out.println(new Solution().thirdMax(nums1));
		int[] nums2 = { 1, 2 };
		System.out.println(new Solution().thirdMax(nums2));
		int[] nums3 = { 2, 2, 3, 1 };
		System.out.println(new Solution().thirdMax(nums3));
	}
}