public class Solution {
	public int bulbSwitch(int n) {
		// divisors comes in pairs, except for square
		return (int) Math.sqrt(n);
	}

	public static void main(String[] args) {
		System.out.println(new Solution().bulbSwitch(3));
	}
}