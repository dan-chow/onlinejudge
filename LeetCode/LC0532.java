import java.util.Arrays;

public class Solution {
	public int findPairs(int[] nums, int k) {
		Arrays.sort(nums);

		int cnt = 0;
		for (int i = 0; i < nums.length - 1; i++) {
			while (i > 0 && i < nums.length && nums[i] == nums[i - 1])
				i++;

			if (i >= nums.length - 1)
				break;

			for (int j = i + 1; j < nums.length && nums[j] - nums[i] <= k; j++) {
				if (nums[j] - nums[i] == k) {
					cnt++;
					break;
				}
			}
		}
		return cnt;
	}

	public static void main(String[] args) {
		int[] nums1 = { 3, 1, 4, 1, 5 };
		System.out.println(new Solution().findPairs(nums1, 2));
		int[] nums2 = { 1, 2, 3, 4, 5 };
		System.out.println(new Solution().findPairs(nums2, 1));
		int[] nums3 = { 1, 3, 1, 5, 4 };
		System.out.println(new Solution().findPairs(nums3, 0));
	}
}