import java.util.LinkedList;
import java.util.Queue;

public class Solution {
	public static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode(int x) {
			val = x;
		}
	}

	public TreeNode invertTree(TreeNode root) {
		if (root == null)
			return root;

		TreeNode left = root.left;
		root.left = root.right;
		root.right = left;

		if (root.left != null)
			root.left = invertTree(root.left);

		if (root.right != null)
			root.right = invertTree(root.right);

		return root;
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(4);
		root.left = new TreeNode(2);
		root.left.left = new TreeNode(1);
		root.left.right = new TreeNode(3);
		root.right = new TreeNode(7);
		root.right.left = new TreeNode(6);
		root.right.right = new TreeNode(9);

		root = new Solution().invertTree(root);

		Queue<TreeNode> queue = new LinkedList<>();
		queue.add(root);
		while (!queue.isEmpty()) {
			TreeNode tmp = queue.poll();
			System.out.println(tmp.val);
			if (tmp.left != null)
				queue.add(tmp.left);
			if (tmp.right != null)
				queue.add(tmp.right);
		}
	}
}