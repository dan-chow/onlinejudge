public class Solution {
	public int[] searchRange(int[] nums, int target) {
		int leftIdx = searchLeftIdx(nums, target);
		if (leftIdx == -1)
			return new int[] { -1, -1 };
		int rightIdx = searchRightIdx(nums, target);
		return new int[] { leftIdx, rightIdx };
	}

	private int searchLeftIdx(int[] nums, int target) {
		int beg = 0, end = nums.length - 1;
		while (beg <= end) {
			int mid = (beg + end) / 2;
			if (nums[mid] >= target)
				end = mid - 1;
			else
				beg = mid + 1;
		}
		if (end + 1 < nums.length && nums[end + 1] == target)
			return end + 1;
		return -1;
	}

	private int searchRightIdx(int[] nums, int target) {
		int beg = 0, end = nums.length - 1;
		while (beg <= end) {
			int mid = (beg + end) / 2;
			if (nums[mid] <= target)
				beg = mid + 1;
			else
				end = mid - 1;
		}
		if (beg - 1 >= 0 && nums[beg - 1] == target)
			return beg - 1;
		return -1;
	}

	public static void main(String[] args) {
		int[] nums = { 5, 7, 7, 8, 8, 10 };
		int[] res = new Solution().searchRange(nums, 8);
		for (int i : res)
			System.out.print(i + " ");
	}
}