public class Solution {
	public int findComplement(int num) {

		int i = 31;
		for (; i >= 0; i--) {
			if ((num & (1 << i)) != 0)
				break;
		}
		for (; i >= 0; i--)
			num ^= (1 << i);

		return num;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().findComplement(5));
		System.out.println(new Solution().findComplement(1));
	}
}