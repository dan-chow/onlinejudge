public class Solution {
	public int rob(int[] nums) {

		// rob[i] = norob[i - 1] + nums[i];
		// norob[i] = max(rob[i - 1], norob[i - 1]);
		int norob = 0;
		int rob = 0;

		for (int n : nums) {
			int newRob = norob + n;
			norob = Math.max(rob, norob);
			rob = newRob;
		}
		return Math.max(rob, norob);
	}

	public static void main(String[] args) {
		int[] nums = { 3, 1, 0, 3, 2, 9 };
		System.out.println(new Solution().rob(nums));

	}
}