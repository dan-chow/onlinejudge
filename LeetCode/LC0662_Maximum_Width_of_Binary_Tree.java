import java.util.LinkedList;
import java.util.Queue;

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

class NodeInfo {
	TreeNode node;
	int levelIdx;

	public NodeInfo(TreeNode node, int levelIdx) {
		this.node = node;
		this.levelIdx = levelIdx;
	}

}

public class Solution {
	public int widthOfBinaryTree(TreeNode root) {
		if (root == null)
			return 0;
		int maxWidth = 1;

		Queue<NodeInfo> queue = new LinkedList<>();
		queue.add(new NodeInfo(root, 1));

		while (!queue.isEmpty()) {
			int levelSize = queue.size();
			int left = -1;
			for (int i = 0; i < levelSize; i++) {
				NodeInfo nodeInfo = queue.poll();

				if (i == 0)
					left = nodeInfo.levelIdx;

				if (i == levelSize - 1)
					maxWidth = Math.max(maxWidth, nodeInfo.levelIdx - left + 1);

				if (nodeInfo.node.left != null)
					queue.add(new NodeInfo(nodeInfo.node.left, 2 * nodeInfo.levelIdx - 1));
				if (nodeInfo.node.right != null)
					queue.add(new NodeInfo(nodeInfo.node.right, 2 * nodeInfo.levelIdx));
			}

		}
		return maxWidth;
	}

	public static void main(String[] args) {
		TreeNode root1 = new TreeNode(1);
		root1.left = new TreeNode(3);
		root1.left.left = new TreeNode(5);
		root1.left.right = new TreeNode(3);
		root1.right = new TreeNode(2);
		root1.right.right = new TreeNode(9);
		System.out.println(new Solution().widthOfBinaryTree(root1));

		TreeNode root2 = new TreeNode(1);
		root2.left = new TreeNode(3);
		root2.left.left = new TreeNode(5);
		root2.left.right = new TreeNode(3);
		System.out.println(new Solution().widthOfBinaryTree(root2));

		TreeNode root3 = new TreeNode(1);
		root3.left = new TreeNode(3);
		root3.left.left = new TreeNode(5);
		root3.right = new TreeNode(2);
		System.out.println(new Solution().widthOfBinaryTree(root3));

		TreeNode root4 = new TreeNode(1);
		root4.left = new TreeNode(3);
		root4.left.left = new TreeNode(5);
		root4.left.left.left = new TreeNode(6);
		root4.right = new TreeNode(2);
		root4.right.right = new TreeNode(9);
		root4.right.right.right = new TreeNode(7);
		System.out.println(new Solution().widthOfBinaryTree(root4));

	}
}