public class Solution {
	public int hammingDistance(int x, int y) {
		int tmp = x ^ y;
		int cnt = 0;
		for (int i = 0; i < 32; i++) {
			if ((tmp & 1 << i) != 0) {
				cnt++;
			}
		}
		return cnt;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().hammingDistance(1, 4));
	}
}