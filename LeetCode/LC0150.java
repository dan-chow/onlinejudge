import java.util.Stack;

public class Solution {
	public int evalRPN(String[] tokens) {
		Stack<Integer> stack = new Stack<>();
		int left;
		int right;
		for (String s : tokens) {
			if (s.trim().equals("+")) {
				right = stack.pop();
				left = stack.pop();
				stack.push(left + right);
			} else if (s.trim().equals("-")) {
				right = stack.pop();
				left = stack.pop();
				stack.push(left - right);
			} else if (s.trim().equals("*")) {
				right = stack.pop();
				left = stack.pop();
				stack.push(left * right);
			} else if (s.trim().equals("/")) {
				right = stack.pop();
				left = stack.pop();
				stack.push(left / right);
			} else {
				stack.push(Integer.parseInt(s));
			}
		}
		return stack.pop();
	}

	public static void main(String[] args) {
		String[] tokens1 = { "2", "1", "+", "3", "*" };
		System.out.println(new Solution().evalRPN(tokens1));
		String[] tokens2 = { "4", "13", "5", "/", "+" };
		System.out.println(new Solution().evalRPN(tokens2));
	}
}