public class Solution {
	public boolean canPlaceFlowers(int[] flowerbed, int n) {
		int max = 0;

		int i = 0;
		while (i < flowerbed.length) {
			if (i >= flowerbed.length)
				break;

			if (i == flowerbed.length - 1) {
				if (flowerbed[flowerbed.length - 1] == 0)
					max++;

				break;
			}

			if (flowerbed[i] == 0) {
				if (flowerbed[i + 1] == 0) {
					max++;
					i += 2;
				} else {
					i++;
					while (i < flowerbed.length && flowerbed[i] == 1)
						i++;
					i++;
				}
			} else {
				i += 2;
			}
		}

		return n <= max;
	}

	public static void main(String[] args) {
		int[] flowerbed = { 1, 0, 0, 0, 1 };
		System.out.println(new Solution().canPlaceFlowers(flowerbed, 1));
		System.out.println(new Solution().canPlaceFlowers(flowerbed, 2));
	}
}