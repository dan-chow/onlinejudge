import java.util.Arrays;

public class Solution {
	public int maximumProduct(int[] nums) {

		Arrays.sort(nums);

		int left0 = nums[nums.length - 1] * nums[nums.length - 2] * nums[nums.length - 3];
		int left2 = nums[0] * nums[1] * nums[nums.length - 1];

		return Math.max(left0, left2);
	}

	public static void main(String[] args) {
		int[] nums = { -4, -3, -2, -1, 60 };
		System.out.println(new Solution().maximumProduct(nums));
	}
}