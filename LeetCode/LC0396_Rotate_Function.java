public class Solution {
	public int maxRotateFunction(int[] A) {
		if (A.length == 0)
			return 0;

		int sum = 0;
		for (int i = 0; i < A.length; i++) {
			sum += A[i];
		}

		int F = 0;
		for (int i = 0; i < A.length; i++) {
			F += i * A[i];
		}
		int max = F;
		for (int i = 1; i < A.length; i++) {
			int tmp = A[(A.length - i) % A.length];
			F = F + sum - A.length * tmp;
			if (F > max)
				max = F;
		}

		return max;
	}

	public static void main(String[] args) {
		int[] A = { 4, 3, 2, 6 };
		System.out.println(new Solution().maxRotateFunction(A));
	}
}