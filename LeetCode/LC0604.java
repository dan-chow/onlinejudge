import java.util.LinkedList;

public class StringIterator {

	LinkedList<Character> charQueue = new LinkedList<>();
	LinkedList<Integer> intQueue = new LinkedList<>();

	// L1e2t1C1o1d1e1
	public StringIterator(String compressedString) {
		charQueue = new LinkedList<>();
		intQueue = new LinkedList<>();

		for (int i = 0; i < compressedString.length(); i++) {
			if (compressedString.charAt(i) >= '0' && compressedString.charAt(i) <= '9') {
				int cnt = compressedString.charAt(i) - '0';

				i++;
				while (i < compressedString.length() && compressedString.charAt(i) >= '0' && compressedString.charAt(i) <= '9') {
					cnt = cnt * 10 + compressedString.charAt(i) - '0';
					i++;
				}
				i--;
				intQueue.add(cnt);
			} else {
				charQueue.add(compressedString.charAt(i));
			}
		}
	}

	public char next() {
		if (!hasNext())
			return ' ';

		int cnt = intQueue.removeFirst();
		char ch = charQueue.removeFirst();
		if (--cnt > 0) {
			intQueue.addFirst(cnt);
			charQueue.addFirst(ch);
		}
		return ch;
	}

	public boolean hasNext() {
		return !intQueue.isEmpty();

	}

	public static void main(String[] args) {

		StringIterator iterator = new StringIterator("L1e2t1C1o1d1e1");

		System.out.println(iterator.next()); // return 'L'
		System.out.println(iterator.next()); // return 'e'
		System.out.println(iterator.next()); // return 'e'
		System.out.println(iterator.next()); // return 't'
		System.out.println(iterator.next()); // return 'C'
		System.out.println(iterator.next()); // return 'o'
		System.out.println(iterator.next()); // return 'd'
		System.out.println(iterator.hasNext()); // return true
		System.out.println(iterator.next()); // return 'e'
		System.out.println(iterator.hasNext()); // return false
		System.out.println(iterator.next()); // return ' '

	}
}
