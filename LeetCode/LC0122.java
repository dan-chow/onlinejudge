public class Solution {
	public int maxProfit(int[] prices) {
		int max = 0;
		int idx = 0;
		while (idx < prices.length - 1) {
			if (idx < prices.length - 1 && prices[idx + 1] <= prices[idx])
				idx++;

			int end = idx;
			while (end < prices.length - 1 && prices[end + 1] > prices[end])
				end++;

			max += prices[end] - prices[idx];
			idx = end;
		}
		return max;
	}

	public static void main(String[] args) {
		int[] prices = { 3, 3 };
		System.out.println(new Solution().maxProfit(prices));
	}
}