public class Solution {
	public void rotate(int[][] matrix) {
		int n = matrix.length;
		for (int len = n; len >= 1; len -= 2) {
			int top = (n - len) / 2;
			int left = top;
			int right = left + len - 1;
			int bottom = right;
			for (int i = 0; i < right - left; i++) {
				int x1 = top;
				int y1 = left + i;

				int x2 = top + i;
				int y2 = right;

				int x3 = bottom;
				int y3 = right - i;

				int x4 = bottom - i;
				int y4 = left;

				int tmp = matrix[x4][y4];
				matrix[x4][y4] = matrix[x3][y3];
				matrix[x3][y3] = matrix[x2][y2];
				matrix[x2][y2] = matrix[x1][y1];
				matrix[x1][y1] = tmp;
			}

		}
	}

	public static void main(String[] args) {
		int[][] matrix1 = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		new Solution().rotate(matrix1);
		for (int[] row : matrix1) {
			for (int num : row)
				System.out.print(num + " ");

			System.out.println();
		}

		int[][] matrix2 = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 }, { 13, 14, 15, 16 } };
		new Solution().rotate(matrix2);
		for (int[] row : matrix2) {
			for (int num : row)
				System.out.print(num + " ");

			System.out.println();
		}
	}
}