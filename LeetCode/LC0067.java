public class Solution {
	public String addBinary(String a, String b) {
		if (a.length() > b.length())
			return addBinary(b, a);

		StringBuffer newA = new StringBuffer(a);
		for (int i = 0; i < b.length() - a.length(); i++) {
			newA.insert(0, 0);
		}

		StringBuffer res = new StringBuffer();
		int carray = 0;
		for (int i = b.length() - 1; i >= 0; i--) {
			int tmpSum = newA.charAt(i) - '0' + b.charAt(i) - '0' + carray;
			if (tmpSum > 1) {
				res.insert(0, tmpSum % 2);
				carray = 1;
			} else {
				res.insert(0, tmpSum);
				carray = 0;
			}
		}
		if (carray > 0)
			res.insert(0, 1);

		return res.toString();
	}

	public static void main(String[] args) {
		System.out.println(new Solution().addBinary("11", "1"));
	}
}