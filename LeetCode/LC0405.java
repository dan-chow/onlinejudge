public class Solution {
	public String toHex(int num) {
		if (num == 0)
			return "0";

		StringBuffer sb = new StringBuffer();
		while (num != 0) {
			int tmp = num & 0xF;
			if (tmp < 10)
				sb.append((char) (tmp + '0'));
			else
				sb.append((char) ('a' + tmp - 10));

			num = num >>> 4;
		}
		return sb.reverse().toString();
	}

	public static void main(String[] args) {
		System.out.println(new Solution().toHex(26));
		System.out.println(new Solution().toHex(-1));
	}
}
