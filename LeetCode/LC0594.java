import java.util.HashMap;
import java.util.Map.Entry;

public class Solution {
	public int findLHS(int[] nums) {
		HashMap<Integer, Integer> map = new HashMap<>();
		for (int i : nums) {
			if (!map.containsKey(i))
				map.put(i, 0);

			map.put(i, map.get(i) + 1);
		}

		int max = Integer.MIN_VALUE;
		for (Entry<Integer, Integer> entry : map.entrySet()) {
			int val = entry.getKey();
			if (map.containsKey(val + 1)) {
				if (map.get(val + 1) + entry.getValue() > max)
					max = map.get(val + 1) + entry.getValue();
			}
			if (map.containsKey(val - 1)) {
				if (map.get(val - 1) + entry.getValue() > max)
					max = map.get(val - 1) + entry.getValue();
			}

		}

		return max == Integer.MIN_VALUE ? 0 : max;
	}

	public static void main(String[] args) {
		int[] nums = { 1, 3, 2, 2, 5, 2, 3, 7 };
		System.out.println(new Solution().findLHS(nums));
	}

}