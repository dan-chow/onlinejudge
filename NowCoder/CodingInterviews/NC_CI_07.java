public class Solution {
	public int Fibonacci(int n) {
		if (n == 0 || n == 1)
			return n;
		int f1 = 0;
		int f2 = 1;
		for (int i = 2; i <= n; i++) {
			f2 = f1 + f2;
			f1 = f2 - f1;
		}
		return f2;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().Fibonacci(3));
	}
}