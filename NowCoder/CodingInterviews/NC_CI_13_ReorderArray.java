public class Solution {
	public void reOrderArray(int[] array) {
		for (int i = 1; i < array.length; i++) {
			// (array[i] & 1) != 0
			if (array[i] % 2 == 1) {
				int tmp = array[i];
				int insertPos = findInsertPosForOdd(array, 0, i - 1);
				if (insertPos == i)
					continue;

				for (int j = i; j > insertPos; j--)
					array[j] = array[j - 1];
				array[insertPos] = tmp;
			}
		}
	}

	private int findInsertPosForOdd(int[] array, int begin, int end) {
		while (begin < end) {
			int mid = (begin + end) / 2;
			if (array[mid] % 2 == 0) {
				end = mid;
			} else {
				begin = mid + 1;
			}
		}
		return array[begin] % 2 == 1 ? begin + 1 : begin;
	}

	public static void main(String[] args) {
		int[] array = { 1, 9, 10, 3, 5, 2, 6, 8, 7 };
		new Solution().reOrderArray(array);
		for (int i : array)
			System.out.println(i);
	}
}
