import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Solution {
	public String PrintMinNumber(int[] numbers) {

		List<Integer> list = new ArrayList<>();
		for (int n : numbers)
			list.add(n);

		Collections.sort(list, new Comparator<Integer>() {

			@Override
			public int compare(Integer i1, Integer i2) {
				StringBuffer sb1 = new StringBuffer().append(i1).append(i2);
				StringBuffer sb2 = new StringBuffer().append(i2).append(i1);
				return sb1.toString().compareTo(sb2.toString());
			}
		});

		StringBuffer sb = new StringBuffer();
		for (int i : list)
			sb.append(i);
		return sb.toString();
	}

	public static void main(String[] args) {
		int[] numbers = { 3, 32, 321 };
		System.out.println(new Solution().PrintMinNumber(numbers));
	}
}