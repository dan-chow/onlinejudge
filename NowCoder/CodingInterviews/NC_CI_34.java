public class Solution {
	public int FirstNotRepeatingChar(String str) {
		int[] dict = new int[255];
		for (int i = 0; i < str.length(); i++)
			dict[str.charAt(i)]++;

		for (int i = 0; i < str.length(); i++)
			if (dict[str.charAt(i)] == 1)
				return i;

		return -1;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().FirstNotRepeatingChar("aabbcdede"));
		System.out.println(new Solution().FirstNotRepeatingChar("google"));
		System.out.println(new Solution().FirstNotRepeatingChar("aa"));
	}
}