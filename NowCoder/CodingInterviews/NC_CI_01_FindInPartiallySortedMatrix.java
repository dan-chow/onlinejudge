public class Solution {
	public boolean Find(int target, int[][] array) {
		int m = array.length;
		if (m == 0)
			return false;
		int n = array[0].length;
		if (n == 0)
			return false;
		int i = m - 1;
		int j = 0;
		while (i >= 0 && j < n) {
			if (array[i][j] == target)
				return true;
			if (array[i][j] < target)
				j++;
			else
				i--;
		}
		return false;
	}

	public static void main(String[] args) {
		int[][] array = { { 1, 2, 8, 9 }, { 2, 4, 9, 12 }, { 4, 7, 10, 13 }, { 6, 8, 11, 15 } };
		System.out.println(new Solution().Find(7, array));
		System.out.println(new Solution().Find(5, array));
	}
}