import java.util.ArrayList;

class TreeNode {
	int val = 0;
	TreeNode left = null;
	TreeNode right = null;

	public TreeNode(int val) {
		this.val = val;

	}

}

public class Solution {
	private ArrayList<ArrayList<Integer>> result;

	public ArrayList<ArrayList<Integer>> FindPath(TreeNode root, int target) {
		result = new ArrayList<ArrayList<Integer>>();
		if (root == null)
			return result;
		FindPath(root, target, new ArrayList<Integer>());
		return result;
	}

	private void FindPath(TreeNode root, int target, ArrayList<Integer> currPath) {
		if (root == null)
			return;

		currPath.add(root.val);

		if (root.val == target && root.left == null && root.right == null)
			result.add(new ArrayList<>(currPath));

		if (root.left != null)
			FindPath(root.left, target - root.val, new ArrayList<>(currPath));
		if (root.right != null)
			FindPath(root.right, target - root.val, new ArrayList<>(currPath));
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(10);
		root.left = new TreeNode(5);
		root.right = new TreeNode(12);
		root.left.left = new TreeNode(4);
		root.left.right = new TreeNode(7);

		Solution soln = new Solution();

		ArrayList<ArrayList<Integer>> paths = soln.FindPath(root, 22);
		for (ArrayList<Integer> p : paths)
			System.out.println(p);
	}
}