class TreeNode {
	int val = 0;
	TreeNode left = null;
	TreeNode right = null;

	public TreeNode(int val) {
		this.val = val;

	}

}

public class Solution {
	public boolean HasSubtree(TreeNode root1, TreeNode root2) {
		if (root1 == null || root2 == null)
			return false;

		if (root1.val == root2.val)
			if (isSubtree(root1, root2))
				return true;

		return HasSubtree(root1.left, root2) || HasSubtree(root1.right, root2);
	}

	private boolean isSubtree(TreeNode root1, TreeNode root2) {

		if (root2 == null)
			return true;

		if (root2 != null && root1 == null)
			return false;

		if (root1.val != root2.val)
			return false;

		return isSubtree(root1.left, root2.left) && isSubtree(root1.right, root2.right);

	}

	public static void main(String[] args) {
		TreeNode root1 = new TreeNode(8);
		root1.left = new TreeNode(8);
		root1.right = new TreeNode(7);
		root1.left.left = new TreeNode(9);
		root1.left.right = new TreeNode(2);
		root1.left.right.left = new TreeNode(4);
		root1.left.right.right = new TreeNode(7);

		TreeNode root2 = new TreeNode(8);
		root2.left = new TreeNode(9);
		root2.right = new TreeNode(2);

		System.out.println(new Solution().HasSubtree(root1, root2));
	}
}