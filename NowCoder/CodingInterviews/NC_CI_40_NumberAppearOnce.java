//num1,num2分别为长度为1的数组。传出参数
//将num1[0],num2[0]设置为返回结果
public class Solution {
	public void FindNumsAppearOnce(int[] array, int num1[], int num2[]) {
		int xor = 0;
		for (int n : array)
			xor ^= n;

		int diff = 1;
		for (int i = 0; i < 32; i++) {
			if ((xor & diff) != 0)
				break;
			diff = diff << 1;
		}

		num1[0] = num2[0] = 0;

		for (int n : array) {
			if ((n & diff) == 0)
				num1[0] ^= n;
			else
				num2[0] ^= n;
		}
	}

	public static void main(String[] args) {
		int[] array = { 1, 2, 3, 4, 2, 5, 4, 3 };
		int[] num1 = { 4 };
		int[] num2 = { 7 };
		new Solution().FindNumsAppearOnce(array, num1, num2);
		System.out.println(num1[0]);
		System.out.println(num2[0]);
	}
}