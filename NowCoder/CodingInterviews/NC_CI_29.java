import java.util.ArrayList;
import java.util.PriorityQueue;

public class Solution {
	public ArrayList<Integer> GetLeastNumbers_Solution(int[] input, int k) {
		PriorityQueue<Integer> queue = new PriorityQueue<>();
		for (int i : input)
			queue.add(i);

		ArrayList<Integer> res = new ArrayList<>();
		if (k == 0 || k > queue.size())
			return res;

		for (int i = 0; i < k; i++) {
			if (!queue.isEmpty())
				res.add(queue.poll());
		}
		return res;
	}

	public static void main(String[] args) {
		int[] input = { 4, 5, 1, 6, 2, 7, 3, 8 };
		System.out.println(new Solution().GetLeastNumbers_Solution(input, 4));
	}
}