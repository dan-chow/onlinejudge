public class Solution {
	public int JumpFloor(int target) {
		if (target == 1)
			return 1;
		if (target == 2)
			return 2;

		int f1 = 1;
		int f2 = 2;
		for (int i = 3; i <= target; i++) {
			f2 = f1 + f2;
			f1 = f2 - f1;
		}
		return f2;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().JumpFloor(4));
	}
}