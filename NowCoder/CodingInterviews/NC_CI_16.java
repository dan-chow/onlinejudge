class ListNode {
	int val;
	ListNode next = null;

	ListNode(int val) {
		this.val = val;
	}
}

public class Solution {

	public ListNode Merge(ListNode list1, ListNode list2) {
		if (list1 == null)
			return list2;
		if (list2 == null)
			return list1;

		ListNode vHead = new ListNode(0);
		ListNode p1 = list1, p2 = list2;
		ListNode q = vHead;
		while (p1 != null && p2 != null) {
			if (p1.val < p2.val) {
				q.next = p1;
				p1 = p1.next;
			} else {
				q.next = p2;
				p2 = p2.next;
			}
			q = q.next;
		}
		q.next = (p1 != null) ? p1 : p2;
		return vHead.next;

	}

	public static void main(String[] args) {
		ListNode list1 = new ListNode(1);
		list1.next = new ListNode(3);
		list1.next.next = new ListNode(5);
		list1.next.next.next = new ListNode(7);

		ListNode list2 = new ListNode(2);
		list2.next = new ListNode(4);
		list2.next.next = new ListNode(6);
		list2.next.next.next = new ListNode(8);

		ListNode merge = new Solution().Merge(list1, list2);
		ListNode p = merge;
		while (p != null) {
			System.out.println(p.val);
			p = p.next;
		}
	}
}