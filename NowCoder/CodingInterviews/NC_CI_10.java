public class Solution {

	public int RectCover(int target) {
		if (target == 0 || target == 1 || target == 2)
			return target;

		int f1 = 1;
		int f2 = 2;
		for (int i = 3; i <= target; i++) {
			f2 = f1 + f2;
			f1 = f2 - f1;
		}
		return f2;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().RectCover(4));
	}
}