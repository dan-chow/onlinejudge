public class Solution {
	public double Power(double base, int exponent) throws Exception {
		if (Math.abs(base) < 1e-6 && exponent <= 0)
			throw new Exception("illegal input");

		if (exponent == 0)
			return 1.0;

		if (exponent < 0)
			return 1.0 / Power(base, -exponent);

		double tmp = base;
		double result = 1.0;
		while (exponent > 0) {
			if (exponent % 2 == 1)
				result *= tmp;
			tmp *= tmp;
			exponent /= 2;
		}
		return result;
	}

	public static void main(String[] args) {
		try {
			System.out.println(new Solution().Power(2.0, 5));
			System.out.println(new Solution().Power(2.0, -1));
			System.out.println(new Solution().Power(0, -1));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
