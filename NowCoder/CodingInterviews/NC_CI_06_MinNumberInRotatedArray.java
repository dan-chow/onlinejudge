public class Solution {
	public int minNumberInRotateArray(int[] array) {
		if (array == null || array.length == 0)
			return 0;

		int begin = 0;
		int end = array.length - 1;
		while (begin < end) {
			int mid = (begin + end) / 2;
			if (array[mid] > array[end]) {
				begin = mid + 1;
			} else if (array[mid] < array[begin]) {
				end = mid;
			} else {
				return array[begin];
			}
		}
		return array[begin];
	}

	public static void main(String[] args) {
		int[] array = { 3, 4, 5, 1, 2 };
		System.out.println(new Solution().minNumberInRotateArray(array));
	}
}