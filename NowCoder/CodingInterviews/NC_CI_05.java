import java.util.Stack;

public class Solution {
	Stack<Integer> stack1 = new Stack<Integer>();
	Stack<Integer> stack2 = new Stack<Integer>();

	public void push(int node) {
		stack1.push(node);
	}

	public int pop() {
		if (stack2.isEmpty()) {
			while (!stack1.isEmpty()) {
				stack2.push(stack1.pop());
			}
		}
		return stack2.pop();
	}

	public static void main(String[] args) {
		Solution soln = new Solution();
		soln.push(1);
		soln.push(2);
		soln.push(3);

		System.out.println(soln.pop());
		System.out.println(soln.pop());
		System.out.println(soln.pop());
	}
}