public class Solution {
	public String replaceSpace(StringBuffer str) {
		if (str == null)
			return null;

		if (str.length() == 0)
			return str.toString();

		int spaceCnt = 0;
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == ' ')
				spaceCnt++;
		}

		int i = str.length() - 1;
		str.setLength(str.length() + 2 * spaceCnt);
		int j = str.length() - 1;
		while (spaceCnt > 0) {
			if (str.charAt(i) != ' ') {
				str.setCharAt(j, str.charAt(i));
				j--;
				i--;
			} else {
				str.setCharAt(j--, '0');
				str.setCharAt(j--, '2');
				str.setCharAt(j--, '%');
				i--;
				spaceCnt--;
			}
		}

		return str.toString();
	}

	public static void main(String[] args) {
		System.out.println(new Solution().replaceSpace(null));
		System.out.println(new Solution().replaceSpace(new StringBuffer("We are happy")));
	}
}