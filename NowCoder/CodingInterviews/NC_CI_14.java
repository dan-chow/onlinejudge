class ListNode {
	int val;
	ListNode next = null;

	ListNode(int val) {
		this.val = val;
	}
}

public class Solution {
	public ListNode FindKthToTail(ListNode head, int k) {
		if (head == null || k == 0)
			return null;

		ListNode p = head;
		ListNode q = p;
		while (--k > 0 && q != null) {
			q = q.next;
		}

		if (k > 0 || q == null)
			return null;

		while (q.next != null) {
			p = p.next;
			q = q.next;
		}
		return p;
	}

	public static void main(String[] args) {
		ListNode head = new ListNode(1);
		head.next = new ListNode(2);
		head.next.next = new ListNode(3);
		head.next.next.next = new ListNode(4);

		System.out.println(new Solution().FindKthToTail(head, 2).val);
	}
}