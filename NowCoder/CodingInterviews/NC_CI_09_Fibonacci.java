public class Solution {

	public int JumpFloorII(int target) {
		if (target == 1)
			return 1;
		int f = 1;
		int sum = 1;
		for (int i = 2; i <= target; i++) {
			f = 1 + sum;
			sum += f;
		}
		return f;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().JumpFloorII(4));
	}
}