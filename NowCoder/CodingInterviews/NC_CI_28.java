public class Solution {
	public int MoreThanHalfNum_Solution(int[] array) {
		if (array == null || array.length == 0)
			return 0;

		int num = array[0];
		int cnt = 1;
		for (int i = 1; i < array.length; i++) {
			if (cnt == 0) {
				num = array[i];
				cnt = 1;
			} else if (array[i] == num) {
				cnt++;
			} else {
				cnt--;
			}
		}

		if (cnt == 0)
			return 0;

		cnt = 0;
		for (int i = 0; i < array.length; i++)
			if (array[i] == num)
				cnt++;

		return cnt * 2 > array.length ? num : 0;
	}

	public static void main(String[] args) {
		int[] array = { 1, 2, 3, 2, 2, 2, 5, 4, 2 };
		System.out.println(new Solution().MoreThanHalfNum_Solution(array));
	}
}