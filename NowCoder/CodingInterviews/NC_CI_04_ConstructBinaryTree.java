import java.util.LinkedList;
import java.util.Queue;

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;

	TreeNode(int x) {
		val = x;
	}
}

public class Solution {
	public TreeNode reConstructBinaryTree(int[] pre, int[] in) {
		return reConstructBinaryTree(pre, 0, pre.length - 1, in, 0, in.length - 1);
	}

	private TreeNode reConstructBinaryTree(int[] pre, int pre_start, int pre_end, int[] in, int in_start, int in_end) {
		if (pre_start > pre_end || in_start > in_end)
			return null;

		int rootIdx = in_start;
		for (; rootIdx < in.length && rootIdx < in_end; rootIdx++)
			if (in[rootIdx] == pre[pre_start])
				break;

		int numLeft = rootIdx - in_start;

		TreeNode root = new TreeNode(pre[pre_start]);
		root.left = reConstructBinaryTree(pre, pre_start + 1, pre_start + numLeft, in, in_start, rootIdx);
		root.right = reConstructBinaryTree(pre, pre_start + numLeft + 1, pre_end, in, rootIdx + 1, in_end);

		return root;
	}

	public static void main(String[] args) {
		int[] pre = { 1, 2, 4, 7, 3, 5, 6, 8 };
		int[] in = { 4, 7, 2, 1, 5, 3, 8, 6 };
		Solution solution = new Solution();
		TreeNode root = solution.reConstructBinaryTree(pre, in);
		solution.print(root);

	}

	public void print(TreeNode root) {
		if (root == null)
			System.out.println("[]");
		Queue<TreeNode> queue = new LinkedList<>();
		queue.add(root);

		StringBuffer sb = new StringBuffer().append("[");
		while (!queue.isEmpty()) {
			sb.append("[");
			int sz = queue.size();
			while (sz-- > 0) {
				TreeNode node = queue.poll();
				sb.append(node.val).append(",");
				if (node.left != null)
					queue.add(node.left);
				if (node.right != null)
					queue.add(node.right);
			}
			sb.setLength(sb.length() - 1);
			sb.append("],");
		}
		sb.setLength(sb.length() - 1);
		sb.append("]");
		System.out.println(sb.toString());
	}
}
