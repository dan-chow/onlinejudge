import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution {
	public ArrayList<String> Permutation(String str) {
		ArrayList<String> res = new ArrayList<>();
		if (str == null || str.isEmpty())
			return res;

		char[] arr = new char[str.length()];
		for (int i = 0; i < str.length(); i++)
			arr[i] = str.charAt(i);
		Arrays.sort(arr);
		boolean[] visited = new boolean[str.length()];

		StringBuffer tmp = new StringBuffer();
		Permutation(arr, visited, tmp, res);

		return res;
	}

	private void Permutation(char[] arr, boolean[] visited, StringBuffer tmp, List<String> res) {
		if (tmp.length() == arr.length) {
			res.add(new String(tmp));
			return;
		}

		for (int i = 0; i < arr.length; i++) {
			if (i > 0 && arr[i] == arr[i - 1] && !visited[i - 1])
				continue;

			if (!visited[i]) {
				visited[i] = true;
				tmp.append((char) arr[i]);
				Permutation(arr, visited, tmp, res);
				tmp.setLength(tmp.length() - 1);
				visited[i] = false;
			}
		}

	}

	public static void main(String[] args) {
		System.out.println(new Solution().Permutation("abc"));
		System.out.println(new Solution().Permutation("aa"));
	}
}