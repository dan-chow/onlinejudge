class ListNode {
	int val;
	ListNode next = null;

	ListNode(int val) {
		this.val = val;
	}
}

public class Solution {
	public ListNode FindFirstCommonNode(ListNode pHead1, ListNode pHead2) {
		if (pHead1 == null || pHead2 == null)
			return null;

		int len1 = 0, len2 = 0;
		ListNode p = pHead1, q = pHead2;
		while (p != null) {
			len1++;
			p = p.next;
		}
		while (q != null) {
			len2++;
			q = q.next;
		}

		int diff = 0;
		if (len1 < len2) {
			p = pHead2;
			q = pHead1;
			diff = len2 - len1;
		} else {
			p = pHead1;
			q = pHead2;
			diff = len1 - len2;
		}

		for (int i = 0; i < diff; i++)
			p = p.next;

		while (p != null && q != null && p != q) {
			p = p.next;
			q = q.next;
		}

		return p;
	}

	public static void main(String[] args) {
		ListNode pHead1 = new ListNode(1);
		pHead1.next = new ListNode(2);
		pHead1.next.next = new ListNode(4);
		pHead1.next.next.next = new ListNode(5);

		ListNode pHead2 = new ListNode(3);
		pHead2.next = pHead1.next.next;

		System.out.println(new Solution().FindFirstCommonNode(pHead1, pHead2).val);
	}
}