public class Solution {
	public int GetUglyNumber_Solution(int index) {
		if (index == 0)
			return 0;

		int[] dp = new int[index];
		dp[0] = 1;

		int mul2 = 0, mul3 = 0, mul5 = 0;

		for (int i = 1; i < index; i++) {
			dp[i] = Math.min(2 * dp[mul2], Math.min(3 * dp[mul3], 5 * dp[mul5]));
			if (dp[i] == 2 * dp[mul2])
				mul2++;
			if (dp[i] == 3 * dp[mul3])
				mul3++;
			if (dp[i] == 5 * dp[mul5])
				mul5++;
		}
		return dp[index - 1];
	}

	public static void main(String[] args) {
		for (int i = 1; i < 12; i++) {
			System.out.println(new Solution().GetUglyNumber_Solution(i));
		}
	}
}