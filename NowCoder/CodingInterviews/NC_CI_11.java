public class Solution {
	public int NumberOf1(int n) {
		int i = 1;
		int cnt = 0;
		for (int j = 0; j < 32; j++) {
			if ((n & i) != 0)
				cnt++;
			i = i << 1;
		}
		return cnt;
	}

	public static void main(String[] args) {
		System.out.println(new Solution().NumberOf1(15));
	}
}