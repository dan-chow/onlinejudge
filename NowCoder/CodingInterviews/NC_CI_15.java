class ListNode {
	int val;
	ListNode next = null;

	ListNode(int val) {
		this.val = val;
	}
}

public class Solution {
	public ListNode ReverseList(ListNode head) {
		if (head == null || head.next == null)
			return head;

		ListNode p = head.next;
		head.next = null;

		ListNode q;
		while (p != null) {
			q = p;
			p = q.next;

			q.next = head;
			head = q;
		}
		return head;
	}

	public static void main(String[] args) {
		ListNode head = new ListNode(1);
		head.next = new ListNode(2);
		head.next.next = new ListNode(3);
		head.next.next.next = new ListNode(4);

		head = new Solution().ReverseList(head);
		ListNode p = head;
		while (p != null) {
			System.out.println(p.val);
			p = p.next;
		}
	}
}