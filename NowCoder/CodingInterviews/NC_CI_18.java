import java.util.LinkedList;
import java.util.Queue;

class TreeNode {
	int val = 0;
	TreeNode left = null;
	TreeNode right = null;

	public TreeNode(int val) {
		this.val = val;

	}

}

public class Solution {
	public void Mirror(TreeNode root) {
		if (root == null)
			return;

		TreeNode tmp = root.left;
		root.left = root.right;
		root.right = tmp;

		Mirror(root.left);
		Mirror(root.right);
	}

	public static void main(String[] args) {
		TreeNode root = new TreeNode(8);
		root.left = new TreeNode(6);
		root.right = new TreeNode(10);
		root.left.left = new TreeNode(5);
		root.left.right = new TreeNode(7);
		root.right.left = new TreeNode(9);
		root.right.right = new TreeNode(11);

		Solution soln = new Solution();
		soln.Mirror(root);
		soln.print(root);
	}

	public void print(TreeNode root) {
		if (root == null)
			System.out.println("[]");
		Queue<TreeNode> queue = new LinkedList<>();
		queue.add(root);

		StringBuffer sb = new StringBuffer().append("[");
		while (!queue.isEmpty()) {
			sb.append("[");
			int sz = queue.size();
			while (sz-- > 0) {
				TreeNode node = queue.poll();
				sb.append(node.val).append(",");
				if (node.left != null)
					queue.add(node.left);
				if (node.right != null)
					queue.add(node.right);
			}
			sb.setLength(sb.length() - 1);
			sb.append("],");
		}
		sb.setLength(sb.length() - 1);
		sb.append("]");
		System.out.println(sb.toString());
	}
}