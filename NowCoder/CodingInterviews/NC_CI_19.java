import java.util.ArrayList;
import java.util.List;

public class Solution {
	public ArrayList<Integer> printMatrix(int[][] matrix) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		if (matrix == null || matrix.length == 0 || matrix[0].length == 0)
			return list;

		int top = 0;
		int bottom = matrix.length - 1;
		int left = 0;
		int right = matrix[0].length - 1;

		int status = 0;
		int i = 0, j = 0;

		list.add(matrix[0][0]);
		while (true) {
			switch (status) {
			case 0:
				while (j < right) {
					list.add(matrix[i][++j]);
				}
				top++;
				break;
			case 1:
				while (i < bottom) {
					list.add(matrix[++i][j]);
				}
				right--;
				break;
			case 2:
				while (j > left) {
					list.add(matrix[i][--j]);
				}
				bottom--;
				break;
			case 3:
				while (i > top) {
					list.add(matrix[--i][j]);
				}
				left++;
				break;
			}

			status = (status + 1) % 4;

			if (list.size() == matrix.length * matrix[0].length)
				break;

		}
		return list;
	}

	public static void main(String[] args) {
		int[][] matrix = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 }, { 13, 14, 15, 16 } };
		List<Integer> res = new Solution().printMatrix(matrix);
		for (int i : res)
			System.out.println(i);
	}
}
