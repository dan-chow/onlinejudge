import java.util.ArrayList;

class ListNode {
	int val;
	ListNode next = null;

	ListNode(int val) {
		this.val = val;
	}
}

public class Solution {
	public ArrayList<Integer> printListFromTailToHead(ListNode listNode) {
		listNode = reverse(listNode);

		ArrayList<Integer> list = new ArrayList<Integer>();

		ListNode p = listNode;
		while (p != null) {
			list.add(p.val);
			p = p.next;
		}

		return list;
	}

	private ListNode reverse(ListNode head) {
		if (head == null || head.next == null)
			return head;

		ListNode p = head.next;
		head.next = null;

		ListNode q;
		while (p != null) {
			q = p;
			p = q.next;

			q.next = head;
			head = q;
		}
		return head;
	}

	public static void main(String[] args) {
		ListNode listNode = new ListNode(0);
		listNode.next = new ListNode(1);
		listNode.next.next = new ListNode(2);

		System.out.println(new Solution().printListFromTailToHead(listNode));
	}
}