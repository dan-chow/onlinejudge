import java.util.Stack;

public class Solution {
	private Stack<Integer> stack = new Stack<Integer>();
	private Stack<Integer> minStack = new Stack<Integer>();

	public void push(int node) {
		stack.push(node);
		if (minStack.isEmpty() || node < minStack.peek())
			minStack.push(node);
	}

	public void pop() {
		int top = stack.pop();
		if (top == minStack.peek())
			minStack.pop();
	}

	public int top() {
		return stack.peek();
	}

	public int min() {
		return minStack.peek();
	}

	public static void main(String[] args) {
		Solution soln = new Solution();
		soln.push(3);
		System.out.println(soln.min());
		soln.push(4);
		System.out.println(soln.min());
		soln.push(2);
		System.out.println(soln.min());
		soln.pop();
		System.out.println(soln.min());

	}
}