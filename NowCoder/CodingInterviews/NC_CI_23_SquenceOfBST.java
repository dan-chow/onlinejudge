public class Solution {
	public boolean VerifySquenceOfBST(int[] sequence) {
		if (sequence == null || sequence.length == 0)
			return false;
		return VerifySequenceOfBST(sequence, 0, sequence.length - 1);
	}

	private boolean VerifySequenceOfBST(int[] sequence, int begin, int end) {
		if (begin >= end)
			return true;

		int i = begin;
		while (i < end && sequence[i] < sequence[end])
			i++;

		for (int j = i; j < end; j++)
			if (sequence[j] < sequence[end])
				return false;

		return VerifySequenceOfBST(sequence, begin, i - 1) && VerifySequenceOfBST(sequence, i, end - 1);
	}

	public static void main(String[] args) {
		int[] sequence = { 1, 3, 2, 5, 7, 6, 4 };
		System.out.print(new Solution().VerifySquenceOfBST(sequence));
	}
}