import java.util.Stack;

public class Solution {
	public boolean IsPopOrder(int[] pushA, int[] popA) {
		Stack<Integer> stack = new Stack<>();
		int i = 0;
		int j = 0;
		while (i < pushA.length) {
			if (pushA[i] != popA[j]) {
				if (!stack.isEmpty() && stack.peek() == popA[j]) {
					stack.pop();
					j++;
				} else {
					stack.push(pushA[i++]);
				}

			} else {
				i++;
				j++;
			}
		}

		while (!stack.isEmpty())
			if (stack.pop() != popA[j++])
				return false;

		return j == popA.length && stack.isEmpty();
	}

	public static void main(String[] args) {
		int[] pushA = { 1, 2, 3, 4, 5 };
		int[] popAT = { 4, 5, 3, 2, 1 };
		int[] popAF = { 4, 3, 5, 1, 2 };
		System.out.println(new Solution().IsPopOrder(pushA, popAT));
		System.out.println(new Solution().IsPopOrder(pushA, popAF));

	}
}