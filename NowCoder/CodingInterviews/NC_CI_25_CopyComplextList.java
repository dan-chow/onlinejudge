class RandomListNode {
	int label;
	RandomListNode next = null;
	RandomListNode random = null;

	RandomListNode(int label) {
		this.label = label;
	}
}

public class Solution {
	public RandomListNode Clone(RandomListNode pHead) {
		duplicateNodes(pHead);
		fillRandomPointer(pHead);
		return splitNewNodes(pHead);
	}

	private void duplicateNodes(RandomListNode pHead) {
		RandomListNode p = pHead;
		while (p != null) {
			RandomListNode q = new RandomListNode(p.label);
			q.next = p.next;
			p.next = q;
			p = q.next;
		}
	}

	private void fillRandomPointer(RandomListNode pHead) {
		RandomListNode p = pHead;
		while (p != null) {
			if (p.random != null)
				p.next.random = p.random.next;

			p = p.next.next;
		}
	}

	private RandomListNode splitNewNodes(RandomListNode pHead) {
		RandomListNode fakeHead = new RandomListNode(-1);
		RandomListNode p = pHead;
		RandomListNode q = fakeHead;
		while (p != null) {
			q.next = p.next;
			q = q.next;

			p.next = q.next;
			p = p.next;
		}
		return fakeHead.next;
	}

	public static void main(String[] args) {
		RandomListNode A = new RandomListNode(1);
		RandomListNode B = new RandomListNode(2);
		RandomListNode C = new RandomListNode(3);
		RandomListNode D = new RandomListNode(4);
		RandomListNode E = new RandomListNode(5);

		A.next = B;
		B.next = C;
		C.next = D;
		D.next = E;
		E.next = null;

		A.random = C;
		B.random = E;
		C.random = null;
		D.random = B;
		E.random = null;

		RandomListNode p = A;
		System.out.println("old:");
		while (p != null) {
			System.out.println(p + ": " + p.label + ", next -> " + p.next + ", random -> " + p.random);
			p = p.next;
		}

		RandomListNode clonedList = new Solution().Clone(A);
		p = clonedList;
		System.out.println("new:");
		while (p != null) {
			System.out.println(p + ": " + p.label + ", next -> " + p.next + ", random -> " + p.random);
			p = p.next;
		}
	}
}