import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int num = in.nextInt();
		int size = in.nextInt();
		int[] arr = new int[num];
		for (int i = 0; i < num; i++)
			arr[i] = in.nextInt();
		in.close();

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == size)
				size *= 2;
		}

		System.out.println(size);
	}
}