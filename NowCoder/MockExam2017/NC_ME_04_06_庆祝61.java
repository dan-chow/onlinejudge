import java.util.Arrays;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] arr = new int[n];
		for (int i = 0; i < arr.length; i++)
			arr[i] = in.nextInt();
		in.close();

		Arrays.sort(arr);

		int max = 0;
		for (int i = 2; i < arr.length; i++) {
			max = Math.max(max, arr[i] - arr[i - 2]);
		}

		System.out.println(max);
	}
}