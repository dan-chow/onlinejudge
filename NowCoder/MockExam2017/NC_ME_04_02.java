import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String input = in.next();
		in.close();

		int result = 1;
		char last = input.charAt(0);
		for (int i = 1; i < input.length(); i++) {
			if (input.charAt(i) != last) {
				last = input.charAt(i);
				result++;
			}
		}

		System.out.println(result);
	}
}