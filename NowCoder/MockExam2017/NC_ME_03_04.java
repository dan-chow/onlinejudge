import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int num = in.nextInt();

		int[] arr = new int[num];
		for (int i = 0; i < num; i++) {
			arr[i] = in.nextInt();
		}
		in.close();

		while (arr[0] % 2 == 0) {
			arr[0] /= 2;
		}
		int base = arr[0];
		for (int i = 1; i < arr.length; i++) {
			while (arr[i] % 2 == 0) {
				arr[i] /= 2;
			}
			if (arr[i] != base) {
				System.out.println("NO");
				return;
			}
		}

		System.out.println("YES");

	}

}