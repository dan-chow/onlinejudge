import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String input = in.next();
		in.close();

		int wLeft = 0;
		int bLeft = 0;

		for (int i = 0; i < input.length(); i++) {
			if (i % 2 == 0) {
				if (input.charAt(i) == 'B') {
					wLeft++;
				} else {
					bLeft++;
				}
			} else {
				if (input.charAt(i) == 'B') {
					bLeft++;
				} else {
					wLeft++;
				}
			}
		}
		System.out.print(bLeft < wLeft ? bLeft : wLeft);
	}
}