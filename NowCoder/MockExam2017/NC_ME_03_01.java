import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		in.close();

		int cnt = 0;
		while (n / 10 > 0) {
			n = multiplyDigits(n);
			cnt++;
		}
		System.out.println(cnt);

	}

	private static int multiplyDigits(int n) {
		int result = 1;
		while (n > 0) {
			result *= n % 10;
			n /= 10;
		}
		return result;
	}
}