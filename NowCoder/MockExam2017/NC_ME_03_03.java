import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String a = in.next();
		String b = in.next();
		in.close();

		if (a.length() > b.length()) {
			System.out.println(-1);
			return;
		}

		int min = a.length();
		for (int i = 0; i <= b.length() - a.length(); i++) {

			int dist = 0;
			for (int j = 0; j < a.length(); j++) {
				dist += (a.charAt(j) != b.charAt(i + j)) ? 1 : 0;

				if (dist > min)
					break;
			}

			if (dist < min)
				min = dist;

		}

		System.out.println(min);

	}

}