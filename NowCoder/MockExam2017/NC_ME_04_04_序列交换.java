import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;

class Arr {
	int[] arr;

	Arr(int[] arr) {
		this.arr = Arrays.copyOf(arr, arr.length);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Arr) {
			Arr other = (Arr) obj;
			return Arrays.equals(arr, other.arr);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(arr);
	}
}

public class Main {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int num = in.nextInt();

		int[] arr = new int[num];
		for (int i = 0; i < num; i++)
			arr[i] = in.nextInt();
		in.close();

		HashSet<Arr> set = new HashSet<>();
		for (int i = 0; i < arr.length - 1; i++) {
			for (int j = i + 1; j < arr.length; j++) {
				swap(arr, i, j);
				set.add(new Arr(arr));
				swap(arr, i, j);

			}
		}

		System.out.println(set.size());
	}

	public static void swap(int[] arr, int i, int j) {
		int tmp = arr[i];
		arr[i] = arr[j];
		arr[j] = tmp;
	}
}