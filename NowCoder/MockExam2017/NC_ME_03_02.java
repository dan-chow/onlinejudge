import java.util.HashSet;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int a = in.nextInt();
		int b = in.nextInt();
		in.close();

		if (a > b) {
			System.out.println(0);
			return;
		}

		HashSet<String> primes = new HashSet<>();
		for (int i = 10; i < 100; i++) {
			if (isPrime(i))
				primes.add(String.valueOf(i));
		}

		int cnt = 0;
		for (int i = a; i <= b; i++) {
			String tmp = String.valueOf(i);
			for (String num : primes) {
				if (tmp.indexOf(num.charAt(0)) != -1 && tmp.indexOf(num.charAt(1)) != -1 && tmp.indexOf(num.charAt(0)) != tmp.lastIndexOf(num.charAt(1))) {
					cnt++;
					break;
				}

			}
		}

		System.out.println(cnt);

	}

	private static boolean isPrime(int n) {
		if (n < 2)
			return false;

		if (n == 2)
			return true;

		for (int i = 2; i <= Math.sqrt(n); i++) {
			if (n % i == 0)
				return false;
		}
		return true;
	}
}