import java.util.Arrays;
import java.util.Scanner;

class Point implements Comparable<Point> {

	int x;
	boolean isLeft;
	char type;

	Point(int x, boolean isLeft, char type) {
		this.x = x;
		this.isLeft = isLeft;
		this.type = type;
	}

	@Override
	public int compareTo(Point other) {
		if (this.x != other.x)
			return this.x - other.x;

		if (this.isLeft)
			return other.isLeft ? 0 : -1;

		return other.isLeft ? 1 : 0;
	}
}

public class Main {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		int N = in.nextInt();
		int M = in.nextInt();

		Point[] points = new Point[2 * N + 2 * M];

		for (int i = 0; i < N; i++) {
			points[2 * i] = new Point(in.nextInt(), true, 'A');
			points[2 * i + 1] = new Point(in.nextInt(), false, 'A');
		}

		for (int i = N; i < N + M; i++) {
			points[2 * i] = new Point(in.nextInt(), true, 'B');
			points[2 * i + 1] = new Point(in.nextInt(), false, 'B');
		}

		in.close();

		Arrays.sort(points);

		int aCnt = 0;
		int bCnt = 0;
		int start = points[0].x;

		int sum = 0;

		for (int i = 0; i < points.length; i++) {

			if (points[i].x != start) {
				if (aCnt > 0 && bCnt == 0)
					sum += points[i].x - start;

				start = points[i].x;
			}

			if (points[i].isLeft) {
				if (points[i].type == 'A')
					aCnt++;
				else
					bCnt++;
			} else {
				if (points[i].type == 'A')
					aCnt--;
				else
					bCnt--;
			}
		}

		System.out.println(sum);

	}
}