import java.util.Scanner;

public class Main {
	public static void main(String[] args) {

		int mod = 1000000007;
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		scanner.close();

		int[] dpEndWith0 = new int[n];
		int[] dpEndWith1 = new int[n];
		dpEndWith0[0] = 1;
		dpEndWith1[0] = 1;

		long max = 2;
		for (int i = 1; i < n; i++) {
			max = (max << 1) % mod;
			dpEndWith0[i] = (dpEndWith0[i - 1] + dpEndWith1[i - 1]) % mod;
			dpEndWith1[i] = dpEndWith0[i - 1] % mod;
		}

		long res = (max + 2 * mod - dpEndWith0[n - 1] - dpEndWith1[n - 1]) % mod;
		System.out.println(res);
	}

}
