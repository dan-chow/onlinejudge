import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int P = scanner.nextInt();
		int Q = scanner.nextInt();
		int N = scanner.nextInt();
		scanner.close();

		double ans = 0;

		for (int i = 0; i < N; i++) {
			int leftP = P;
			double lastP = 1.0;
			double expect = 0;
			int cnt = 0;
			while (true) {
				expect += (++cnt) * lastP * leftP / 100;

				if (leftP == 100)
					break;

				lastP *= (100 - leftP) / 100.0;
				leftP += Q;

				if (leftP > 100)
					leftP = 100;
			}
			ans += expect;
			P /= 2;
		}
		System.out.printf("%.2f\n", ans);
	}
}
