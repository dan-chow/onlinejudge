import java.util.Scanner;

public class Main {
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		for (int i = 0; i < n; i++) {
			char[] src = scanner.next().toCharArray();
			char[] dst = scanner.next().toCharArray();
			System.out.println(minSwap(src, dst, 0, 0));
		}
		scanner.close();
	}

	private static int minSwap(char[] src, char[] dst, int idx, int swapCnt) {
		if (idx == src.length)
			return swapCnt;

		if (src[idx] == dst[idx]) {
			idx++;
			return minSwap(src, dst, idx, swapCnt);
		} else {
			int minValue = Integer.MAX_VALUE;
			for (int j = idx + 1; j < src.length; j++) {
				if (src[j] == dst[idx]) {
					swap(src, idx, j);
					minValue = Math.min(minValue, minSwap(src, dst, idx + 1, swapCnt + 1));
					swap(src, idx, j);
				}
			}
			return minValue;
		}
	}

	private static void swap(char[] arr, int i, int j) {
		char tmp = arr[i];
		arr[i] = arr[j];
		arr[j] = tmp;
	}
}
