import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		long M = scanner.nextLong();
		long N = scanner.nextLong();
		scanner.close();

		long equalCnt = getDivisorNum(gcd(M, N));
		long totalCnt = getDivisorNum(M) * getDivisorNum(N);

		long c = gcd(equalCnt, totalCnt);

		System.out.println(totalCnt / c + " " + equalCnt / c);

	}

	private static long getDivisorNum(long m) {
		long cnt = 1;
		int pow = 1;
		for (long i = 2; i * i <= m; i++) {
			while (m % i == 0) {
				pow++;
				m /= i;
			}
			cnt *= pow;
			pow = 1;
		}

		if (m != 1)
			cnt *= 2;

		return cnt;
	}

	private static long gcd(long m, long n) {

		if (n == 0)
			return m;

		return gcd(n, m % n);
	}
}
