import java.util.Arrays;
import java.util.Scanner;

class Point implements Comparable<Point> {

	public static final int LEFT = 0;
	public static final int RIGHT = 1;

	int pos;
	int label;

	Point(int pos, int label) {
		this.pos = pos;
		this.label = label;
	}

	@Override
	public int compareTo(Point other) {
		if (this.pos != other.pos)
			return this.pos - other.pos;

		if (this.label == RIGHT)
			return other.label == RIGHT ? 0 : -1;

		return other.label == LEFT ? 0 : 1;
	}
}

public class Main {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		int n = in.nextInt();

		Point[] points = new Point[2 * n];

		for (int i = 0; i < n; i++) {
			points[2 * i] = new Point(in.nextInt(), Point.LEFT);
			points[2 * i + 1] = new Point(in.nextInt(), Point.RIGHT);
		}

		Arrays.sort(points);

		int cnt = 0;
		int max = 0;
		for (int i = 0; i < points.length; i++) {
			if (points[i].label == Point.LEFT) {
				cnt++;
				if (cnt > max)
					max = cnt;
			} else {
				cnt--;
			}
		}

		System.out.println(max);
		in.close();
	}
}